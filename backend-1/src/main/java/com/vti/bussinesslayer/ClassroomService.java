package com.vti.bussinesslayer;



import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vti.datalayer.IClassroomRepository;
import com.vti.datalayer.IClassroomStudentRepository;
import com.vti.datalayer.ITeacherRepository;
import com.vti.entity.Classroom;
import com.vti.entity.ClassroomStudent;
import com.vti.entity.Student;
import com.vti.entity.Teacher;


@Service
public class ClassroomService implements IClassroomService{
	
	@Autowired
	private IClassroomRepository repository;
	
	@Autowired
	private ITeacherRepository teacherSerivce;
	
	@Autowired
	private IClassroomStudentRepository classStService;
	
	@Override
	public Classroom getClassroomByName(String name) {
		
		return repository.findByClassName(name);
	}

	@Override
	public List<Classroom> getListClassroomByMentorId(int id) {
		return repository.findByMentorList_Mentor_Id(id);
	}

	@Override
	public List<Classroom> getAllClassroom() {
		return repository.findAll();
	}

	@Override
	public List<Classroom> getListClassInGrade(int grade) {
		return repository.findByGrade(grade);
	}

	@Override
	public void createClass(Classroom clazz) {
		Teacher teacher = teacherSerivce.findById(clazz.getTeacherId().getId());
		clazz.setTeacherId(teacher);
		clazz.setCreatedDate(new Date());
		repository.save(clazz);
	}

	@Override
	public List<Classroom> getListClassroomBySchedule(String schedule) {
		return repository.findBySchedule(schedule);
	}

	@Override
	public List<Student> getListStudentInClass(int classId) {
		
		List<ClassroomStudent> listSt = classStService.findByClassroom_Id(classId);
		
		List<Student> res = new ArrayList<Student>();
		 
		for(ClassroomStudent cs: listSt) {
			res.add(cs.getStudent());
		}
		
		return res;
	}

	@Override
	public List<Classroom> getListClassroomWithSameTypeToday(String subject, int grade, String schedule) {
		return repository.findBySubjectAndGradeAndSchedule(subject, grade, schedule);
	}

	@Override
	public List<Classroom> getListClassroomByScheduleAndGrade(int grade, String schedule) {
		return repository.findByGradeAndSchedule(grade, schedule);
	}

	@Override
	public List<Classroom> getListClassroomInGradeAndSubject(int grade, String subject) {
		// TODO Auto-generated method stub
		return repository.findByGradeAndSubject(grade, subject);
	}

	@Override
	public Classroom getClassroomById(int classId) {
		// TODO Auto-generated method stub
		return repository.findById(classId);
	}

	@Override
	public void updateClass(Classroom clazz) {
		repository.save(clazz);
		
	}

	@Override
	public void deleteClass(int classId) {
		Classroom clazz = repository.findById(classId);
		repository.delete(clazz);
	}

	

	

}

package com.vti.config.security;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import com.vti.bussinesslayer.IUserService;
import com.vti.bussinesslayer.JWTTokenService;
//import com.vti.entity.User;


public class JWTAuthenticationFilter extends AbstractAuthenticationProcessingFilter {
	private IUserService userService;

	public JWTAuthenticationFilter(String url, AuthenticationManager authManager, IUserService service) {
		super(new AntPathRequestMatcher(url));
		setAuthenticationManager(authManager);
		this.userService = service;
	}

	@Override
    public Authentication attemptAuthentication(
    		HttpServletRequest request, 
    		HttpServletResponse response) 
    		throws AuthenticationException, IOException, ServletException {

    	List<GrantedAuthority> list = new ArrayList<GrantedAuthority>();
    	
    	
    	String username = request.getParameter("username");
    	
    	String role = userService.findByUserName(username).getRole();
    	
    	list.add(new SimpleGrantedAuthority(role));

        return getAuthenticationManager().authenticate(
                new UsernamePasswordAuthenticationToken(
                		request.getParameter("username"),
                		request.getParameter("password"),
                		list
                )
        );
    }

    @Override
    protected void successfulAuthentication(
    		HttpServletRequest request, 
    		HttpServletResponse response, 
    		FilterChain chain, 
    		Authentication authResult) throws IOException, ServletException {
    	// infor user
//    	User user = userService.findUserByUserName(authResult.getName());
        JWTTokenService.addJWTTokenAndUserInfoToBody(response, userService, authResult.getName());
    }


}

package com.vti.bussinesslayer;

import java.util.List;


import com.vti.entity.Mentor;

public interface IMentorService {
	
	public List<Mentor> getListMentor();
	
	public Mentor getMentorById(int id);
	
	public void createMentor (Mentor mentor);
}

package com.vti.dto;

import java.util.Date;



import com.fasterxml.jackson.annotation.JsonFormat;
import com.vti.entity.Chapter;
import com.vti.entity.Video;

public class LessonDTO3 {
	
	private int id;
	
	
	private String lessonName;
	
	@JsonFormat(pattern = "dd-MM-yyyy")
	private Date date;
	

	private Video video;
	
	private Chapter chapter;
	
	private HomeWorkDTO homeWork;
	
	

	public LessonDTO3(int id, String lessonName, Date date, Video video, Chapter chapter, HomeWorkDTO homeWork) {
		super();
		this.id = id;
		this.lessonName = lessonName;
		this.date = date;
		this.video = video;
		this.chapter = chapter;
		this.homeWork = homeWork;
	}

	public HomeWorkDTO getHomeWork() {
		return homeWork;
	}

	public void setHomeWork(HomeWorkDTO homeWork) {
		this.homeWork = homeWork;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLessonName() {
		return lessonName;
	}

	public void setLessonName(String lessonName) {
		this.lessonName = lessonName;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Video getVideo() {
		return video;
	}

	public void setVideo(Video video) {
		this.video = video;
	}

	public Chapter getChapter() {
		return chapter;
	}

	public void setChapter(Chapter chapter) {
		this.chapter = chapter;
	}
	
	public LessonDTO3() {
		// TODO Auto-generated constructor stub
	}

	public LessonDTO3(int id, String lessonName, Date date, Video video, Chapter chapter) {
		super();
		this.id = id;
		this.lessonName = lessonName;
		this.date = date;
		this.video = video;
		this.chapter = chapter;
	}
	
}

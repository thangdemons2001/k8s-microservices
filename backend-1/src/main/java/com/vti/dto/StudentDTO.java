package com.vti.dto;


import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.vti.entity.Student;


public class StudentDTO {
	
	private int id;
		
	private String firstName;
	
	private String lastName;
		
	private String fullName;
	
	private String status;
	
	private String school;
	
	private int grade;
	
	private String studentNumber;
	
	private String parentNumber;
	
	private String parentName;
	
	private String facebookLink;
	
	private List<ClassroomDTO> listClass;
	
	private String avatarUrl;
	
	private String facebookUrl;
	
	private String testingSystemId;
	
	@JsonFormat(pattern="yyyy-MM-dd")
	private Date startDate;
	
	private String note;
	
	private String reason;
	

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public StudentDTO(int id, String firstName, String lastName, String fullName, String status, String school,
			int grade, String studentNumber, String parentNumber, String parentName, String facebookLink,
			String avatarUrl, String facebookUrl, String testingSystemId) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.fullName = fullName;
		this.status = status;
		this.school = school;
		this.grade = grade;
		this.studentNumber = studentNumber;
		this.parentNumber = parentNumber;
		this.parentName = parentName;
		this.facebookLink = facebookLink;
		this.avatarUrl = avatarUrl;
		this.facebookUrl = facebookUrl;
		this.testingSystemId = testingSystemId;
	}
	
	

	public StudentDTO(int id, String firstName, String lastName, String fullName, String status, String school,
			int grade, String studentNumber, String parentNumber, String parentName, String facebookLink,
			String avatarUrl, String facebookUrl, String testingSystemId, Date startDate) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.fullName = fullName;
		this.status = status;
		this.school = school;
		this.grade = grade;
		this.studentNumber = studentNumber;
		this.parentNumber = parentNumber;
		this.parentName = parentName;
		this.facebookLink = facebookLink;
		this.avatarUrl = avatarUrl;
		this.facebookUrl = facebookUrl;
		this.testingSystemId = testingSystemId;
		this.startDate = startDate;
	}
	
	public StudentDTO(int id, String firstName, String lastName, String fullName, String status, String school,
			int grade, String studentNumber, String parentNumber, String parentName, String facebookLink,
			String avatarUrl, String facebookUrl, String testingSystemId, Date startDate, String note) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.fullName = fullName;
		this.status = status;
		this.school = school;
		this.grade = grade;
		this.studentNumber = studentNumber;
		this.parentNumber = parentNumber;
		this.parentName = parentName;
		this.facebookLink = facebookLink;
		this.avatarUrl = avatarUrl;
		this.facebookUrl = facebookUrl;
		this.testingSystemId = testingSystemId;
		this.startDate = startDate;
		this.note = note;
	}
	
	public StudentDTO(int id, String firstName, String lastName, String fullName, String status, String school,
			int grade, String studentNumber, String parentNumber, String parentName, String facebookLink,
			String avatarUrl, String facebookUrl, String testingSystemId, Date startDate, String note, String reason) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.fullName = fullName;
		this.status = status;
		this.school = school;
		this.grade = grade;
		this.studentNumber = studentNumber;
		this.parentNumber = parentNumber;
		this.parentName = parentName;
		this.facebookLink = facebookLink;
		this.avatarUrl = avatarUrl;
		this.facebookUrl = facebookUrl;
		this.testingSystemId = testingSystemId;
		this.startDate = startDate;
		this.note = note;
		this.reason = reason;
	}

	public String getTestingSystemId() {
		return testingSystemId;
	}

	public void setTestingSystemId(String testingSystemId) {
		this.testingSystemId = testingSystemId;
	}

	public String getFacebookUrl() {
		return facebookUrl;
	}

	public void setFacebookUrl(String facebookUrl) {
		this.facebookUrl = facebookUrl;
	}

	public String getAvatarUrl() {
		return avatarUrl;
	}

	public void setAvatarUrl(String avatarUrl) {
		this.avatarUrl = avatarUrl;
	}

	public String getFacebookLink() {
		return facebookLink;
	}

	public void setFacebookLink(String facebookLink) {
		this.facebookLink = facebookLink;
	}

	public String getSchool() {
		return school;
	}

	public void setSchool(String school) {
		this.school = school;
	}

	public int getGrade() {
		return grade;
	}

	public void setGrade(int grade) {
		this.grade = grade;
	}

	public String getStudentNumber() {
		return studentNumber;
	}

	public void setStudentNumber(String studentNumber) {
		this.studentNumber = studentNumber;
	}

	public String getParentNumber() {
		return parentNumber;
	}

	public void setParentNumber(String parentNumber) {
		this.parentNumber = parentNumber;
	}

	public String getParentName() {
		return parentName;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	public List<ClassroomDTO> getListClass() {
		return listClass;
	}

	public void setListClass(List<ClassroomDTO> listClass) {
		this.listClass = listClass;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}



	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	

	public StudentDTO(int id, String firstName, String lastName, String fullName, String status, String school,
			int grade, String studentNumber, String parentNumber, String parentName, String facebookLink) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.fullName = fullName;
		this.status = status;
		this.school = school;
		this.grade = grade;
		this.studentNumber = studentNumber;
		this.parentNumber = parentNumber;
		this.parentName = parentName;
		this.facebookLink = facebookLink;
	}
	
	
	
	public StudentDTO(int id, String firstName, String lastName, String fullName, String status, String school,
			int grade, String studentNumber, String parentNumber, String parentName, String facebookLink,
			List<ClassroomDTO> listClass) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.fullName = fullName;
		this.status = status;
		this.school = school;
		this.grade = grade;
		this.studentNumber = studentNumber;
		this.parentNumber = parentNumber;
		this.parentName = parentName;
		this.facebookLink = facebookLink;
		this.listClass = listClass;
	}

	public StudentDTO(int id, String firstName, String lastName, 
			String fullName, String status,  String school, int grade, String studentNumber,
			String parentNumber, String parentName, List<ClassroomDTO> listClass) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.fullName = fullName;
		this.status = status;
		this.school = school;
		this.grade = grade;
		this.studentNumber = studentNumber;
		this.parentNumber = parentNumber;
		this.parentName = parentName;
		this.listClass = listClass;
	}
	public StudentDTO(int id, String firstName, String lastName, 
			String fullName, String status,  String school, int grade, String studentNumber,
			String parentNumber, String parentName) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.fullName = fullName;
		this.status = status;
		this.school = school;
		this.grade = grade;
		this.studentNumber = studentNumber;
		this.parentNumber = parentNumber;
		this.parentName = parentName;
	}
	
	
	public StudentDTO(int id, String firstName, String lastName, String fullName, String status, String school,
			int grade, String studentNumber, String parentNumber, String parentName, String facebookLink,
			 String avatarUrl) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.fullName = fullName;
		this.status = status;
		this.school = school;
		this.grade = grade;
		this.studentNumber = studentNumber;
		this.parentNumber = parentNumber;
		this.parentName = parentName;
		this.facebookLink = facebookLink;
	
		this.avatarUrl = avatarUrl;
	}

	public StudentDTO() {
		// TODO Auto-generated constructor stub
	}
	
	
	
	public StudentDTO(int id, String firstName, String lastName, String fullName, String status, String school,
			int grade, String studentNumber, String parentNumber, String parentName, String facebookLink,
			String avatarUrl, String facebookUrl) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.fullName = fullName;
		this.status = status;
		this.school = school;
		this.grade = grade;
		this.studentNumber = studentNumber;
		this.parentNumber = parentNumber;
		this.parentName = parentName;
		this.facebookLink = facebookLink;
		this.avatarUrl = avatarUrl;
		this.facebookUrl = facebookUrl;
	}

	public Student convertToEntity() {
		Student st = new Student();
		
		st.setId(id);
		st.setFirstName(firstName);
		st.setLastName(lastName);
		st.setFullName(fullName);
		st.setGrade(grade);
		st.setParentName(parentName);
		st.setParentNumber(parentNumber);
		st.setSchool(school);
		
		
		return st;
	}
}

package com.vti.presentationlayer;


import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vti.bussinesslayer.IHomeWorkService;
import com.vti.bussinesslayer.IQuestionService;
import com.vti.dto.QuestionDTO;
import com.vti.entity.HomeWork;
import com.vti.entity.Question;

@RestController
@RequestMapping(value = "api/v1/questions")
@CrossOrigin("*")
public class QuestionController {

	@Autowired
	private IQuestionService service;

	@Autowired
	private IHomeWorkService homeworkService;

	@PostMapping("/homeworks/{homeworkId}")
	public ResponseEntity<?> createListExamResult(@RequestBody List<Question> dto,
			@PathVariable(name = "homeworkId") int homeworkId) {
		if (homeworkService.getHomeWorkById(homeworkId) == null) {
			return new ResponseEntity<String>("empty", HttpStatus.OK);
		}
		HomeWork homework = homeworkService.getHomeWorkById(homeworkId);
		for (Question question : dto) {
			question.setHomework(homework);
		}
		service.createQuestion(dto);
		return new ResponseEntity<String>("create successful!", HttpStatus.OK);
	}
	
	@GetMapping("/homeworks/{homeworkId}")
	public List<QuestionDTO> getAllHomeWorkQuestions (@PathVariable(name = "homeworkId") int homeworkId){
		List<Question> questions = service.getHomeWorkQuestion(homeworkId);
		List<QuestionDTO> res = new ArrayList<QuestionDTO>();
		for(Question question :questions) {
			res.add(question.convertToDTO());
		}
		return res;
	}
	
	@DeleteMapping("/{questionId}")
	public ResponseEntity<?> deleteQuestion(@PathVariable(name = "questionId") long questionId) {
		service.deleteQuestion(questionId);
		return new ResponseEntity<String>("delete successful!", HttpStatus.OK);
	}
	
	@PutMapping("/{questionId}")
	public ResponseEntity<?> updateQuestion(@PathVariable(name = "questionId") long questionId, @RequestBody Question dto) {
		service.updateQuestion(dto);
		return new ResponseEntity<String>("delete successful!", HttpStatus.OK);
	}
	
}

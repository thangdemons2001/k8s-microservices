package com.vti.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import com.vti.dto.MentorDTO;
@Entity
@Table(name = "Mentor", catalog = "TCTmanagement")
@PrimaryKeyJoinColumn(name = "mentor_id")
public class Mentor extends User implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Column(name = "school_name", length = 800, nullable = false)
	private String school;
	
	@OneToMany(mappedBy = "mentor")
	private List<ClassroomMentor> listClass;
	
	public Mentor(String school) {
		super();
		this.school = school;
	}
	
	public Mentor() {
		// TODO Auto-generated constructor stub
	}

	public String getSchool() {
		return school;
	}

	public void setSchool(String school) {
		this.school = school;
	}
	
	public MentorDTO convertToDTO() {
		return new MentorDTO(getId(), getFullName(), getFirstName(), getLastName(), school);
	}
	

}

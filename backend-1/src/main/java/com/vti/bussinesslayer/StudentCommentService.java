package com.vti.bussinesslayer;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vti.datalayer.IStudentCommentRepository;
import com.vti.entity.StudentComment;



@Service
public class StudentCommentService implements IStudentCommentService {
	
	@Autowired
	private IStudentCommentRepository repository;

	@Override
	public void createStudentComment(StudentComment comment) {
			repository.save(comment);
	}

	@Override
	public List<StudentComment> getLastTenStudentCommentOfStudent(int studentId) {
		return repository.findTop10ByStudent_IdOrderByCommentDateDesc(studentId);
	}
	
	
	
}

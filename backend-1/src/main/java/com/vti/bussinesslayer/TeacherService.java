package com.vti.bussinesslayer;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vti.datalayer.ITeacherRepository;
import com.vti.entity.Teacher;
@Service
public class TeacherService implements ITeacherService{
	
	@Autowired
	private ITeacherRepository repository;
	
	
	@Override
	public List<Teacher> findTeacherBySubjectName(String subjectName) {
		// TODO Auto-generated method stub
		return repository.findBySubjectName(subjectName);
	}


	@Override
	public Teacher findTeacherById(int id) {
		// TODO Auto-generated method stub
		return repository.findById(id);
	}


	@Override
	public void createTeacher(Teacher teacher) {
		teacher.setCreatedDate(new Date());
		repository.save(teacher);
	}


	@Override
	public List<Teacher> findAllTeacher() {
		return repository.findAll();
	}

}

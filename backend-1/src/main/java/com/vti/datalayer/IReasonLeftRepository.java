package com.vti.datalayer;



import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;


import com.vti.entity.ReasonLeft;


public interface IReasonLeftRepository extends JpaRepository<ReasonLeft, Integer>, JpaSpecificationExecutor<ReasonLeft> {
	
	
	public List<ReasonLeft> findByDeactivedStudent_Id (int studentId);
	
}

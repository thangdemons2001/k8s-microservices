package com.vti.bussinesslayer;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vti.datalayer.IMentorRepository;
import com.vti.entity.Mentor;
@Service
public class MentorService implements IMentorService{
	
	@Autowired
	private IMentorRepository repository;

	@Override
	public List<Mentor> getListMentor() {
		// TODO Auto-generated method stub
		return repository.findAll();
	}

	@Override
	public Mentor getMentorById(int id) {
		// TODO Auto-generated method stub
		return repository.findById(id);
	}

	@Override
	public void createMentor(Mentor mentor) {
		mentor.setCreatedDate(new Date());
		repository.save(mentor);
	}

}

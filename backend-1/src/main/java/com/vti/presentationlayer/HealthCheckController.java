package com.vti.presentationlayer;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;



@RestController
@RequestMapping(value = "")
@CrossOrigin("*")
public class HealthCheckController {
	
	@GetMapping(value = "/")
	public String healthCheckLoadBalance() {
		return "healthy";
	}

}

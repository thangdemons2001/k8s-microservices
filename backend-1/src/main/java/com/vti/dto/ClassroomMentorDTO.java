package com.vti.dto;

public class ClassroomMentorDTO {
	
		private int mentorId;
		
		private int classId;

		public int getMentorId() {
			return mentorId;
		}

		public void setMentorId(int mentorId) {
			this.mentorId = mentorId;
		}

		public int getClassId() {
			return classId;
		}

		public void setClassId(int classId) {
			this.classId = classId;
		}

		public ClassroomMentorDTO(int mentorId, int classId) {
			super();
			this.mentorId = mentorId;
			this.classId = classId;
		}
		
		public ClassroomMentorDTO() {
			// TODO Auto-generated constructor stub
		}
}

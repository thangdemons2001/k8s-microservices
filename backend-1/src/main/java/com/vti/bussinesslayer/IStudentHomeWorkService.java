package com.vti.bussinesslayer;


import java.util.List;
import com.vti.entity.StudentHomeWork;

public interface IStudentHomeWorkService {
		
	public void createStudentSubmitHomeWork (StudentHomeWork submit);
	
	public void createMultiStudentSubmitHomeWork (List<StudentHomeWork> listSubmision, List<Integer> studentIds);
	
	public void deleteStudentSubmitHomeWorkInLesson (int studentId, int lessonId);
	
	public void deleteMultiStudentSubmitHomeWorkInLesson (int lessonId, List<Integer> studentIds);
	
	public List<StudentHomeWork> getAllSubmittedStudentInLesson (int lessonId);

	
	public List<StudentHomeWork> getAllSubmittedHomeWorkOfStudentInMonth (String subject, int month,int grade);
	
	public List<StudentHomeWork> getAllSubmittedHomeWorkOfStudentInThreeMonths(int studentId);
}

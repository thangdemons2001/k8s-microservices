package com.vti.bussinesslayer;

import java.util.List;

import com.vti.entity.Chapter;



public interface IChapterService {
	
	public void createChapter (Chapter chapter);
	
	public Chapter getChapterById(int chapterId);
	
	public List<Chapter> getAllChapterSubjectInGrade (String subject, int grade);

}

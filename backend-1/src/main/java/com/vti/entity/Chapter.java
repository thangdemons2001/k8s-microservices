package com.vti.entity;

import java.io.Serializable;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.Table;




@Entity
@Table(name = "Chapter", catalog = "TCTmanagement")
public class Chapter implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Column(name = "chapter_id" , nullable = false)
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "chapter_name" , nullable = false, length = 800)
	private String chapterName;
	
	@Column(name = "grade" , nullable = false)
	private int grade;
	
	@Column(name = "subject" , nullable = false, length = 800)
	private String subject;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getChapterName() {
		return chapterName;
	}

	public void setChapterName(String chapterName) {
		this.chapterName = chapterName;
	}

	public int getGrade() {
		return grade;
	}

	public void setGrade(int grade) {
		this.grade = grade;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}
	
	public Chapter() {
		// TODO Auto-generated constructor stub
	}

	public Chapter(int id, String chapterName, int grade, String subject) {
		super();
		this.id = id;
		this.chapterName = chapterName;
		this.grade = grade;
		this.subject = subject;
	}
	
	
}

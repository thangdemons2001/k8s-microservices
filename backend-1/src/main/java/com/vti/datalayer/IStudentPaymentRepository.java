package com.vti.datalayer;







import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.vti.entity.StudentPayment;



public interface IStudentPaymentRepository extends JpaRepository<StudentPayment, Long>, JpaSpecificationExecutor<StudentPayment> {
		

	
	
		
}

package com.vti.presentationlayer;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vti.bussinesslayer.IExamResultService;
import com.vti.bussinesslayer.IExamService;
import com.vti.bussinesslayer.IExamTypeService;
import com.vti.dto.ExamDTO;
import com.vti.dto.ExamResultDTO;
import com.vti.dto.ExamResultDTO2;
import com.vti.dto.ExamTypeDTO;
import com.vti.dto.StudentDTO;
import com.vti.dto.StudentDTO5;
import com.vti.entity.Exam;
import com.vti.entity.ExamResult;
import com.vti.entity.ExamResultKey;
import com.vti.entity.ExamType;
import com.vti.entity.Student;

@RestController
@RequestMapping(value = "api/v1/exams")
@CrossOrigin("*")
public class ExamController {

	@Autowired
	private IExamTypeService typeService;

	@Autowired
	private IExamService service;

	@Autowired
	private IExamResultService resultService;

	@PostMapping("/types/")
	public ResponseEntity<?> createExamType(@RequestBody ExamTypeDTO dto) {
		ExamType type = new ExamType();
		type.setName(dto.getName());
		typeService.createExamType(type);
		return new ResponseEntity<Integer>(type.getId(), HttpStatus.OK);
	}

	@GetMapping("/types/")
	public List<ExamTypeDTO> getAllExamType() {
		List<ExamType> query = typeService.getAllExamType();
		List<ExamTypeDTO> res = new ArrayList<ExamTypeDTO>();

		for (ExamType type : query) {
			res.add(type.convertToDTO());
		}

		return res;
	}

	@PostMapping("/")
	public ResponseEntity<?> createExam(@RequestBody ExamDTO dto) {
		ExamType type = typeService.findTypeById(dto.getTypeId());
		Exam exam = new Exam();
		exam.setName(dto.getExamName());
		exam.setType(type);
		if (dto.getCreatedDate() != null) {
			exam.setCreatedDate(dto.getCreatedDate());
		} else {
			exam.setCreatedDate(new Date());
		}
		if(dto.getTestingSystemId() != null && !dto.getTestingSystemId().isEmpty()) {
			exam.setTestingSystemId(dto.getTestingSystemId());
		}
		service.createExam(exam);
		return new ResponseEntity<Integer>(exam.getId(), HttpStatus.OK);
	}
	
	@DeleteMapping("/")
	public ResponseEntity<?> deleteExam(int examId) {
		
		try {
			service.deleteExam(examId);
			return new ResponseEntity<String>("delete successful!", HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<String>("delete unsuccessful!", HttpStatus.OK);
		}
		
		
		
	}

	@PostMapping("/results/")
	public ResponseEntity<?> createListExamResult(@RequestBody List<ExamResultDTO> dto) {
		List<ExamResult> results = new ArrayList<ExamResult>();
		for (ExamResultDTO result : dto) {
			ExamResultKey id = new ExamResultKey(result.getStudentId(), result.getClassroomId(), result.getExamId());
			ExamResult temp = new ExamResult();
			temp.setId(id);
			temp.setMark(result.getMark());
			results.add(temp);
		}
		resultService.createListExamResult(results);
		return new ResponseEntity<String>("create successful!", HttpStatus.OK);
	}

	@GetMapping("/")
	public Set<ExamDTO> getListExamInClass(int classId) {

		Set<ExamDTO> res = new HashSet<ExamDTO>();

		List<ExamResult> results = resultService.getListExamInClass(classId);

		for (ExamResult result : results) {
			res.add(result.getExam().convertToDTO());
		}

		return res;
	}

	@GetMapping("/results/")
	public List<StudentDTO5> getListStudentExamMarkInClass(int classId, int examId) {

		List<StudentDTO5> res = new ArrayList<StudentDTO5>();

		List<ExamResult> results = resultService.getListStudentExamMarkInClass(classId, examId);

		for (ExamResult result : results) {
			res.add(result.getStudent().convertToDTO6(result.getMark()));
		}

		return res;
	}

	@PutMapping("/results/students/{studentId}")
	public ResponseEntity<?> updateExamResultOfStudent(@PathVariable(name = "studentId") int studentId,
			@RequestBody ExamResult result) {
		ExamResultKey id = new ExamResultKey(studentId, result.getId().getClassroomId(), result.getId().getExamId());
		resultService.updateExamResult(id, result.getMark());
		return new ResponseEntity<String>("update successful!", HttpStatus.OK);
	}
	
	@PutMapping("/results/")
	public ResponseEntity<?> updateManyExamResultOfStudent(@RequestBody List<ExamResultDTO> dtos) {
		resultService.updateManyExamResult(dtos);
		return new ResponseEntity<String>("update successful!", HttpStatus.OK);
	}

	@GetMapping("/results/details/{grade}")
	public List<ExamResultDTO2> getAllSubjectExamOfGradeInMonth(int month, @PathVariable(name = "grade") int grade,
			String subject) {

		List<ExamResultDTO2> res = service.findAllSubjectExamInMonthAtGrade(month, subject, grade);

		return res;
	}

	@GetMapping("/results/{examId}/students-mark")
	public List<StudentDTO5> getAllStudentMarkInExam(@PathVariable(name = "examId") int examId) {

		List<StudentDTO5> res = new ArrayList<StudentDTO5>();

		List<ExamResult> query = resultService.getListStudentMarkInExam(examId);
		for (ExamResult st : query) {
			res.add(st.getStudent().convertToDTO6(st.getMark()));
		}

		return res;
	}

	@GetMapping("/results/unsubmit/students/grade/{grade}")
	public List<StudentDTO> getAllStudentNotTakeSubjectExamInMonthAtGrade(int month,
			@PathVariable(name = "grade") int grade, String subject) {

		List<StudentDTO> res = new ArrayList<StudentDTO>();

		List<Student> query = resultService.getListStudentNotTakeSubjectExamInMonthAtGrade(month, subject, grade);
		for (Student st : query) {
			res.add(st.convertToDTO2());
		}

		return res;
	}

	@GetMapping("/results-statistic/")
	public int[] getStatisticStudentExamMarkInClass(int classId, int examId) {

		int res[] = new int[6];

		for (int i = 0; i < 5; i++) {
			res[i] = 0;
		}
		List<ExamResult> results = resultService.getListStudentExamMarkInClass(classId, examId);

		for (ExamResult result : results) {
			if (result.getMark() >= 0 && result.getMark() < 3) {
				res[0]++;
			}
			if (result.getMark() >= 3 && result.getMark() < 5) {
				res[1]++;
			}
			if (result.getMark() >= 5 && result.getMark() < 7) {
				res[2]++;
			}
			if (result.getMark() >= 7 && result.getMark() < 8) {
				res[3]++;
			}
			if (result.getMark() >= 8 && result.getMark() < 9) {
				res[4]++;
			}
			if (result.getMark() >= 9 && result.getMark() <= 10) {
				res[5]++;
			}
		}

		return res;
	}
}

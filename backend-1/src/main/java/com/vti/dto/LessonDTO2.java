package com.vti.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.vti.entity.Chapter;

public class LessonDTO2 {
	
	private String lessonName;
	
	@JsonFormat(pattern = "dd-MM-yyyy")
	private Date date;
	
	private Chapter chapter;
	
	private int videoId;

	public String getLessonName() {
		return lessonName;
	}

	public void setLessonName(String lessonName) {
		this.lessonName = lessonName;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Chapter getChapter() {
		return chapter;
	}

	public void setChapter(Chapter chapter) {
		this.chapter = chapter;
	}

	public int getVideoId() {
		return videoId;
	}

	public void setVideoId(int videoId) {
		this.videoId = videoId;
	}
	
	public LessonDTO2() {
		// TODO Auto-generated constructor stub
	}

	public LessonDTO2(String lessonName, Date date, Chapter chapter, int videoId) {
		super();
		this.lessonName = lessonName;
		this.date = date;
		this.chapter = chapter;
		this.videoId = videoId;
	}

	

}

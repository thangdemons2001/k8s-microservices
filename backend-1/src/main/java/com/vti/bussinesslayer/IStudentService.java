package com.vti.bussinesslayer;

import java.util.List;


import com.vti.dto.StudentDTO7;
import com.vti.dto.StudentSubjectInfo;
import com.vti.entity.Student;
import com.vti.filter.Filter;

public interface IStudentService {
		
		public List<Student> getListStudentByClassId(int id, short page, short pageSize, Filter filter);
		
		public List<Student> getListStudentInGrade(int grade);
		
		public List<StudentDTO7> getListStudentAvgMarkSubjectInGrade(int grade,String subjectName);
		
		public List<Student> getAllStudent();
		
		public Integer countNewStudentAtGradeOfSubjectInMonth (int month, int grade, String subject);
		
		public Integer countNewStudentAtGradeInMonth (int month, int grade, int year);
		
		public Integer countAllStudentAtGradeInMonth (int month, int grade, int year);
		
		public Integer countDeactivedStudentAtGradeInMonth (int month, int grade, int year);
		
		public void updateStudent(Student studentdto);
		
		public Student findStudentById(int id);
		
		public void createStudent(Student student);
		
		public Student findStudentByPhoneNumber(String number);
		
		public List<Student> findStudentByStatus(String status);
		
		public StudentSubjectInfo getSubjectStudentInfo(int studentId);
		
		public void deleteStudent (int studentId);
		
		public List<Student> getListStudentInGradeWithActiveStatus (int grade, String status);
}

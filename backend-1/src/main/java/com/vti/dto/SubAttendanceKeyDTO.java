package com.vti.dto;

import java.io.Serializable;
import java.util.Date;





import com.fasterxml.jackson.annotation.JsonFormat;



public class SubAttendanceKeyDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int studentId;
	
	private int classroomId;
	
	
	@JsonFormat(pattern="yyyy-MM-dd")
	private Date dateId;
	
	public int getStudentId() {
		return studentId;
	}


	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}


	public Date getDateId() {
		return dateId;
	}


	public void setDateId(Date dateId) {
		this.dateId = dateId;
	}


	public SubAttendanceKeyDTO() {
		// TODO Auto-generated constructor stub
	}

	

	public int getClassroomId() {
		return classroomId;
	}

	public void setClassroomId(int classroomId) {
		this.classroomId = classroomId;
	}



	public SubAttendanceKeyDTO(int studentId, int classroomId, Date dateId) {
		super();
		this.studentId = studentId;
		this.classroomId = classroomId;
		this.dateId = dateId;
	}

	
	
}

package com.vti.dto;

import java.util.ArrayList;

public class StudentDTO9 {
	
	private int id;
		
	private String firstName;
	
	private String lastName;
		
	private String fullName;
	
	private String status;
	
	private String school;
	
	private int grade;
	
	private ArrayList<ClassroomDTO> listClass;
	
	private String studentNumber;
	
	private String parentNumber;
	
	private String parentName;
	
	private String first;
	
	private String second;
	
	private String third;
	
	private String fourth;
	
	private String avatarUrl;
	
	private String facebookUrl;
	
	
	

	public StudentDTO9(int id, String firstName, String lastName, String fullName, String status, String school,
			int grade, String studentNumber, String parentNumber, String parentName, String avatarUrl,
			String facebookUrl) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.fullName = fullName;
		this.status = status;
		this.school = school;
		this.grade = grade;
		this.studentNumber = studentNumber;
		this.parentNumber = parentNumber;
		this.parentName = parentName;
		this.avatarUrl = avatarUrl;
		this.facebookUrl = facebookUrl;
	}



	public String getAvatarUrl() {
		return avatarUrl;
	}



	public void setAvatarUrl(String avatarUrl) {
		this.avatarUrl = avatarUrl;
	}



	public String getFacebookUrl() {
		return facebookUrl;
	}



	public void setFacebookUrl(String facebookUrl) {
		this.facebookUrl = facebookUrl;
	}



	public StudentDTO9(int id, String firstName, String lastName, String fullName, String status, String school,
			int grade, ArrayList<ClassroomDTO> listClass, String studentNumber, String parentNumber, String parentName, String first,
			String second, String third, String fourth) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.fullName = fullName;
		this.status = status;
		this.school = school;
		this.grade = grade;
		this.listClass = listClass;
		this.studentNumber = studentNumber;
		this.parentNumber = parentNumber;
		this.parentName = parentName;
		this.first = first;
		this.second = second;
		this.third = third;
		this.fourth = fourth;
	}



	public String getFirst() {
		return first;
	}



	public void setFirst(String first) {
		this.first = first;
	}



	public String getSecond() {
		return second;
	}



	public void setSecond(String second) {
		this.second = second;
	}



	public String getThird() {
		return third;
	}



	public void setThird(String third) {
		this.third = third;
	}



	public String getFourth() {
		return fourth;
	}



	public void setFourth(String fourth) {
		this.fourth = fourth;
	}



	public StudentDTO9(int id, String firstName, String lastName, String fullName, String status, String school,
			int grade, ArrayList<ClassroomDTO> listClass, String studentNumber, String parentNumber, String parentName) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.fullName = fullName;
		this.status = status;
		this.school = school;
		this.grade = grade;
		this.listClass = listClass;
		this.studentNumber = studentNumber;
		this.parentNumber = parentNumber;
		this.parentName = parentName;
	}

	

	public StudentDTO9(int id, String firstName, String lastName, String fullName, String status, String school,
			int grade, String studentNumber, String parentNumber, String parentName) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.fullName = fullName;
		this.status = status;
		this.school = school;
		this.grade = grade;
		this.studentNumber = studentNumber;
		this.parentNumber = parentNumber;
		this.parentName = parentName;
	}



	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public String getFirstName() {
		return firstName;
	}



	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}



	public String getLastName() {
		return lastName;
	}



	public void setLastName(String lastName) {
		this.lastName = lastName;
	}



	public String getFullName() {
		return fullName;
	}



	public void setFullName(String fullName) {
		this.fullName = fullName;
	}



	public String getStatus() {
		return status;
	}



	public void setStatus(String status) {
		this.status = status;
	}



	public String getSchool() {
		return school;
	}



	public void setSchool(String school) {
		this.school = school;
	}



	public int getGrade() {
		return grade;
	}



	public void setGrade(int grade) {
		this.grade = grade;
	}

	
	



	public ArrayList<ClassroomDTO> getListClass() {
		return listClass;
	}



	public void setListClass(ArrayList<ClassroomDTO> listClass) {
		this.listClass = listClass;
	}



	public String getStudentNumber() {
		return studentNumber;
	}



	public void setStudentNumber(String studentNumber) {
		this.studentNumber = studentNumber;
	}



	public String getParentNumber() {
		return parentNumber;
	}



	public void setParentNumber(String parentNumber) {
		this.parentNumber = parentNumber;
	}



	public String getParentName() {
		return parentName;
	}



	public void setParentName(String parentName) {
		this.parentName = parentName;
	}



	public StudentDTO9() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return this.id + " " + this.fullName;
	}
	
	@Override
	public int hashCode() {
		
		return this.id;
	}
	
	@Override
	public boolean equals(Object obj) {
		StudentDTO9 student = (StudentDTO9) obj;
		return this.id == student.id;
	}
	
}

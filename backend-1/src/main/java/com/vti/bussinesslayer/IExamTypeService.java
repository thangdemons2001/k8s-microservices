package com.vti.bussinesslayer;

import java.util.List;

import com.vti.entity.ExamType;

public interface IExamTypeService {
		
	
		public void createExamType (ExamType type);
		
		public ExamType findTypeById(int id);
		
		public List<ExamType> getAllExamType();
}

package com.vti.presentationlayer;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vti.bussinesslayer.IChapterService;
import com.vti.bussinesslayer.IClassroomLessonService;
import com.vti.bussinesslayer.IClassroomService;
import com.vti.bussinesslayer.IHomeWorkService;
import com.vti.bussinesslayer.ILessonService;
import com.vti.bussinesslayer.IVideoService;
import com.vti.dto.HomeWorkDTO;
import com.vti.dto.LessonDTO;
import com.vti.dto.LessonDTO3;
import com.vti.entity.Chapter;
import com.vti.entity.Classroom;
import com.vti.entity.ClassroomLesson;
import com.vti.entity.ClassroomLessonKey;
import com.vti.entity.Lesson;
import com.vti.entity.Video;

@RestController
@RequestMapping(value = "api/v1/lessons")
@CrossOrigin("*")
public class LessonController {

	@Autowired
	private ILessonService service;

	@Autowired
	private IVideoService videoSerivce;

	@Autowired
	private IChapterService chapterService;

	@Autowired
	private IHomeWorkService homeWorkSerivce;

	@Autowired
	private IClassroomService classService;

	@Autowired
	private IClassroomLessonService classLessonService;

	@PostMapping("/classes/{classId}")
	public ResponseEntity<?> createLesson(@PathVariable(name = "classId") int classId,
			@RequestBody LessonDTO lessonDto) {

		Lesson lesson = new Lesson();
		lesson.setLessonName(lessonDto.getLessonName());
		lesson.setDate(lessonDto.getDate());
		Chapter chapter = chapterService.getChapterById(lessonDto.getChapterId());
		lesson.setChapter(chapter);
		service.createLesson(lesson);

		List<ClassroomLesson> listClassToAddLesson = new ArrayList<ClassroomLesson>();

		ClassroomLessonKey id = new ClassroomLessonKey(lesson.getId(), classId);

		Classroom clazz = classService.getClassroomById(classId);

		ClassroomLesson addLessonToClass = new ClassroomLesson(id, lesson, clazz);

		listClassToAddLesson.add(addLessonToClass);

		classLessonService.createClassroomLesson(listClassToAddLesson);

		return new ResponseEntity<String>("create successful!", HttpStatus.OK);
	}
	
	
	
	@PostMapping("/exist/classes/{classId}")
	public ResponseEntity<?> addExitsLessonsToClass(@PathVariable(name = "classId") int classId,
			@RequestBody List<Integer> listIds) {

		service.addExistLessonsToClass(classId, listIds);

		return new ResponseEntity<String>("create successful!", HttpStatus.OK);
	}

	@GetMapping("/grade/{grade}/")
	public List<LessonDTO3> getAllLessonOfSubjectAtGrade(@PathVariable(name = "grade") int grade, String subject,
			int page, int pageSize) {

		List<Lesson> lesson = service.getAllLessonInSubjectAtGrade(subject,grade,page,pageSize);
		List<LessonDTO3> res = new ArrayList<LessonDTO3>();
		for (Lesson les : lesson) {
			res.add(les.convertToDTO());
		}

		return res;
	}
	
	@GetMapping("/grade/{grade}/search")
	public List<LessonDTO3> getSearchLessonOfSubjectAtGrade(@PathVariable(name = "grade") int grade, String subject,
			int page, int pageSize, String lessonName) {

		List<Lesson> lesson = service.searchLessonByIdOrByLessonName(subject,grade,lessonName,page,pageSize);
		List<LessonDTO3> res = new ArrayList<LessonDTO3>();
		for (Lesson les : lesson) {
			res.add(les.convertToDTO());
		}

		return res;
	}

	@GetMapping("/classes/{classId}")
	public List<LessonDTO3> getAllLessonInClass(@PathVariable(name = "classId") int classId) {

		List<Lesson> lesson = service.getAllLessonInClass(classId);
		List<LessonDTO3> res = new ArrayList<LessonDTO3>();
		for (Lesson les : lesson) {
			res.add(les.convertToDTO());
		}

		return res;
	}

	@GetMapping("/classes/{classId}/current-lesson")
	public ResponseEntity<?> getLastHomeWorkInClass(@PathVariable(name = "classId") int classId) {

		Lesson currentLesson = service.getCurrentLessonInClass(classId);
		if (currentLesson == null) {
			return new ResponseEntity<String>("empty", HttpStatus.OK);
		}
		if (currentLesson.getHomework() == null) {
			return new ResponseEntity<String>("empty", HttpStatus.OK);
		}
		LessonDTO3 res = currentLesson.convertToDTO();

		return new ResponseEntity<LessonDTO3>(res, HttpStatus.OK);
	}

	@GetMapping("/classes/{classId}/today-lesson")
	public ResponseEntity<?> getLessonInClassToday(@PathVariable(name = "classId") int classId) {

		Lesson todayLesson = service.getLessonInClassToday(classId);
		if (todayLesson == null) {
			return new ResponseEntity<String>("empty", HttpStatus.OK);
		}
		LessonDTO3 res = todayLesson.convertToDTO();

		return new ResponseEntity<LessonDTO3>(res, HttpStatus.OK);
	}

	@PutMapping("/{lessonId}")
	public ResponseEntity<?> updateLesson(@PathVariable(name = "lessonId") int lessonId,
			@RequestBody LessonDTO lessonDto) {

		Lesson lesson = service.getLessonById(lessonId);
		lesson.setLessonName(lessonDto.getLessonName());
		if (lessonDto.getDate() != null) {
			lesson.setDate(lessonDto.getDate());
		}
		if (lessonDto.getChapterId() != 0) {
			Chapter chapter = chapterService.getChapterById(lessonDto.getChapterId());
			lesson.setChapter(chapter);
		}
		if (lessonDto.getVideoId() != 0) {
			Video video = videoSerivce.getVideoById(lessonDto.getVideoId());
			lesson.setVideo(video);
		}
		service.createLesson(lesson);

		return new ResponseEntity<String>("update successful!", HttpStatus.OK);
	}

	@DeleteMapping("/{lessonId}")
	public ResponseEntity<?> deleteLesson(@PathVariable(name = "lessonId") int lessonId) {
		service.deleteLesson(lessonId);
		return new ResponseEntity<String>("delete successful!", HttpStatus.OK);
	}
}

package com.vti.entity;

import java.io.Serializable;


import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

import com.vti.dto.ExamResultDTO3;

@Entity
@Table(name = "ExamResult", catalog = "TCTmanagement")
public class ExamResult implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@EmbeddedId
	private ExamResultKey id;
	
	@ManyToOne
	@MapsId("student_id")
	@JoinColumn(name = "student_id")
	private Student student;
	
	@ManyToOne
	@MapsId("class_id")
	@JoinColumn(name = "class_id")
	private Classroom classroom;
	
	@ManyToOne
	@MapsId("exam_id")
	@JoinColumn(name = "exam_id")
	private Exam exam;
	
	@Column(name = "mark", nullable = false)
	private double mark;

	public ExamResultKey getId() {
		return id;
	}

	public void setId(ExamResultKey id) {
		this.id = id;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public Classroom getClassroom() {
		return classroom;
	}

	public void setClassroom(Classroom classroom) {
		this.classroom = classroom;
	}

	public Exam getExam() {
		return exam;
	}

	public void setExam(Exam exam) {
		this.exam = exam;
	}

	public double getMark() {
		return mark;
	}

	public void setMark(double mark) {
		this.mark = mark;
	}
	
	public ExamResult() {
		// TODO Auto-generated constructor stub
	}

	public ExamResult(ExamResultKey id, Student student, Classroom classroom, Exam exam, double mark) {
		super();
		this.id = id;
		this.student = student;
		this.classroom = classroom;
		this.exam = exam;
		this.mark = mark;
	}
	public ExamResultDTO3 convertToDTO3() {
		return new ExamResultDTO3(this.exam.getName(), this.mark, this.id.getExamId());
	}

	
}

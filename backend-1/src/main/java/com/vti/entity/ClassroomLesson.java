package com.vti.entity;

import java.io.Serializable;


import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;






@Entity
@Table(name = "ClassroomLesson", catalog = "TCTmanagement")
public class ClassroomLesson implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@EmbeddedId
	private ClassroomLessonKey id;
	
	@ManyToOne
	@MapsId("lesson_id")
	@JoinColumn(name = "lesson_id")
	private Lesson lesson;
	
	@ManyToOne
	@MapsId("class_id")
	@JoinColumn(name = "class_id")
	private Classroom classroom;

	public ClassroomLessonKey getId() {
		return id;
	}

	public void setId(ClassroomLessonKey id) {
		this.id = id;
	}

	public Lesson getLesson() {
		return lesson;
	}

	public void setLesson(Lesson lesson) {
		this.lesson = lesson;
	}

	public Classroom getClassroom() {
		return classroom;
	}

	public void setClassroom(Classroom classroom) {
		this.classroom = classroom;
	}
	
	
	public ClassroomLesson() {
		// TODO Auto-generated constructor stub
	}

	public ClassroomLesson(ClassroomLessonKey id, Lesson lesson, Classroom classroom) {
		super();
		this.id = id;
		this.lesson = lesson;
		this.classroom = classroom;
	}
	
	
	
	
}

package com.vti.dto;


import java.util.List;






public class ClassroomDTO {
	
	private int id;
	
	private String className;
	
	private String subjectName;
	
	private int grade;
	
	private TeacherDTO teacher;
	
	private List<ScheduleDTO> listSchedule;
	
	private int totalStudent;

	public int getTotalStudent() {
		return totalStudent;
	}

	public void setTotalStudent(int totalStudent) {
		this.totalStudent = totalStudent;
	}
	
	public TeacherDTO getTeacher() {
		return teacher;
	}

	public void setTeacher(TeacherDTO teacher) {
		this.teacher = teacher;
	}

	public List<ScheduleDTO> getListSchedule() {
		return listSchedule;
	}

	public void setListSchedule(List<ScheduleDTO> listSchedule) {
		this.listSchedule = listSchedule;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public int getGrade() {
		return grade;
	}

	public void setGrade(int grade) {
		this.grade = grade;
	}

	public TeacherDTO getTeacherId() {
		return teacher;
	}

	public void setTeacherId(TeacherDTO teacherId) {
		this.teacher = teacherId;
	}


	public String getSubjectName() {
		return subjectName;
	}

	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}

	
	public ClassroomDTO() {
		// TODO Auto-generated constructor stub
	}

	public ClassroomDTO(int id, String className, String subjectName, int grade, TeacherDTO teacher,
			List<ScheduleDTO> listSchedule, int totalStudent) {
		super();
		this.id = id;
		this.className = className;
		this.subjectName = subjectName;
		this.grade = grade;
		this.teacher = teacher;
		this.listSchedule = listSchedule;
		this.totalStudent = totalStudent;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return this.id + " " + this.grade + this.className;
	}
	

	
}

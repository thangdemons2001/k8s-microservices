package com.vti.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.vti.dto.QuestionDTO;

@Entity
@Table(name = "Questions", catalog = "TCTmanagement")
public class Question implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Column(name = "question_id", nullable = false)
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "homework_id")
	private HomeWork homework;
	
	@Column(name = "score", nullable = false)
	private double score;
	
	@Column(name = "question_type", nullable = false)
	private String type;
	
	@Column(name = "content", nullable = true, length = 8000)
	private String content;
	
	@Column(name = "question_image_link", nullable = true, length = 8000)
	private String imageLink;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public HomeWork getHomework() {
		return homework;
	}

	public void setHomework(HomeWork homework) {
		this.homework = homework;
	}

	public double getScore() {
		return score;
	}

	public void setScore(double score) {
		this.score = score;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getImageLink() {
		return imageLink;
	}

	public void setImageLink(String imageLink) {
		this.imageLink = imageLink;
	}
	
	public Question() {
		// TODO Auto-generated constructor stub
	}

	public Question(long id, HomeWork homework, double score, String type, String content, String imageLink) {
		super();
		this.id = id;
		this.homework = homework;
		this.score = score;
		this.type = type;
		this.content = content;
		this.imageLink = imageLink;
	}
	
	public QuestionDTO convertToDTO() {
		return new QuestionDTO(id, score, type, content, imageLink);
	}
	
}

package com.vti.entity;

import java.io.Serializable;


import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;


@Entity
@Table(name = "ClassroomMentor", catalog = "TCTmanagement")
public class ClassroomMentor implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@EmbeddedId
	private ClassroomMentorKey id;
	
	@ManyToOne
	@MapsId("mentor_id")
	@JoinColumn(name = "mentor_id")
	private Mentor mentor;
	
	@ManyToOne
	@MapsId("class_id")
	@JoinColumn(name = "class_id")
	private Classroom classroom;
	
	public ClassroomMentor() {
		// TODO Auto-generated constructor stub
	}

	public ClassroomMentor(ClassroomMentorKey id, Mentor mentor, Classroom classroom) {
		super();
		this.id = id;
		this.mentor = mentor;
		this.classroom = classroom;
	}

	public ClassroomMentorKey getId() {
		return id;
	}

	public void setId(ClassroomMentorKey id) {
		this.id = id;
	}

	public Mentor getMentor() {
		return mentor;
	}

	public void setMentor(Mentor mentor) {
		this.mentor = mentor;
	}

	public Classroom getClassroom() {
		return classroom;
	}

	public void setClassroom(Classroom classroom) {
		this.classroom = classroom;
	}
	
	
}

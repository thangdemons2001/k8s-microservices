package com.vti.datalayer;





import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;


import com.vti.entity.Mentor;

public interface IMentorRepository extends JpaRepository<Mentor, Integer>, JpaSpecificationExecutor<Mentor> {
		
	public Mentor findById(int id);
		
}

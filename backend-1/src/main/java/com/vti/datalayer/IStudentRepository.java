package com.vti.datalayer;



import java.util.Date;
import java.util.List;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Pageable;
//import java.util.List;
//
//import org.springframework.data.domain.Pageable;
//import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.vti.entity.Student;

public interface IStudentRepository extends JpaRepository<Student, Integer>, JpaSpecificationExecutor<Student> {
		
		
//		public List<Student> findByClassList_Classroom_Id(int id, Pageable pageable, Specification<Student> where);
//		
//		public List<Student> findByClassList_Classroom_Id(int id, Pageable pageable);
		
	
//		@Query("SELECT st FROM STUDENT st WHERE st.id = ?1")
		public Student findById(int id);
		
		@Cacheable(cacheNames = "students", key = "grade")
		public List<Student> findByGrade (int grade);
		
		public Student findByStudentNumber(String number);
		
		public List<Student> findByStatus (String status);
		
		@Query("SELECT COUNT(s) FROM Student s "
				+ "JOIN ClassroomStudent c ON c.id.studentId = s.id  "
				+ "WHERE c.classroom.subject LIKE LOWER(CONCAT('%', ?4, '%')) "
				+ "AND s.grade = ?3 AND s.startDate >= ?1 AND s.startDate <= ?2 GROUP BY s.id")
		public int countNewStudentAtGradeOfSubjectInMonth (Date startDateOfMonth, Date endDateOfMonth, int grade, String subject);
		
		
		@Query("SELECT COUNT(s) FROM Student s "
				+ "WHERE s.grade = ?3 AND s.startDate >= ?1 AND s.startDate <= ?2 ")
		public int countNewStudentAtGradeInMonth (Date startDateOfMonth, Date endDateOfMonth, int grade);
		
		@Query("SELECT COUNT(s) FROM Student s "
				+ "WHERE s.grade = ?2 AND s.startDate <= ?1 ")
		public int countAllStudentAtGradeInMonth (Date endDateOfMonth, int grade);
		
		@Query("SELECT COUNT(s) FROM DeactivedStudent s "
				+ "WHERE s.grade = ?3 AND s.leftDate >= ?1 AND s.leftDate <= ?2 ")
		public int countDeactivedStudentAtGradeInMonth (Date startDateOfMonth, Date endDateOfMonth, int grade);
		
		public List<Student> findAllByGrade(int grade);
		
		public List<Student> findByGrade(int grade, Pageable pageable);
		
		@Transactional
		@Modifying
		@Query("UPDATE Student st SET st.score = st.score + 10 WHERE st.id IN ?1 ")
		public void increaseScoreForMultiHomeWorkSubmissions (List<Integer> studentIds);
		
		@Transactional
		@Modifying
		@Query("UPDATE Student st SET st.score = st.score - 10 WHERE st.id IN ?1 AND st.score > 0 ")
		public void decreaseScoreForMultiHomeWorkSubmitDeletion (List<Integer> studentIds);
		
		@Query("SELECT e.student "
				+ "FROM ExamResult e "
				+ "JOIN Classroom c on c.id = e.classroom.id "
				+ "WHERE c.subject = ?1 AND c.grade = ?2 "
				+ "GROUP BY e.student.id ORDER BY avg(e.mark) DESC")
		public List<Student> findAllByGradeOrderByAvgMarkInSubject(String subjectName, int grade, Pageable pageable);
		
		public List<Student> findByGradeAndStatus (int grade, String status);
		
		
		
		
}

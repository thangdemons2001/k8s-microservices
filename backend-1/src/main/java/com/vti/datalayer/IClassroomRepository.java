package com.vti.datalayer;



import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import com.vti.entity.Classroom;



public interface IClassroomRepository extends JpaRepository<Classroom, Integer>, JpaSpecificationExecutor<Classroom> {
	
		public Classroom findByClassName(String name);
		
		public List<Classroom> findByMentorList_Mentor_Id(int id);
		
		public List<Classroom> findByGrade(int grade);
		
		public Classroom findById(int id);
		
		@Query("FROM Classroom c LEFT JOIN c.listSchedule s WHERE s.schedule = ?1")
		public List<Classroom> findBySchedule(String schedule);
		
		@Query("FROM Classroom c LEFT JOIN c.listSchedule s WHERE s.schedule = ?3 AND c.subject = ?1 AND c.grade = ?2 ")
		public List<Classroom> findBySubjectAndGradeAndSchedule (String subject, int grade, String schedule);
		
		@Query("FROM Classroom c LEFT JOIN c.listSchedule s WHERE s.schedule = ?2 AND c.grade = ?1")
		public List<Classroom> findByGradeAndSchedule (int grade, String schedule);
		
		public List<Classroom> findByGradeAndSubject (int grade, String subject);
}

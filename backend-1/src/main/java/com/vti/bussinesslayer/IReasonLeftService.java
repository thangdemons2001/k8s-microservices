package com.vti.bussinesslayer;

import java.util.List;

import com.vti.entity.ReasonLeft;

public interface IReasonLeftService {
	
	public void createReasonLeft (List<ReasonLeft> dto, int deactivedStudentId);
	
	public void updateDeactivedStudentReasonLeft (int deactivedStudentId, List<ReasonLeft> dto);

}

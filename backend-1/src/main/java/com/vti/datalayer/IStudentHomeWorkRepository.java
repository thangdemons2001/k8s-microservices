package com.vti.datalayer;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.vti.entity.StudentHomeWork;

public interface IStudentHomeWorkRepository
		extends JpaRepository<StudentHomeWork, Long>, JpaSpecificationExecutor<StudentHomeWork> {

	public List<StudentHomeWork> findByLesson_Id(int lessonId);

	public StudentHomeWork findByLesson_IdAndStudent_Id(int lessonId, int studentId);

	@Transactional
	@Modifying
	@Query("DELETE from StudentHomeWork st WHERE st.lesson.id = ?1 AND st.student.id IN ?2 ")
	public void deleteStudentSubmitHomeWorkInListId(int lessonId, List<Integer> studentIds);

	@Query("SELECT DISTINCT st FROM StudentHomeWork st JOIN ClassroomLesson c ON st.lesson.id = c.id.lessonId WHERE c.classroom.subject = ?1 "
			+ "AND c.classroom.grade = ?2 AND st.submitDate BETWEEN ?3 AND ?4 ")
	public List<StudentHomeWork> findByLesson_Classroom_SubjectAndLesson_Classroom_GradeAndSubmitDateBetween(
			String subject, int grade, Date firstDayOfMonth, Date lastDayOfMonth);

	public List<StudentHomeWork> findByStudent_IdAndSubmitDateAfter(int studentId, Date date);

	@Transactional
	@Modifying
	@Query("DELETE from StudentHomeWork WHERE submitDate < ?1")
	public void deleteAllSubmitBeforeDate(Date date);
}

package com.vti.dto;



public class TeacherDTO {
	
	public int getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(int teacherId) {
		this.teacherId = teacherId;
	}

	private int teacherId;
	
	private String firstName;
	
	private String lastName;
		
	private String fullName;
	
	private String subjectName;
	
	
	

	public TeacherDTO(int teacherId, String firstName, String lastName, String fullName, String subjectName) {
		super();
		this.teacherId = teacherId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.fullName = fullName;
		this.subjectName = subjectName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getSubjectName() {
		return subjectName;
	}

	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}

	

	
	
	public TeacherDTO() {
		// TODO Auto-generated constructor stub
	}

	public TeacherDTO(String firstName, String lastName, String fullName, String subjectName
			) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.fullName = fullName;
		this.subjectName = subjectName;
	
	}
	
	
}

package com.vti.bussinesslayer;

import java.util.List;

import com.vti.entity.DeactivedStudent;

public interface IDeactivedStudentService {
	
	public List<DeactivedStudent> getAllDeactivedStudentInGrade (int grade);
	
	public void createDeactiveStudent (DeactivedStudent dto);
	
	public void updateDeactiveStudent  (int studentId, DeactivedStudent dto);
	
	public void studentComeBack (int studentId);

}

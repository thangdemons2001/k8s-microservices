package com.vti.bussinesslayer;


import org.springframework.security.core.userdetails.UserDetailsService;

import com.vti.dto.ConnectTestingSystemDTO;
import com.vti.entity.User;

public interface IUserService extends UserDetailsService {
	public User findByUserName(String name);

	public boolean existsUserByUserName(String userName);
	
	public void connectToTestingSystem (ConnectTestingSystemDTO dto);
	
	public User getUserById (int userId);
	
	public void deleteUser (int userId);
	
	public void updateStatusUser (int userId, String status);
}

package com.vti.bussinesslayer;




import java.util.List;

import com.vti.entity.Classroom;
import com.vti.entity.Student;



public interface IClassroomService {
	
		public Classroom getClassroomByName(String name);
		
		public List<Classroom> getAllClassroom();
		
		public List<Classroom> getListClassroomByMentorId(int id);
		
		public List<Classroom> getListClassInGrade(int grade);
		
		public void createClass(Classroom clazz);
		
		public void updateClass(Classroom clazz);
		
		public void deleteClass(int classId);
		
		public List<Classroom> getListClassroomBySchedule(String schedule);
		
		public List<Student> getListStudentInClass(int classId);
		
		public List<Classroom> getListClassroomWithSameTypeToday(String subject, int grade, String schedule);
		
		public List<Classroom> getListClassroomByScheduleAndGrade (int grade, String schedule);
		
		public List<Classroom> getListClassroomInGradeAndSubject (int grade, String subject);
		
		public Classroom getClassroomById (int classId);
}

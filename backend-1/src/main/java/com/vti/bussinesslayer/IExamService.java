package com.vti.bussinesslayer;

import java.util.List;

import com.vti.dto.ExamResultDTO2;
import com.vti.entity.Exam;

public interface IExamService {
	public void createExam (Exam exam);
	
	public void deleteExam (int examId);
	
	public List<ExamResultDTO2> findAllSubjectExamInMonthAtGrade(int month, String subject, int grade);
	
	
}

package com.vti.bussinesslayer;

import java.util.List;

import com.vti.entity.StudentComment;

public interface IStudentCommentService {
	
	public void createStudentComment (StudentComment comment);

	public List<StudentComment> getLastTenStudentCommentOfStudent (int studentId);
}

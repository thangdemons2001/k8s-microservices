package com.vti.datalayer;

import java.util.Date;
import java.util.List;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.vti.entity.Attendance;
import com.vti.entity.AttendanceKey;

public interface IAttenanceRepository
		extends JpaRepository<Attendance, AttendanceKey>, JpaSpecificationExecutor<Attendance> {

	public List<Attendance> findById_ClassroomIdAndId_DateIdAndStatus(int classId, Date dateId, String status);

	public Attendance findFirstById_StudentIdAndId_ClassroomIdOrderById_DateIdDesc(int studentId, int classId);

	public List<Attendance> findById_DateIdAndClassroom_GradeAndClassroom_SubjectAndStatus(Date dateId, int grade,
			String subject, String status);
	
	public List<Attendance> findById_ClassroomIdOrderById_DateIdDesc(int classId);
	
	public Attendance findById_ClassroomIdAndId_DateIdAndId_StudentId (int classId, Date dateId, int studentId);
	
	public int countById_ClassroomIdAndId_DateId (int classId, Date dateId);
	
	
	@Transactional
	@Modifying
	@Query("DELETE from Attendance WHERE date < ?1")
	public void deleteAllAttendanceBeforeDate (Date date);
}

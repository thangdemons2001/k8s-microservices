package com.vti.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

import com.vti.dto.ClassroomStudentDTO;




@Entity
@Table(name = "ClassroomStudent", catalog = "TCTmanagement")
public class ClassroomStudent implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@EmbeddedId
	private ClassroomStudentKey id;
	
	@ManyToOne
	@MapsId("student_id")
	@JoinColumn(name = "student_id")
	private Student student;
	
	@ManyToOne
	@MapsId("class_id")
	@JoinColumn(name = "class_id")
	private Classroom classroom;
	
	@Column(name = "status_boolean", nullable = false)
	private String status;
	
	@Column(name = "status_boolean_sub", nullable = false)
	private String subStatus;
	
	
	
	public String getSubStatus() {
		return subStatus;
	}

	public void setSubStatus(String subStatus) {
		this.subStatus = subStatus;
	}

	public ClassroomStudent(ClassroomStudentKey id, Student student, Classroom classroom, String status) {
		super();
		this.id = id;
		this.student = student;
		this.classroom = classroom;
		this.status = status;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public ClassroomStudent() {
		// TODO Auto-generated constructor stub
	}

	public ClassroomStudentKey getId() {
		return id;
	}

	public void setId(ClassroomStudentKey id) {
		this.id = id;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public Classroom getClassroom() {
		return classroom;
	}

	public void setClassroom(Classroom classroom) {
		this.classroom = classroom;
	}
	
	public ClassroomStudentDTO convertToDTO() {
		return new ClassroomStudentDTO(id.getClassroomId(), id.getStudentId());
	}
	
}

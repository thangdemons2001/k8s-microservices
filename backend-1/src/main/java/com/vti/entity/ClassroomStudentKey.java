package com.vti.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
@Embeddable
public class ClassroomStudentKey implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Column(name = "student_id" , nullable = false)
	private int studentId;
	
	@Column(name = "class_id" , nullable = false)
	private int classroomId;

	public int getStudentId() {
		return studentId;
	}

	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}

	public int getClassroomId() {
		return classroomId;
	}

	public void setClassroomId(int classroomId) {
		this.classroomId = classroomId;
	}
	public ClassroomStudentKey() {
		// TODO Auto-generated constructor stub
	}

	public ClassroomStudentKey(int studentId, int classroomId) {
		super();
		this.studentId = studentId;
		this.classroomId = classroomId;
	}
}

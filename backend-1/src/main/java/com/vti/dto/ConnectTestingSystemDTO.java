package com.vti.dto;

public class ConnectTestingSystemDTO {
	
	private int userId;
	
	private String testingSystemId;
	
	private String facebookUrl;

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getTestingSystemId() {
		return testingSystemId;
	}

	public void setTestingSystemId(String testingSystemId) {
		this.testingSystemId = testingSystemId;
	}

	public String getFacebookUrl() {
		return facebookUrl;
	}

	public void setFacebookUrl(String facebookUrl) {
		this.facebookUrl = facebookUrl;
	}

	public ConnectTestingSystemDTO(int userId, String testingSystemId, String facebookUrl) {
		super();
		this.userId = userId;
		this.testingSystemId = testingSystemId;
		this.facebookUrl = facebookUrl;
	}
	
	public ConnectTestingSystemDTO() {
		// TODO Auto-generated constructor stub
	}

}

package com.vti.dto;

import java.util.Comparator;

public class SortByAvgMark implements Comparator<StudentDTO7> {

	@Override
	public int compare(StudentDTO7 o1, StudentDTO7 o2) {
		// TODO Auto-generated method stub
		double mark1 = o1.getAvgMark();
		double mark2 = o2.getAvgMark();

        if (mark1 == mark2)
            return 0;
        else if (mark1 > mark2)
            return 1;
        else
            return -1;
	}

}

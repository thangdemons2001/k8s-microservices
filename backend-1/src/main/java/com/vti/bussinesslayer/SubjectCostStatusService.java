package com.vti.bussinesslayer;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vti.datalayer.ISubjectCostStatusRepository;
import com.vti.entity.SubjectCostStatus;

@Service
public class SubjectCostStatusService implements ISubjectCostStatusService {
	
	@Autowired
	private ISubjectCostStatusRepository repository;

	@Override
	public SubjectCostStatus getCostStatusOfSubjectInGrade(String subject, int grade) {
		
		return repository.findBySubjectAndGrade(subject, grade);
	}

	@Override
	public void updateCostStatusOfSubjectInGrade(SubjectCostStatus dto) {
		repository.save(dto);
	}

	@Override
	public void createSubjectCostStatus(SubjectCostStatus dto) {
		repository.save(dto);
	}

	@Override
	public List<SubjectCostStatus> findAllCostStatusRequirementAtGrade(int grade) {
		return repository.findByGrade(grade);
	}

}

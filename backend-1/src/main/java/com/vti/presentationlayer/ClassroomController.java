package com.vti.presentationlayer;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vti.bussinesslayer.IAttendanceService;
import com.vti.bussinesslayer.IClassroomMentorService;
import com.vti.bussinesslayer.IClassroomService;
import com.vti.bussinesslayer.IClassroomStudentService;
import com.vti.bussinesslayer.ISubAttendanceService;
import com.vti.bussinesslayer.ISubjectCostStatusService;
import com.vti.bussinesslayer.ITeacherService;
import com.vti.dto.AttendanceDTO2;
import com.vti.dto.ClassroomDTO;
import com.vti.dto.ClassroomDTO2;
import com.vti.dto.ClassroomDTO4;
import com.vti.dto.ClassroomDTO7;
import com.vti.dto.ClassroomMentorDTO;
import com.vti.dto.ClassroomStudentDTO;
import com.vti.dto.StudentDTO;
import com.vti.dto.StudentDTO10;
import com.vti.dto.StudentDTO2;

import com.vti.dto.StudentDTO6;
import com.vti.dto.StudentDTO8;
import com.vti.entity.Attendance;
import com.vti.entity.Classroom;
import com.vti.entity.ClassroomStudent;
import com.vti.entity.Schedule;
import com.vti.entity.Student;
import com.vti.entity.SubAttendance;
import com.vti.entity.SubjectCostStatus;
import com.vti.entity.Teacher;

@RestController
@RequestMapping(value = "api/v1/classes")
@CrossOrigin("*")
public class ClassroomController {

	@Autowired
	private IClassroomService service;

	@Autowired
	private IClassroomMentorService classSerivce;

	@Autowired
	private IClassroomStudentService classStService;

	@Autowired
	private IAttendanceService attenService;

	@Autowired
	private ISubAttendanceService subAttendance;

	@Autowired
	private ITeacherService teacherService;
	
	@Autowired
	private ISubjectCostStatusService costStatusService;

	@GetMapping("/")
	public List<ClassroomDTO4> getAllClass() {

		List<ClassroomDTO4> res = new ArrayList<ClassroomDTO4>();

		List<Classroom> listClass = service.getAllClassroom();

		for (Classroom clazz : listClass) {
			res.add(clazz.convertToDTO4());
		}

		return res;
	}
	

	@GetMapping("/cost-status")
	public ResponseEntity<?> getCostStatusOfSubjectInGrade(int grade, String subject) {

		SubjectCostStatus costStatus = costStatusService.getCostStatusOfSubjectInGrade(subject, grade);

		return new ResponseEntity<SubjectCostStatus>(costStatus, HttpStatus.OK);
	}
	
	@GetMapping("/grade/{grade}/cost-status/enable")
	public ResponseEntity<?> enableSubCostStatusForMainStatus(@PathVariable(name = "grade") int grade, String subject) {
		
		SubjectCostStatus costStatus = costStatusService.getCostStatusOfSubjectInGrade(subject, grade);
		costStatus.setLastModify(new Date());
		costStatus.setCostStatus("active");
		costStatusService.updateCostStatusOfSubjectInGrade(costStatus);
		classStService.enableSubCostStatusForMainStauts(subject, grade);

		return new ResponseEntity<String>("update successful!", HttpStatus.OK);
	}
	
	
	@PostMapping("/cost-status/")
	public ResponseEntity<?> createSubjectCostStatusRequirment(@RequestBody SubjectCostStatus dto) {
		costStatusService.createSubjectCostStatus(dto);
		return new ResponseEntity<String>("create successful!", HttpStatus.OK);
	}
	
	
	@GetMapping("/cost-status/grade/{grade}")
	public ResponseEntity<?> getAllCostStatusRequirementInGrade(@PathVariable(name = "grade") int grade) {
		
		List<SubjectCostStatus> res = costStatusService.findAllCostStatusRequirementAtGrade(grade);

		return new ResponseEntity<List<SubjectCostStatus>>(res, HttpStatus.OK);
	}
	
	
	
	@PutMapping("/{classId}/mentors")
	public ResponseEntity<?> updateClassMentor(@PathVariable(name = "classId") int classId,
			@RequestBody List<ClassroomMentorDTO> listClass) {

		classSerivce.updateMentorClass(classId, listClass);

		return new ResponseEntity<String>("update successful!", HttpStatus.OK);
	}

	@PutMapping("/{classId}")
	public ResponseEntity<?> updateClass(@PathVariable(name = "classId") int classId,
			@RequestBody Classroom classroom) {

		Classroom clazz = service.getClassroomById(classId);
		clazz.setGrade(classroom.getGrade());
		clazz.setSubject(classroom.getSubject());
		Teacher teacher = teacherService.findTeacherById(classroom.getTeacherId().getId());
		clazz.setTeacherId(teacher);
		clazz.setClassName(classroom.getClassName());
		clazz.setStatus("active");

		service.updateClass(clazz);
		return new ResponseEntity<String>("update successful!", HttpStatus.OK);
	}

	@GetMapping("/{classId}")
	public ResponseEntity<?> getClassroomById(@PathVariable(name = "classId") int classId) {
		Classroom clazz = service.getClassroomById(classId);
		ClassroomDTO4 res = clazz.convertToDTO4();
		return new ResponseEntity<ClassroomDTO4>(res, HttpStatus.OK);
	}

	@GetMapping("/grade/{grade}/students")
	public List<StudentDTO10> getAllStudentCostStatusInfoInClass(@PathVariable(name = "grade") int grade,
			String subject, String status) {

		List<StudentDTO10> res = new ArrayList<StudentDTO10>();

		List<ClassroomStudent> listClass = classStService.getListStudentInGradeAndSubject(grade, subject, status);

		Map<StudentDTO10, ArrayList<ClassroomDTO>> mapStudentClassroom = new HashMap<StudentDTO10, ArrayList<ClassroomDTO>>();

		for (ClassroomStudent clazz : listClass) {

			String clazzStatus = clazz.getStatus();

			Student st = clazz.getStudent();
			Classroom classroom = clazz.getClassroom();
			StudentDTO10 student = new StudentDTO10(st.getId(), st.getFirstName(), st.getLastName(), st.getFullName(),
					clazzStatus, st.getSchool(), grade, st.getStudentNumber(), st.getParentNumber(), st.getParentName(),
					st.getAvatarUrl(), st.getFacebookUrl(),clazz.getSubStatus());

			if (mapStudentClassroom.get(student) == null) {
				mapStudentClassroom.put(student, new ArrayList<ClassroomDTO>());
			}
			mapStudentClassroom.get(student).add(classroom.convertToDTO());
		}
		for (Map.Entry<StudentDTO10, ArrayList<ClassroomDTO>> entry : mapStudentClassroom.entrySet()) {

			entry.getKey().setListClass(entry.getValue());
			res.add(entry.getKey());
		}

		return res;
	}

	@GetMapping("/grade/{grade}/subjects/")
	public List<ClassroomDTO2> getAllSubjectClassInGrade(@PathVariable(name = "grade") int grade, String subject) {

		List<ClassroomDTO2> res = new ArrayList<ClassroomDTO2>();

		List<Classroom> listClass = service.getListClassroomInGradeAndSubject(grade, subject);

		for (Classroom clazz : listClass) {
			res.add(clazz.convertToDTO2());
		}

		return res;
	}

	@PutMapping("/students/{studentId}/cost-info")
	public ResponseEntity<?> updateStudentClassCostInfo(@RequestBody ClassroomStudentDTO status, String subject,
			@PathVariable(name = "studentId") int studentId) {
		classStService.updateStudentClassSubjectStatus(studentId, subject, status.getStatus(), status.getMainStatus());
		return new ResponseEntity<String>("update successful!", HttpStatus.OK);
	}

	@PostMapping("/")
	public ResponseEntity<?> createClass(@RequestBody Classroom classroom) {
		service.createClass(classroom);
		return new ResponseEntity<Integer>(classroom.getId(), HttpStatus.OK);
	}

	@DeleteMapping("/{classId}")
	public ResponseEntity<?> deleteClass(@PathVariable(name = "classId") int classId) {
		service.deleteClass(classId);
		return new ResponseEntity<String>("delete successful!", HttpStatus.OK);
	}

	@PostMapping("/{classId}/mentors")
	public ResponseEntity<?> createMentorClass(@PathVariable(name = "classId") int classId,
			@RequestBody List<ClassroomMentorDTO> listClass) {
		classSerivce.createMentorClass(classId, listClass);
		return new ResponseEntity<String>("create successful!", HttpStatus.OK);
	}

	@GetMapping("/grade/{grade}")
	public List<ClassroomDTO> getAllClassInGrade(@PathVariable(name = "grade") int grade) {

		List<ClassroomDTO> res = new ArrayList<ClassroomDTO>();

		List<Classroom> listClass = service.getListClassInGrade(grade);

		for (Classroom clazz : listClass) {
			res.add(clazz.convertToDTO1());
		}

		return res;
	}

	@GetMapping("/grade/{grade}/schedule/{weeklyday}")
	public List<ClassroomDTO> getAllClassInGradeAndSchedule(@PathVariable(name = "grade") int grade,
			@PathVariable(name = "weeklyday") String dayInNumber) {

		List<ClassroomDTO> res = new ArrayList<ClassroomDTO>();

		List<Classroom> listClass = service.getListClassroomByScheduleAndGrade(grade, dayInNumber);

		for (Classroom clazz : listClass) {
			res.add(clazz.convertToDTO1());
		}

		return res;
	}

	@GetMapping("/schedule/{weeklyday}")
	public List<ClassroomDTO7> getAllClassToDay(@PathVariable(name = "weeklyday") String dayInNumber) {
//		sunday is 1 moday is 2 ...

		List<ClassroomDTO7> res = new ArrayList<ClassroomDTO7>();

		List<Classroom> listClass = service.getListClassroomBySchedule(dayInNumber);

		for (Classroom clazz : listClass) {
			for (Schedule schedule : clazz.getListSchedule()) {
				if (schedule.getSchedule().equals(dayInNumber)) {
					res.add(clazz.convertToDTO7(schedule.getSchedule(), schedule.getStartTime(),
							schedule.getEndTime()));
					break;
				}
			}

		}

		return res;
	}

	// Lấy ra các lớp học có cùng kiểu grade{grade}, subject {subject} để học bù
	// trong ngày hôm nay tại thời điểm điểm danh
	@GetMapping("/schedule/{weeklyday}/grade/{grade}")
	public List<ClassroomDTO2> getAllClassWithSameTypeToDay(@PathVariable(name = "weeklyday") String dayInNumber,
			@PathVariable(name = "grade") int grade, String subjectName) throws ParseException {
//		sunday is 1 moday is 2 ...
		// only take time to compare
		Date date = Calendar.getInstance().getTime();
		DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
		String now = dateFormat.format(date);
		// get today without year month day
		Date today = new SimpleDateFormat("HH:mm:ss").parse(now);

		List<ClassroomDTO2> res = new ArrayList<ClassroomDTO2>();

		List<Classroom> listClass = service.getListClassroomWithSameTypeToday(subjectName, grade, dayInNumber);
		Calendar c = Calendar.getInstance();
		for (Classroom clazz : listClass) {
			boolean isExist = false;
			for (Schedule schedule : clazz.getListSchedule()) {
				// convert date -> string, string -> date to compare time with Date now
				String startString = dateFormat.format(schedule.getStartTime());
				Date startTime = new SimpleDateFormat("HH:mm:ss").parse(startString);
				// minus startTime to 30 mins
				c.setTime(startTime);
				c.add(Calendar.YEAR, 0);
				c.add(Calendar.MONTH, 0);
				c.add(Calendar.DATE, 0);
				c.add(Calendar.HOUR, 0);
				c.add(Calendar.MINUTE, -30);
				c.add(Calendar.SECOND, 0);
				// Starting attendance time
				Date startringAttendance = c.getTime();
				// add startTime to 30 minus
				c.add(Calendar.MINUTE, 60);
				// ending attendance time
				Date endingAttendance = c.getTime();
				if (schedule.getSchedule().equals(dayInNumber) && today.after(startringAttendance)
						&& today.before(endingAttendance)) {
					isExist = true;
					break;
				}
			}
			if (isExist) {
				res.add(clazz.convertToDTO2());
			}
		}

		return res;
	}

	@GetMapping("/{classId}/students/")
	public List<StudentDTO> getAllStudentInClass(@PathVariable(name = "classId") int classId) {

		List<Student> listSt = service.getListStudentInClass(classId);

		List<StudentDTO> res = new ArrayList<StudentDTO>();

		for (Student st : listSt) {
			res.add(st.convertToDTO2());
		}

		return res;
	}

	// lấy danh sách học sinh đi học của lớp {classId} trong ngày {date}
	@GetMapping("/attended/classes/{classId}/")
	public List<StudentDTO2> getAllStudentAttendanceInClassToday(@PathVariable(name = "classId") int classId,
			String date) throws ParseException {
		List<StudentDTO2> res = new ArrayList<StudentDTO2>();

		List<Attendance> listAttendanceInClassToday = attenService.getAllStudentAttendanceInClassToday(classId,
				new SimpleDateFormat("yyyy-MM-dd").parse(date));
		for (Attendance atten : listAttendanceInClassToday) {
			res.add(atten.getStudent().convertToDTO3(atten.getStatus()));
		}

		List<SubAttendance> listSubAttendanceInClassToday = subAttendance.getListSubAttendanceInClassToday(classId,
				new SimpleDateFormat("yyyy-MM-dd").parse(date));
		for (SubAttendance atten : listSubAttendanceInClassToday) {
			res.add(atten.getStudent().convertToDTO3(atten.getStatus()));
		}
		return res;
	}

}

package com.vti.bussinesslayer;

import com.vti.entity.HomeWork;



public interface IHomeWorkService {
	
	public void createHomeWork (HomeWork homework);
	
	public HomeWork getLessonHomeWorkByLessonId (int id);
	
	public HomeWork getHomeWorkById (int id);

}

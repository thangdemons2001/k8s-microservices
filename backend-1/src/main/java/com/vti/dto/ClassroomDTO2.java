package com.vti.dto;

import java.util.Date;
import java.util.List;

import com.vti.entity.Classroom;




public class ClassroomDTO2 {
	
	private int id;
	
	private String className;
	
	private String subjectName;
	
	private int grade;
	
	private TeacherDTO teacher;
	
	private List<ScheduleDTO> listSchedule;
	
	private List<MentorDTO> listMentor;
	
	public List<MentorDTO> getListMentor() {
		return listMentor;
	}

	public void setListMentor(List<MentorDTO> listMentor) {
		this.listMentor = listMentor;
	}

	public int getTotalStudent() {
		return totalStudent;
	}

	public void setTotalStudent(int totalStudent) {
		this.totalStudent = totalStudent;
	}
	private int totalStudent;
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public int getGrade() {
		return grade;
	}

	public void setGrade(int grade) {
		this.grade = grade;
	}

	public TeacherDTO getTeacherId() {
		return teacher;
	}

	public void setTeacherId(TeacherDTO teacherId) {
		this.teacher = teacherId;
	}


	public TeacherDTO getTeacher() {
		return teacher;
	}

	public void setTeacher(TeacherDTO teacher) {
		this.teacher = teacher;
	}

	public List<ScheduleDTO> getListSchedule() {
		return listSchedule;
	}

	public void setListSchedule(List<ScheduleDTO> listSchedule) {
		this.listSchedule = listSchedule;
	}

	public String getSubjectName() {
		return subjectName;
	}

	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}

	
	public ClassroomDTO2() {
		// TODO Auto-generated constructor stub
	}

	public ClassroomDTO2(int id, String className, String subjectName, int grade, TeacherDTO teacher,
			List<ScheduleDTO> listSchedule, List<MentorDTO> listMentor, int totalStudent) {
		super();
		this.id = id;
		this.className = className;
		this.subjectName = subjectName;
		this.grade = grade;
		this.teacher = teacher;
		this.listSchedule = listSchedule;
		this.listMentor = listMentor;
		this.totalStudent = totalStudent;
	}

	public ClassroomDTO2(int id, String className, String subjectName, int grade, TeacherDTO teacher,
			List<ScheduleDTO> listSchedule) {
		super();
		this.id = id;
		this.className = className;
		this.subjectName = subjectName;
		this.grade = grade;
		this.teacher = teacher;
		this.listSchedule = listSchedule;
	}

	public ClassroomDTO2(int id, String className, String subjectName, int grade, TeacherDTO teacher,
			List<ScheduleDTO> listSchedule, int totalStudent) {
		super();
		this.id = id;
		this.className = className;
		this.subjectName = subjectName;
		this.grade = grade;
		this.teacher = teacher;
		this.listSchedule = listSchedule;
		this.totalStudent = totalStudent;
	}

	
	
	

}

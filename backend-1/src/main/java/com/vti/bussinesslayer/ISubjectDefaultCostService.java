package com.vti.bussinesslayer;

import java.util.List;

import com.vti.entity.SubjectDefaultCost;

public interface ISubjectDefaultCostService {
	
	public void createDefaultCost (SubjectDefaultCost dto);
	
	public void updateDefaultCost (int id, SubjectDefaultCost dto);
	
	public List<SubjectDefaultCost> getAllDefaultCost ();
	
	public SubjectDefaultCost getDefaultCostOfSubjectInGrade (String subject, int grade);
}

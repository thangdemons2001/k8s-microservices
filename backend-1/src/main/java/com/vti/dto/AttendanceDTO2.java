package com.vti.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class AttendanceDTO2  {
	
	private String status;
	
	@JsonFormat(pattern = "dd-MM-yyyy")
	private Date date;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	
	public AttendanceDTO2() {
		// TODO Auto-generated constructor stub
	}

	public AttendanceDTO2(String status, Date date) {
		super();
		this.status = status;
		this.date = date;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return this.status + " " + this.date.toString();
	}

	
	
}

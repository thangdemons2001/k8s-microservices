package com.vti.datalayer;





import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.vti.entity.ClassroomLesson;
import com.vti.entity.ClassroomLessonKey;
import com.vti.entity.ClassroomStudent;
import com.vti.entity.ClassroomStudentKey;

public interface IClassroomLessonRepository extends JpaRepository<ClassroomLesson, ClassroomLessonKey>, JpaSpecificationExecutor<ClassroomStudent> {
		
	
		
		
}

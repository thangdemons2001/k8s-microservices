package com.vti.bussinesslayer;

import java.util.List;

import com.vti.entity.Schedule;

public interface IScheduleService {
	
	public void createSchedule (List<Schedule> schedule, int classId);
	
	public void updateSchedule (int classId, List<Schedule> dto);
	
	public void deleteSchedule (List<Integer> listId);

}

package com.vti.dto;



public class ExamResultDTO  {
	
	private int examId;
	
	private int studentId;
	
	private int classroomId;
	
	private double mark;

	public int getExamId() {
		return examId;
	}

	public void setExamId(int examId) {
		this.examId = examId;
	}

	public int getStudentId() {
		return studentId;
	}

	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}

	public int getClassroomId() {
		return classroomId;
	}

	public void setClassroomId(int classroomId) {
		this.classroomId = classroomId;
	}

	public double getMark() {
		return mark;
	}

	public void setMark(double mark) {
		this.mark = mark;
	}
	
	public ExamResultDTO() {
		// TODO Auto-generated constructor stub
	}

	public ExamResultDTO(int examId, int studentId, int classroomId, double mark) {
		super();
		this.examId = examId;
		this.studentId = studentId;
		this.classroomId = classroomId;
		this.mark = mark;
	}
	
	
	
}

package com.vti.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class UserCommentDTO {
	
	private int commentId;
	
	private String fullName;
	
	private String role;
	
	private String comment;
	
	@JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
	private Date commentDate;
	
	private String avatarUrl;
	
	private String facebookUrl;
	
	

	public String getFacebookUrl() {
		return facebookUrl;
	}

	public void setFacebookUrl(String facebookUrl) {
		this.facebookUrl = facebookUrl;
	}

	public String getAvatarUrl() {
		return avatarUrl;
	}

	public void setAvatarUrl(String avatarUrl) {
		this.avatarUrl = avatarUrl;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public int getCommentId() {
		return commentId;
	}

	public void setCommentId(int commentId) {
		this.commentId = commentId;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Date getCommentDate() {
		return commentDate;
	}

	public void setCommentDate(Date commentDate) {
		this.commentDate = commentDate;
	}
	
	public UserCommentDTO() {
		// TODO Auto-generated constructor stub
	}
	
	
	
	public UserCommentDTO(int commentId, String fullName, String role, String comment, Date commentDate,
			String avatarUrl, String facebookUrl) {
		super();
		this.commentId = commentId;
		this.fullName = fullName;
		this.role = role;
		this.comment = comment;
		this.commentDate = commentDate;
		this.avatarUrl = avatarUrl;
		this.facebookUrl = facebookUrl;
	}

	public UserCommentDTO(int commentId, String fullName, String comment, Date commentDate) {
		super();
		this.commentId = commentId;
		this.fullName = fullName;
		this.comment = comment;
		this.commentDate = commentDate;
	}

	public UserCommentDTO(int commentId, String fullName, String role, String comment, Date commentDate) {
		super();
		this.commentId = commentId;
		this.fullName = fullName;
		this.role = role;
		this.comment = comment;
		this.commentDate = commentDate;
	}

	public UserCommentDTO(int commentId, String fullName, String role, String comment, Date commentDate,
			String avatarUrl) {
		super();
		this.commentId = commentId;
		this.fullName = fullName;
		this.role = role;
		this.comment = comment;
		this.commentDate = commentDate;
		this.avatarUrl = avatarUrl;
	}
	
	

}

package com.vti.bussinesslayer;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vti.datalayer.IExamResultRepository;
import com.vti.datalayer.IExamTypeRepository;
import com.vti.entity.ExamResult;
import com.vti.entity.ExamType;



@Service
public class ExamTypeSerivce implements IExamTypeService{
		
		@Autowired
		private IExamTypeRepository repository;

		@Override
		public void createExamType(ExamType type) {
			repository.save(type);
		}

		@Override
		public ExamType findTypeById(int id) {
			
			return repository.getById(id);
		}

		@Override
		public List<ExamType> getAllExamType() {
			return repository.findAll();
		}

		

		
		
	
}

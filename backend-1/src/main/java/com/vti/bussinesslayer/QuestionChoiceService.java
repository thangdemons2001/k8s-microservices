package com.vti.bussinesslayer;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vti.datalayer.IQuestionChoiceRepository;
import com.vti.datalayer.IQuestionRepository;
import com.vti.entity.Question;
import com.vti.entity.QuestionChoice;

@Service
public class QuestionChoiceService implements IQuestionChoiceService {
	
	@Autowired
	private IQuestionChoiceRepository repository;
	
	@Autowired
	private IQuestionRepository questionRepository;

	@Override
	public void createQuestionChoice(long questionId, List<QuestionChoice> choices) {
		Question question = questionRepository.getById(questionId);
		for(QuestionChoice choice: choices) {
			choice.setQuestion(question);
		}
		repository.saveAll(choices);
	}

	@Override
	public void deleteQuestionChoice(long questionChoiceId) {
		QuestionChoice choice = repository.getById(questionChoiceId);
		repository.delete(choice);
	}

	@Override
	public void updateQuestionChoice(QuestionChoice dto) {
		QuestionChoice choice = repository.getById(dto.getId());
		if(dto.getContent() != null) {
			choice.setContent(dto.getContent());
		}
		if(dto.getImageLink() != null) {
			choice.setImageLink(dto.getImageLink());
		}
		if(dto.getIsCorrect() != null) {
			choice.setIsCorrect(dto.getIsCorrect());
		}
		
	}

	@Override
	public List<QuestionChoice> getAllQuestionChoice(long questionId) {
		return repository.findByQuestion_Id(questionId);
	}

}

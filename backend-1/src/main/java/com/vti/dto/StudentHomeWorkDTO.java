package com.vti.dto;

import java.util.Date;

public class StudentHomeWorkDTO {
	
	
	private String status;
	
	private int id;
	
	private String name;
	
	private String link;
	
	private String keyLink;
	
	private String misson;
	
	private int quizzId;
	
	
	private Date date;
	
	
	
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getKeyLink() {
		return keyLink;
	}

	public void setKeyLink(String keyLink) {
		this.keyLink = keyLink;
	}

	public String getMisson() {
		return misson;
	}

	public void setMisson(String misson) {
		this.misson = misson;
	}

	public int getQuizzId() {
		return quizzId;
	}

	public void setQuizzId(int quizzId) {
		this.quizzId = quizzId;
	}
	
	public StudentHomeWorkDTO() {
		// TODO Auto-generated constructor stub
	}

	public StudentHomeWorkDTO(String status, int id, String name, String link, String keyLink, String misson,
			int quizzId) {
		super();
		this.status = status;
		this.id = id;
		this.name = name;
		this.link = link;
		this.keyLink = keyLink;
		this.misson = misson;
		this.quizzId = quizzId;
	}

	public StudentHomeWorkDTO(String status, int id, String name, String link, String keyLink, String misson,
			int quizzId, Date date) {
		super();
		this.status = status;
		this.id = id;
		this.name = name;
		this.link = link;
		this.keyLink = keyLink;
		this.misson = misson;
		this.quizzId = quizzId;
		this.date = date;
	}
	

}

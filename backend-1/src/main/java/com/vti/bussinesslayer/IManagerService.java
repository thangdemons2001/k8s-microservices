package com.vti.bussinesslayer;

public interface IManagerService {
	
	public void resetMonth ();
	
	public void resetCostStatusOfStatusInSubjectInGrade (String subject, int grade);
}

package com.vti.bussinesslayer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vti.datalayer.IClassroomRepository;
import com.vti.datalayer.IClassroomStudentRepository;

import com.vti.dto.ClassroomStudentDTO;

import com.vti.entity.Classroom;
import com.vti.entity.ClassroomStudent;
import com.vti.entity.ClassroomStudentKey;

@Service
public class ClassroomStudentService implements IClassroomStudentService{
		
	@Autowired
	private IClassroomStudentRepository repository;
	
	@Autowired
	private IClassroomRepository classroomRepository;

	@Override
	public List<Classroom> findStudentClass(int studentId) {
		List<ClassroomStudent> classStudent = repository.findById_StudentIdAndStatus(studentId, "active");
		
		List<Classroom> res = new ArrayList<Classroom>();
		
		for(ClassroomStudent clazzSt: classStudent) {
			res.add(clazzSt.getClassroom());
		}
		
		return res;
	}

	@Override
	public List<ClassroomStudent> findStudentClassroom(int studentId) {
		return repository.findByStudent_Id(studentId);
	}

	@Override
	public void updateStudentClass(int studentId, List<ClassroomStudentDTO> listClass) {
		
		List<ClassroomStudent> copy = repository.findByStudent_Id(studentId);
		Map<String, String> subjectStatus = new HashMap<String, String>(); // lưu lại trạng thái học phí của các môn của học sinh trước khi xóa
		for (ClassroomStudent c: copy) {
			subjectStatus.put(c.getClassroom().getSubject(), c.getStatus());
		}

		repository.deleteAll(copy);
		List<ClassroomStudent> classStudent = new ArrayList<ClassroomStudent>(listClass.size());
		for(ClassroomStudentDTO cs: listClass) {
			ClassroomStudentKey id = new ClassroomStudentKey(cs.getStudentId(),cs.getClassId());
			Classroom clazz = classroomRepository.findById(cs.getClassId());
			ClassroomStudent clazzSt = new ClassroomStudent();
			clazzSt.setId(id);
			if(subjectStatus.containsKey(clazz.getSubject())) { 
				clazzSt.setStatus(subjectStatus.get(clazz.getSubject()));
				clazzSt.setSubStatus(subjectStatus.get(clazz.getSubject()));
			}else {
				clazzSt.setStatus("inactive");
				clazzSt.setSubStatus("inactive");
			}
			classStudent.add(clazzSt);
		}
	
		repository.saveAll(classStudent);
	}

	@Override
	public void createStudentClass(int studentId, List<ClassroomStudentDTO> listClass) {
		// TODO Auto-generated method stub
		List<ClassroomStudent> classStudent = new ArrayList<ClassroomStudent>(listClass.size());
		for(ClassroomStudentDTO cs: listClass) {
			ClassroomStudentKey id = new ClassroomStudentKey(cs.getStudentId(),cs.getClassId());
			ClassroomStudent clazzSt = new ClassroomStudent();
			clazzSt.setId(id);
			clazzSt.setStatus("active");
			
			classStudent.add(clazzSt);
		}
	
		repository.saveAll(classStudent);
	}

	@Override
	public void updateStudentClassStatus(ClassroomStudentDTO id) {
		ClassroomStudentKey clazzStId = new ClassroomStudentKey(id.getStudentId(), id.getClassId());
		ClassroomStudent clazzSt = repository.getById(clazzStId);
		if(id.getStatus() != null) {
			clazzSt.setStatus(id.getStatus());
		}
		repository.save(clazzSt);
	}

	@Override
	public List<ClassroomStudent> getListStudentInGradeAndSubject(int grade, String subject, String status) {
		return repository.findByClassroom_GradeAndClassroom_SubjectAndSubStatus(grade, subject, status);
	}

	@Override
	public void updateStudentClassSubjectStatus(int studentId, String subject, String status, String mainStatus) {
		repository.updateStudentSubjectClassStatus(subject, studentId, status, mainStatus);
		
	}

	@Override
	public List<Classroom> findAllStudentClass(int studentId) {
		
		List<ClassroomStudent> classStudent = repository.findByStudent_Id(studentId);
		
		List<Classroom> res = new ArrayList<Classroom>();
		
		for(ClassroomStudent clazzSt: classStudent) {
			res.add(clazzSt.getClassroom());
		}
		
		return res;
	}

	@Override
	public int countSubjectStatusOfStudent(String status, int studentId, String subject) {
		
		return repository.countByStatusAndId_StudentIdAndClassroom_Subject(status, studentId,subject);
	}

	@Override
	public void enableSubCostStatusForMainStauts(String subject, int grade) {
		repository.enableSubCostStatusForAllStudentInGradeOfSubject(subject, grade);
	}
	
	
}

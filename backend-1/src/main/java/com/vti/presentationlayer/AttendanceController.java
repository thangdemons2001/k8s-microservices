package com.vti.presentationlayer;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.vti.bussinesslayer.IAttendanceService;
import com.vti.bussinesslayer.IClassroomService;
import com.vti.bussinesslayer.IClassroomStudentService;
import com.vti.bussinesslayer.IStudentService;
import com.vti.bussinesslayer.ISubAttendanceService;
import com.vti.dto.AttendanceDTO;
import com.vti.dto.AttendanceDTO2;
import com.vti.dto.AttendanceKeyDTO;
import com.vti.dto.StudentDTO;
import com.vti.dto.StudentDTO2;
import com.vti.dto.StudentDTO3;
import com.vti.dto.StudentDTO8;
import com.vti.entity.Attendance;
import com.vti.entity.AttendanceKey;
import com.vti.entity.Student;
import com.vti.entity.SubAttendance;
import com.vti.entity.SubAttendanceKey;

@RestController
@RequestMapping(value = "api/v1/attendances")
@CrossOrigin("*")
public class AttendanceController {

	@Autowired
	private IAttendanceService service;

	@Autowired
	private IClassroomService classService;
	
	@Autowired
	private IClassroomStudentService classroomStudentService;

	@Autowired
	private IStudentService stService;

	@Autowired
	private ISubAttendanceService subAttendance;

	@GetMapping("/")
	public List<AttendanceDTO> getAllStudentAttendance() {

		List<AttendanceDTO> res = new ArrayList<AttendanceDTO>();
		List<Attendance> listAttendanceInClassToday = service.getAllAttendance();
		for (Attendance atten : listAttendanceInClassToday) {
			System.out.println(atten.getId().getDateId());
			res.add(atten.convertToDTO());
		}
		return res;
	}
	
	@GetMapping("/students/{studentId}/cost-info")
	public ResponseEntity<?> getSubjectStatusCostInfoOfStudent(String status, String subject, @PathVariable(name = "studentId") int studentId) {
		
		int countAllInactiveSubjectClassOfStudent = classroomStudentService.countSubjectStatusOfStudent(status, studentId, subject);
		if (countAllInactiveSubjectClassOfStudent > 0) {
			return new ResponseEntity<>(false, HttpStatus.OK);
		}
		return new ResponseEntity<>(true, HttpStatus.OK);
	}

	// Lấy thông tin để điểm danh của học sinh có id là {studentId}
	@GetMapping("/students/{studentId}")
	public StudentDTO getStudentById(@PathVariable(name = "studentId") int studentId) {
		return stService.findStudentById(studentId).convertToDTO2();
	}
	
	// update lí do nghỉ học
	@PostMapping("/students/{studentId}/reasons")
	public ResponseEntity<?> studentUpdateReasonAbsent(@PathVariable(name = "studentId") int studentId, String reason,
			@RequestBody AttendanceKeyDTO attenId) {
		System.out.println(attenId.getDateId());
		AttendanceKey attenKey = new AttendanceKey(studentId, attenId.getClassroomId(), attenId.getDateId());
		Attendance atten = service.findAttendanceById(attenKey);
		atten.setReason(reason);
		service.studentAtten(atten);
		return new ResponseEntity<String>("update successful!", HttpStatus.OK);
	}
	

	// Điểm danh chính cho học sinh có id là {classId} và cho lớp có id là {classId}
	@PostMapping("/students/{studentId}")
	public ResponseEntity<?> studentAtten(@PathVariable(name = "studentId") int studentId, String status,
			@RequestBody AttendanceKeyDTO attenId) {

		Attendance atten = new Attendance();
		AttendanceKey attenKey = new AttendanceKey(studentId, attenId.getClassroomId(), attenId.getDateId());
		atten.setId(attenKey);
		atten.setStatus(status);
		Date now = new Date();
		atten.setDate(now);
		service.studentAtten(atten);
		return new ResponseEntity<String>("atten successful!", HttpStatus.OK);
	}

	@PostMapping("/students/")
	public ResponseEntity<?> studentListAtten(@RequestBody List<AttendanceKeyDTO> attenId) {

		for (AttendanceKeyDTO key : attenId) {
			Attendance atten = new Attendance();
			AttendanceKey attenKey = new AttendanceKey(key.getStudentId(), key.getClassroomId(), key.getDateId());
			atten.setId(attenKey);
			atten.setStatus(key.getStatus());
			Date now = new Date();
			atten.setDate(now);
			service.studentAtten(atten);
		}
		return new ResponseEntity<String>("atten successful!", HttpStatus.OK);
	}

	// Điểm danh bù, api này sẽ cập nhật lại lần điểm danh gần nhất của học sinh có
	// id là {studentId} trong lớp
	// của học sinh có id là {classId} trong ngày hôm nay
	@PostMapping("/classes/{classId}/students/{studentId}")
	public ResponseEntity<?> studentAttenCompensate(@PathVariable(name = "classId") int classId,
			@PathVariable(name = "studentId") int studentId) {
		// điểm danh bù lại buổi đã thiếu
		Attendance lastesStudentAttendanceInClass = service.getLastAttendance(studentId, classId);
		lastesStudentAttendanceInClass.setStatus("P");
		lastesStudentAttendanceInClass.setDate(new Date());
		service.studentAtten(lastesStudentAttendanceInClass);
		return new ResponseEntity<String>("Atten Compensate successful!", HttpStatus.OK);
	}

	// Thêm vào danh sách lớp học bù. Api này để thêm vào danh sách điểm danh bù của
	// lớp học có id là {classId} của học
	// sinh có id là {studentId} trong ngày hôm nay
	@PostMapping("/classes/{classId}/sub/{studentId}")
	public ResponseEntity<?> addToSubAttendanceInClassToday(@PathVariable(name = "classId") int classId,
			@PathVariable(name = "studentId") int studentId) {
		SubAttendance atten = new SubAttendance();
		SubAttendanceKey attenKey = new SubAttendanceKey(studentId, classId, new Date());
		atten.setId(attenKey);
		atten.setStatus("P");
		Date now = new Date();
		atten.setDate(now);
		subAttendance.studentAttenCompensate(atten);
		return new ResponseEntity<String>("Atten Compensate successful!", HttpStatus.OK);
	}

	@DeleteMapping("/classes/{classId}/students/{studentId}")
	public ResponseEntity<?> deleteAttendance(@PathVariable(name = "classId") int classId,
			@PathVariable(name = "studentId") int studentId, @RequestBody AttendanceKeyDTO dateId) {
		service.deleteAttendance(classId, dateId.getDateId(), studentId);
		return new ResponseEntity<String>("delete successful!", HttpStatus.OK);
	}

	@DeleteMapping("/classes/{classId}/sub-students/{studentId}")
	public ResponseEntity<?> deleteSubAttendance(@PathVariable(name = "classId") int classId,
			@PathVariable(name = "studentId") int studentId, @RequestBody AttendanceKeyDTO dateId) {
		// xóa khỏi bảng điểm danh bù
		subAttendance.deleteAttenCompensate(classId, studentId, dateId.getDateId());
		return new ResponseEntity<String>("delete compensate successful!", HttpStatus.OK);
	}

	// lấy danh sách chính của lớp học có id là {classId} hôm nay
	@GetMapping("/classes/{classId}")
	public List<StudentDTO2> getAllStudentAttendanceInClassToday(@PathVariable(name = "classId") int classId)
			throws ParseException {

		List<StudentDTO2> res = new ArrayList<StudentDTO2>();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String now = dateFormat.format(new Date());
		// get today without year month day
		List<Attendance> listAttendanceInClassToday = service.getAllStudentAttendanceInClassToday(classId,
				new SimpleDateFormat("yyyy-MM-dd").parse(now));
		for (Attendance atten : listAttendanceInClassToday) {
			res.add(atten.getStudent().convertToDTO3(atten.getStatus()));
		}

		return res;
	}

	// lấy danh sách tổng của lớp học có id là {classId}
	@GetMapping("/classes/{classId}/main")
	public List<StudentDTO8> getAllStudentAttendanceInClass(@PathVariable(name = "classId") int classId) {

		List<StudentDTO8> res = new ArrayList<StudentDTO8>();
		List<Attendance> query = service.getListStudentAttendanceInClass(classId);
		Map<StudentDTO8, ArrayList<AttendanceDTO2>> map = new HashMap<StudentDTO8, ArrayList<AttendanceDTO2>>();
		for (Attendance atten : query) {

			Student st = atten.getStudent();
			StudentDTO8 stTemp = new StudentDTO8(st.getId(), st.getFirstName(), st.getLastName(), st.getFullName(),
					st.getSchool(), st.getStudentNumber(), st.getParentNumber(), st.getParentName(), null);

			AttendanceDTO2 temp = new AttendanceDTO2(atten.getStatus(), atten.getId().getDateId());
			if (map.get(stTemp) == null) {
				map.put(stTemp, new ArrayList<AttendanceDTO2>());
			}
			map.get(stTemp).add(temp);
		}
		for (Map.Entry<StudentDTO8, ArrayList<AttendanceDTO2>> entry : map.entrySet()) {

			entry.getKey().setListAtten(entry.getValue());
			res.add(entry.getKey());
		}

		return res;
	}

	// lấy danh sách học sinh học bù của lớp học có id là {classId}
	@GetMapping("/classes/{classId}/sub")
	public List<StudentDTO2> getAllSubStudentAttendanceInClassToday(@PathVariable(name = "classId") int classId)
			throws ParseException {

		List<StudentDTO2> res = new ArrayList<StudentDTO2>();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String now = dateFormat.format(new Date());
		// get today without year month day
		List<SubAttendance> listSubAttendanceInClassToday = subAttendance.getListSubAttendanceInClassToday(classId,
				new SimpleDateFormat("yyyy-MM-dd").parse(now));
		for (SubAttendance atten : listSubAttendanceInClassToday) {
			res.add(atten.getStudent().convertToDTO3(atten.getStatus()));
		}

		return res;
	}

	// lay danh sach nghi hoc của lớp học có id là {classId}
	@GetMapping("/classes/{classId}/absent")
	public List<StudentDTO> getAllStudentAttendanceNotInClassToday(@PathVariable(name = "classId") int classId)
			throws ParseException {
		// get list student in class
		List<Student> listSt = classService.getListStudentInClass(classId);
		List<StudentDTO> res = new ArrayList<StudentDTO>();
		for (Student st : listSt) {
			res.add(st.convertToDTO2());
		}
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String now = dateFormat.format(new Date());
		// get list student in class now
		List<Attendance> listAttendanceInClassToday = service.getAllStudentAttendanceInClassToday(classId,
				new SimpleDateFormat("yyyy-MM-dd").parse(now));
		Map<Integer, String> inClass = new HashMap<Integer, String>();
		for (Attendance atten : listAttendanceInClassToday) {
			inClass.put(atten.getStudent().getId(), atten.getStudent().getFullName());
		}
		// list Student not in class
		List<StudentDTO> listNotInClass = new ArrayList<StudentDTO>();
		for (StudentDTO stInClass : res) {
			if (!inClass.containsKey(stInClass.getId())) {
				listNotInClass.add(stInClass);
			}
		}
		return listNotInClass;
	}

	// lấy danh sách học sinh nghỉ của Môn học có tên là {subject} và của khối
	// {grade} trong Thứ {Date}
	@PostMapping("/students/grade/{grade}/absent")
	public List<StudentDTO3> getAllAbsentStudentInWeeklyDay(@PathVariable(name = "grade") int grade, String subject,
			@RequestBody AttendanceKeyDTO dateId) throws ParseException {

		List<Attendance> listAbsentInWeeklyDay = service.getListAbsentStudentInWeeklyDay(dateId.getDateId(), grade,
				subject, "A");

		List<StudentDTO3> listAbsentStudent = new ArrayList<StudentDTO3>();
		for (Attendance st : listAbsentInWeeklyDay) {

			listAbsentStudent.add(st.getStudent().convertToDTO4(st.getClassroom().getClassName()));

		}
		return listAbsentStudent;
	}

	@GetMapping("/students/classes/{classId}/dates")
	public int getTotalStudentInClassAtDate(@PathVariable(name = "classId") int classId, String dateId)
			throws ParseException {
		System.out.println(new SimpleDateFormat("dd-MM-yyyy").parse(dateId));
		return service.getTotalStudentInClassAtDate(classId, new SimpleDateFormat("dd-MM-yyyy").parse(dateId));
	}

	@GetMapping("/students/classes/{classId}/lessons")
	public ResponseEntity<?> getListStatusStudentInLesson(@PathVariable(name = "classId") int classId, String dateId,
			String status) throws ParseException {
		
		System.out.println(new SimpleDateFormat("dd-MM-yyyy").parse(dateId));
		
		List<Attendance> listAbsentInLesson = service.getListAbsentStudentInLesson(classId,
				new SimpleDateFormat("dd-MM-yyyy").parse(dateId), status);
		
		List<StudentDTO> res = new ArrayList<StudentDTO>();
		
		for (Attendance attend : listAbsentInLesson) {
			res.add(attend.getStudent().convertToDTO10(attend.getReason()));
		}
		
		return new ResponseEntity<List<StudentDTO>>(res, HttpStatus.OK);
	}
	
	@GetMapping("/students/classes/{classId}/lessons/sub")
	public List<StudentDTO2> getListStatusSubStudentInLesson(@PathVariable(name = "classId") int classId, String dateId)
			throws ParseException {

		List<StudentDTO2> res = new ArrayList<StudentDTO2>();
		
		
		List<SubAttendance> listSubAttendanceInClassToday = subAttendance.getListSubAttendanceInClassToday(classId,
				new SimpleDateFormat("dd-MM-yyyy").parse(dateId));
		
		
		for (SubAttendance atten : listSubAttendanceInClassToday) {
			res.add(atten.getStudent().convertToDTO3(atten.getStatus()));
		}

		return res;
	}

}

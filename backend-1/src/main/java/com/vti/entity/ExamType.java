package com.vti.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.vti.dto.ExamTypeDTO;



@Entity
@Table(name = "ExamType", catalog = "TCTmanagement")
public class ExamType implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Column(name = "examtype_id" , nullable = false)
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "name" , nullable = false, length = 100)
	private String name;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ExamType(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	
	public ExamType() {
		// TODO Auto-generated constructor stub
	}
	public ExamTypeDTO convertToDTO() {
		return new ExamTypeDTO(id, name);
	}
}

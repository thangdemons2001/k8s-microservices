package com.vti.bussinesslayer;

import java.util.List;

import com.vti.entity.QuestionChoice;

public interface IQuestionChoiceService {
	
	public void createQuestionChoice (long questionId, List<QuestionChoice> choices);
	
	public void deleteQuestionChoice (long questionChoiceId);
	
	public void updateQuestionChoice (QuestionChoice dto);
	
	public List<QuestionChoice> getAllQuestionChoice (long questionId);

}

package com.vti.bussinesslayer;

import java.util.List;

import com.vti.dto.ClassroomStudentDTO;
import com.vti.entity.Classroom;
import com.vti.entity.ClassroomStudent;


public interface IClassroomStudentService {
		
		public List<Classroom> findStudentClass (int studentId);
		
		public List<Classroom> findAllStudentClass (int studentId);
		
		public List<ClassroomStudent> findStudentClassroom (int studentId);
		
		public void updateStudentClass (int studentId, List<ClassroomStudentDTO> listClass);
		
		public void createStudentClass (int studentId, List<ClassroomStudentDTO> listClass);
		
		public void updateStudentClassStatus (ClassroomStudentDTO id);
		
		public int countSubjectStatusOfStudent (String status, int studentId, String subject);
		
		public void updateStudentClassSubjectStatus (int studentId, String subject, String status, String mainStatus);
		
		public List<ClassroomStudent> getListStudentInGradeAndSubject (int grade, String subject, String status);
		
		public void enableSubCostStatusForMainStauts (String subject, int grade);
		
		
}

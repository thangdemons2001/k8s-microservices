package com.vti.presentationlayer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import com.vti.bussinesslayer.IUserService;
import com.vti.dto.ConnectTestingSystemDTO;
import com.vti.entity.User;

@RestController
@RequestMapping(value = "api/v1/users")
@CrossOrigin("*")
public class UserController {
	
	@Autowired
	private IUserService userService;
	
	@GetMapping(value = "/{name}")
	public User findByUsername(@PathVariable(name = "name") String name) {
		return userService.findByUserName(name);
	}
	
	@GetMapping(value = "/userName/{userName}")
	public ResponseEntity<?> existsUserByUserName(@PathVariable(name = "userName") String userName) {
		// get entity
		boolean result = userService.existsUserByUserName(userName);

		// return result
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	
	@PutMapping(value = "/asscociate")
	public ResponseEntity<?> connectToTestingSystem(@RequestBody ConnectTestingSystemDTO dto) {
		userService.connectToTestingSystem(dto);
		return new ResponseEntity<String>("Connect Successful", HttpStatus.OK);
	}
		

}

package com.vti.entity;

import java.io.Serializable;


import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class ExamResultKey implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Column(name = "student_id" , nullable = false)
	private int studentId;
	
	@Column(name = "class_id" , nullable = false)
	private int classroomId;
	
	@Column(name = "exam_id", nullable = false)
	private int examId;

	public int getStudentId() {
		return studentId;
	}

	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}

	public int getClassroomId() {
		return classroomId;
	}

	public void setClassroomId(int classroomId) {
		this.classroomId = classroomId;
	}

	public int getExamId() {
		return examId;
	}

	public void setExamId(int examId) {
		this.examId = examId;
	}
	
	public ExamResultKey() {
		// TODO Auto-generated constructor stub
	}

	public ExamResultKey(int studentId, int classroomId, int examId) {
		super();
		this.studentId = studentId;
		this.classroomId = classroomId;
		this.examId = examId;
	}
	
	
	
	
}

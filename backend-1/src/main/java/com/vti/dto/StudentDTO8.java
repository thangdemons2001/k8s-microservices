package com.vti.dto;

import java.util.List;

// only use for get Student attendance
public class StudentDTO8  {
	
	private int id;
		
	private String firstName;
	
	private String lastName;
		
	private String fullName;
	
	private String school;
	
	private String studentNumber;
	
	private String parentNumber;
	
	private String parentName;
	
	private List<AttendanceDTO2> listAtten;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getSchool() {
		return school;
	}

	public void setSchool(String school) {
		this.school = school;
	}

	public String getStudentNumber() {
		return studentNumber;
	}

	public void setStudentNumber(String studentNumber) {
		this.studentNumber = studentNumber;
	}

	public String getParentNumber() {
		return parentNumber;
	}

	public void setParentNumber(String parentNumber) {
		this.parentNumber = parentNumber;
	}

	public String getParentName() {
		return parentName;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	public List<AttendanceDTO2> getListAtten() {
		return listAtten;
	}

	public void setListAtten(List<AttendanceDTO2> listAtten) {
		this.listAtten = listAtten;
	}
	
	public StudentDTO8() {
		// TODO Auto-generated constructor stub
	}

	public StudentDTO8(int id, String firstName, String lastName, String fullName, String school, String studentNumber,
			String parentNumber, String parentName, List<AttendanceDTO2> listAtten) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.fullName = fullName;
		this.school = school;
		this.studentNumber = studentNumber;
		this.parentNumber = parentNumber;
		this.parentName = parentName;
		this.listAtten = listAtten;
	}
	
	@Override
	public String toString() {
		
		return this.fullName + " " + this.id;
	}
	
	@Override
	public int hashCode() {
		
		return this.id;
	}
	
	@Override
	public boolean equals(Object obj) {
		StudentDTO8 student = (StudentDTO8) obj;
		return this.id == student.id;
	}
}

package com.vti.datalayer;







import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.vti.entity.Chapter;



public interface IChapterRepository extends JpaRepository<Chapter, Integer>, JpaSpecificationExecutor<Chapter> {
	
		public Chapter findById (int chapterId);
		
		public List<Chapter> findByGradeAndSubject (int grade, String subject);
		
		
}

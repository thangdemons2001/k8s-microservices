package com.vti.bussinesslayer;

import java.util.List;

import com.vti.entity.StudentPayment;

public interface IStudentPaymentService {
	
	public void createNewPayment (StudentPayment dto);
	
	public void updatePayment (long paymentId, StudentPayment dto);
	
	public void deletePayment (long paymentId);
	
	public List<StudentPayment> getAllPaymentInGradeToday ();
	
	public List<StudentPayment> getAllPaymentInGradeOfSubject ();

}

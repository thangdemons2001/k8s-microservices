package com.vti.bussinesslayer;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vti.datalayer.IClassroomLessonRepository;
import com.vti.entity.ClassroomLesson;
@Service
public class ClassroomLessonService implements IClassroomLessonService {
	
	@Autowired
	private IClassroomLessonRepository repository;

	@Override
	public void createClassroomLesson(List<ClassroomLesson> dto) {
		
		repository.saveAll(dto);
	}

}

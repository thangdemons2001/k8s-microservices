package com.vti.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.vti.dto.AttendanceDTO;
@Entity
@Table(name = "Attendance", catalog = "TCTmanagement")
public class Attendance implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@EmbeddedId
	private AttendanceKey id;
	
	@ManyToOne
	@MapsId("student_id")
	@JoinColumn(name = "student_id")
	private Student student;
	
	@ManyToOne
	@MapsId("class_id")
	@JoinColumn(name = "class_id")
	private Classroom classroom;
	
	
	@Column(name = "status_boolean",  nullable = false)
	private String status;
	
	@Column(name = "date",  nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date date;
	
	@Column(name = "reason")
	private String reason;
	
	

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public AttendanceKey getId() {
		return id;
	}

	public void setId(AttendanceKey id) {
		this.id = id;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public Classroom getClassroom() {
		return classroom;
	}

	public void setClassroom(Classroom classroom) {
		this.classroom = classroom;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Attendance(AttendanceKey id, Student student, Classroom classroom, String status, Date date) {
		super();
		this.id = id;
		this.student = student;
		this.classroom = classroom;
		this.status = status;
		this.date = date;
	}
	

	public AttendanceDTO convertToDTO() {
		return new AttendanceDTO(this.student.convertToDTO2(), status, this.id.getDateId());
	}
	
	public Attendance() {
		// TODO Auto-generated constructor stub
	}
}

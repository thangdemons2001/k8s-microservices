package com.vti.dto;

public class ExamResultDTO3 {
		
		private String examName;
		
		private double mark;
		
		private int examId;
		
		

		public ExamResultDTO3(String examName, double mark, int examId) {
			super();
			this.examName = examName;
			this.mark = mark;
			this.examId = examId;
		}

		public int getExamId() {
			return examId;
		}

		public void setExamId(int examId) {
			this.examId = examId;
		}

		public String getExamName() {
			return examName;
		}

		public void setExamName(String examName) {
			this.examName = examName;
		}

		public double getMark() {
			return mark;
		}

		public void setMark(double mark) {
			this.mark = mark;
		}

		public ExamResultDTO3(String examName, double mark) {
			super();
			this.examName = examName;
			this.mark = mark;
		}
		
		public ExamResultDTO3() {
			// TODO Auto-generated constructor stub
		}
}

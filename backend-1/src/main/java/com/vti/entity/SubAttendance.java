package com.vti.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.vti.dto.SubAttendanceDTO;
@Entity
@Table(name = "SubAttendance", catalog = "TCTmanagement")
public class SubAttendance implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@EmbeddedId
	private SubAttendanceKey id;
	
	@ManyToOne
	@MapsId("student_id")
	@JoinColumn(name = "student_id")
	private Student student;
	
	@ManyToOne
	@MapsId("class_id")
	@JoinColumn(name = "class_id")
	private Classroom classroom;
	
	
	@Column(name = "status_boolean",  nullable = false)
	private String status;
	
	@Column(name = "date",  nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date date;
	

	public SubAttendanceKey getId() {
		return id;
	}

	public void setId(SubAttendanceKey id) {
		this.id = id;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public Classroom getClassroom() {
		return classroom;
	}

	public void setClassroom(Classroom classroom) {
		this.classroom = classroom;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	
	

	public SubAttendance(SubAttendanceKey id, Student student, Classroom classroom, String status, Date date) {
		super();
		this.id = id;
		this.student = student;
		this.classroom = classroom;
		this.status = status;
		this.date = date;
	}

	public SubAttendanceDTO convertToDTO() {
		return new SubAttendanceDTO(this.student.convertToDTO2(), status, this.id.getDateId());
	}
	
	public SubAttendance() {
		// TODO Auto-generated constructor stub
	}
}

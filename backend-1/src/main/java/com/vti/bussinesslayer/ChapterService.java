package com.vti.bussinesslayer;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vti.datalayer.IChapterRepository;
import com.vti.entity.Chapter;

@Service
public class ChapterService implements IChapterService {
	
	@Autowired
	private IChapterRepository repository;

	@Override
	public void createChapter(Chapter chapter) {
		repository.save(chapter);
	}

	@Override
	public Chapter getChapterById(int chapterId) {
		return repository.findById(chapterId);
	}

	@Override
	public List<Chapter> getAllChapterSubjectInGrade(String subject, int grade) {
		return repository.findByGradeAndSubject(grade, subject);
	}

}

package com.vti.bussinesslayer;

import java.util.Date;
import java.util.List;

import com.vti.entity.Attendance;
import com.vti.entity.AttendanceKey;

public interface IAttendanceService {
			
		public void studentAtten(Attendance atten);
		
		public void deleteAttendance (int classId, Date dateId, int studentId);
		
		public Attendance findAttendanceById(AttendanceKey id);
		
		public Attendance findAttendanceByStudentIdAndDateIdAndClassId (int studentId, int classId, Date dateId);
		
		public List<Attendance> getAllStudentAttendanceInClassToday(int classId, Date dateId);
		
		public List<Attendance> getAllAttendance();
		
		public Attendance getLastAttendance(int studentId, int classId);
		
		public List<Attendance> getListAbsentStudentInWeeklyDay (Date dateId, int grade, String subject, String status);
		
		public List<Attendance> getListAbsentStudentInLesson (int classId, Date dateId, String status);
		
		public List<Attendance> getListStudentAttendanceInClass (int classId);
		
		public int getTotalStudentInClassAtDate (int classId, Date dateId);
}

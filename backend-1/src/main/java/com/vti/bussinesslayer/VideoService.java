package com.vti.bussinesslayer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vti.datalayer.IVideoRepository;
import com.vti.entity.Video;


@Service
public class VideoService implements IVideoService {
	
	@Autowired
	private IVideoRepository repository;

	@Override
	public void createVideo(Video video) {
		repository.save(video);
	}

	@Override
	public Video getVideoById(int id) {
		// TODO Auto-generated method stub
		return repository.findById(id);
	}
	
}

package com.vti.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Formula;

import com.vti.dto.DeactivedStudentDTO;
import com.vti.dto.ReasonLeftDTO;


@Entity
@Table(name = "DeactivedStudent", catalog = "TCTmanagement")
public class DeactivedStudent implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Column(name = "student_id", nullable = false)
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "first_name", length = 50, nullable = false )
	private String firstName;
	
	@Column(name = "last_name", length = 50, nullable = false )
	private String lastName;
	
	@Formula(value = "concat(last_name, ' ', first_name)")
	private String fullName;

	@Column(name = "school_name", length = 800)
	private String school;

	@Column(name = "grade", nullable = false)
	private int grade;

	@Column(name = "student_phone_number", length = 50, nullable = false)
	private String studentNumber;

	@Column(name = "parent_phone_number", length = 50, nullable = false)
	private String parentNumber;

	@Column(name = "parent_name", length = 50, nullable = true)
	private String parentName;
	
	@Column(name = "process_status")
	private String processStatus;
	
	@Column(name = "left_date", nullable = false)
	@Temporal(TemporalType.DATE)
	private Date leftDate;
	
	@Column(name = "link_social", length = 800, nullable = true )
	private String social;
	
	@OneToMany(mappedBy = "deactivedStudent")
	private List<ReasonLeft> listReasonLeft;
	
	
	
	public DeactivedStudent(int id, String userName, String password, String firstName, String lastName,
			Date createdDate, String social, String fullName, String status, String role, String firstName2,
			String lastName2, String fullName2, String school, int grade, String studentNumber, String parentNumber,
			String parentName, String processStatus, Date leftDate, String social2) {
		
		firstName = firstName2;
		lastName = lastName2;
		fullName = fullName2;
		this.school = school;
		this.grade = grade;
		this.studentNumber = studentNumber;
		this.parentNumber = parentNumber;
		this.parentName = parentName;
		this.processStatus = processStatus;
		this.leftDate = leftDate;
		social = social2;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getSocial() {
		return social;
	}

	public void setSocial(String social) {
		this.social = social;
	}

	public DeactivedStudent() {
		// TODO Auto-generated constructor stub
	}
	
	public List<ReasonLeft> getListReasonLeft() {
		return listReasonLeft;
	}

	public void setListReasonLeft(List<ReasonLeft> listReasonLeft) {
		this.listReasonLeft = listReasonLeft;
	}
	
	
	
	public DeactivedStudent(int id, String userName, String password, String firstName, String lastName,
			Date createdDate, String social, String fullName, String status, String role, String firstName2,
			String lastName2, String school, int grade, String studentNumber, String parentNumber, String parentName,
			String processStatus, List<ReasonLeft> listReasonLeft) {
	
		firstName = firstName2;
		lastName = lastName2;
		this.school = school;
		this.grade = grade;
		this.studentNumber = studentNumber;
		this.parentNumber = parentNumber;
		this.parentName = parentName;
		this.processStatus = processStatus;
		this.listReasonLeft = listReasonLeft;
	}

	public DeactivedStudent(int id, String userName, String password, String firstName, String lastName,
			Date createdDate, String social, String fullName, String status, String role, String firstName2,
			String lastName2, String school, int grade, String studentNumber, String parentNumber, String parentName,
			String processStatus) {
		
		firstName = firstName2;
		lastName = lastName2;
		this.school = school;
		this.grade = grade;
		this.studentNumber = studentNumber;
		this.parentNumber = parentNumber;
		this.parentName = parentName;
		this.processStatus = processStatus;
	}

	public DeactivedStudent(int id, String userName, String password, String firstName, String lastName,
			Date createdDate, String social, String fullName, String status, String role, String firstName2,
			String lastName2, String school, int grade, String studentNumber, String parentNumber, String parentName,
			String processStatus, Date leftDate) {
	
		firstName = firstName2;
		lastName = lastName2;
		this.school = school;
		this.grade = grade;
		this.studentNumber = studentNumber;
		this.parentNumber = parentNumber;
		this.parentName = parentName;
		this.processStatus = processStatus;
		this.leftDate = leftDate;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getSchool() {
		return school;
	}

	public void setSchool(String school) {
		this.school = school;
	}

	public int getGrade() {
		return grade;
	}

	public void setGrade(int grade) {
		this.grade = grade;
	}

	public String getStudentNumber() {
		return studentNumber;
	}

	public void setStudentNumber(String studentNumber) {
		this.studentNumber = studentNumber;
	}

	public String getParentNumber() {
		return parentNumber;
	}

	public void setParentNumber(String parentNumber) {
		this.parentNumber = parentNumber;
	}

	public String getParentName() {
		return parentName;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	public String getProcessStatus() {
		return processStatus;
	}

	public void setProcessStatus(String processStatus) {
		this.processStatus = processStatus;
	}

	public Date getLeftDate() {
		return leftDate;
	}

	public void setLeftDate(Date leftDate) {
		this.leftDate = leftDate;
	}
	
	public DeactivedStudentDTO convertToDTO() {
		List<ReasonLeftDTO> listReason = new ArrayList<ReasonLeftDTO>();
		for(ReasonLeft reason: this.getListReasonLeft()) {
			listReason.add(reason.convertToDTO());
		}
		return new DeactivedStudentDTO(id, firstName, lastName,fullName,social, school, grade, studentNumber, parentNumber, parentName, processStatus, leftDate,listReason);
	}
	
	
	
	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return this.getId();
	}
	
	@Override
	public boolean equals(Object obj) {
		DeactivedStudent student = (DeactivedStudent) obj;
		return this.getId() == student.getId();
	}
}

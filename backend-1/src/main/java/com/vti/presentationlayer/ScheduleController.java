package com.vti.presentationlayer;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vti.bussinesslayer.IClassroomService;
import com.vti.bussinesslayer.IScheduleService;
import com.vti.entity.Classroom;
import com.vti.entity.Schedule;

@RestController
@RequestMapping(value = "api/v1/schedules")
@CrossOrigin("*")
public class ScheduleController {
	
	@Autowired
	private IScheduleService service;
	
	@Autowired
	private IClassroomService classService;
	
	@PostMapping("/classes/{classId}")
	public ResponseEntity<?> createClassSchedule(@RequestBody List<Schedule> dto, @PathVariable(name = "classId") int classId) {
		
		Classroom classroom = classService.getClassroomById(classId);
		
		for (Schedule schedule: dto) {
			schedule.setClassroom(classroom);
		}
		
		service.createSchedule(dto, classId);
		
		return new ResponseEntity<String>("create successful!", HttpStatus.OK);
	}
	
	@PutMapping("/classes/{classId}")
	public ResponseEntity<?> updateClassSchedule(@RequestBody List<Schedule> dto, @PathVariable(name = "classId") int classId) {
		
		Classroom classroom = classService.getClassroomById(classId);
		
		for (Schedule schedule: dto) {
			schedule.setClassroom(classroom);
		}
		
		service.updateSchedule(classId, dto);
		
		return new ResponseEntity<String>("update successful!", HttpStatus.OK);
	}

}

package com.vti.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.vti.dto.ExamDTO;



@Entity
@Table(name = "Exam", catalog = "TCTmanagement")
public class Exam implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Column(name = "exam_id" , nullable = false)
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "exam_name" , nullable = false, length = 100)
	private String name;
	
	@Column(name = "testing_system_id" ,length = 8000)
	private String testingSystemId;
	
	@Column(name = "start_date", nullable = false)
	@Temporal(TemporalType.DATE)
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date createdDate;
	
	@ManyToOne 
    @JoinColumn(name = "exam_type")
	private ExamType type;
	
	@OneToMany(mappedBy = "exam")
	private List<ExamResult> examList;
	
	

	public String getTestingSystemId() {
		return testingSystemId;
	}

	public void setTestingSystemId(String testingSystemId) {
		this.testingSystemId = testingSystemId;
	}

	public List<ExamResult> getExamList() {
		return examList;
	}

	public void setExamList(List<ExamResult> examList) {
		this.examList = examList;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Exam() {
		// TODO Auto-generated constructor stub
	}
	
	

	public ExamType getType() {
		return type;
	}

	public void setType(ExamType type) {
		this.type = type;
	}

	public Exam(int id, String name, Date createdDate) {
		super();
		this.id = id;
		this.name = name;
		this.createdDate = createdDate;
	}

	public Exam(int id, String name, Date createdDate, ExamType type) {
		super();
		this.id = id;
		this.name = name;
		this.createdDate = createdDate;
		this.type = type;
	}
	
	
	
	public Exam(String name, String testingSystemId, Date createdDate) {
		super();
		this.name = name;
		this.testingSystemId = testingSystemId;
		this.createdDate = createdDate;
	}

	public ExamDTO convertToDTO() {
		return new ExamDTO(id, name, createdDate, type.getName(), type.getId());
	}
	
}

package com.vti.datalayer;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.vti.entity.Teacher;


public interface ITeacherRepository extends JpaRepository<Teacher, Integer>, JpaSpecificationExecutor<Teacher> {
	
	public Teacher findByUserName(String userName);
	
	
	public List<Teacher> findBySubjectName(String subjectName);
	
	public Teacher findById(int id);

}

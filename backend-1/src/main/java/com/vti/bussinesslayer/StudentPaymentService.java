package com.vti.bussinesslayer;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vti.datalayer.IStudentPaymentRepository;
import com.vti.entity.StudentPayment;

@Service
public class StudentPaymentService implements IStudentPaymentService {
	
	@Autowired
	private IStudentPaymentRepository repository;

	@Override
	public void createNewPayment(StudentPayment dto) {
		// TODO Auto-generated method stub
		repository.save(dto);
	}

	@Override
	public void updatePayment(long paymentId, StudentPayment dto) {
		// TODO Auto-generated method stub

	}

	@Override
	public void deletePayment(long paymentId) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<StudentPayment> getAllPaymentInGradeToday() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<StudentPayment> getAllPaymentInGradeOfSubject() {
		// TODO Auto-generated method stub
		return null;
	}

}

package com.vti.datalayer;





import java.util.Date;
import java.util.List;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import com.vti.entity.ExamResult;
import com.vti.entity.ExamResultKey;
import com.vti.entity.Student;

public interface IExamResultRepository extends JpaRepository<ExamResult, ExamResultKey>, JpaSpecificationExecutor<ExamResult> {
		
		public List<ExamResult> findById_ClassroomIdAndId_ExamId (int classId, int examId);
		
		@Query("SELECT e FROM ExamResult e WHERE e.id.classroomId = ?1 GROUP BY e.id.examId")
		public List<ExamResult> findById_ClassroomId (int classId);
		
		@Query("SELECT avg(e.mark)  "
				+ "FROM ExamResult e "
				+ "JOIN Classroom c on c.id = e.classroom.id "
				+ "WHERE c.subject = ?1 AND c.grade = ?2 "
				+ "GROUP BY e.student.id ORDER BY avg(e.mark) DESC")
		public List<Double> findSubjectAvgMarkOfStudentInGrade (String subject,int grade);
		
		@Query("SELECT e.student.id "
				+ "FROM ExamResult e "
				+ "JOIN Classroom c on c.id = e.classroom.id "
				+ "WHERE c.subject = ?1 AND c.grade = ?2 "
				+ "GROUP BY e.student.id ORDER BY avg(e.mark) DESC")
		public List<Integer> findSubjectStudentAvgMarkOfStudentInGrade (String subject,int grade);
		
		public List<ExamResult> findByClassroom_SubjectAndId_StudentId (String subject, int studentId);
		
		@Query(value="SELECT dense_rank() OVER (order by avg(e.mark) desc)  "
				+ "FROM ExamResult e "
				+ "JOIN Class c on c.class_id = e.class_id "
				+ "WHERE c.subject_name = ?1 AND c.grade = ?2 "
				+ "GROUP BY e.student_id",nativeQuery = true)
		public List<Integer> findSubjectStudentRankAvgMarkOfStudentInGrade (String subject,int grade);
		
		@Query(
				 "FROM ExamResult e "
				+ "JOIN Exam exam on exam.id = e.id.examId "
				+ "WHERE exam.createdDate >= ?1 AND e.exam.createdDate <= ?4 "
				+ "AND e.classroom.grade = ?3 AND e.classroom.subject = ?2 "
				+ "GROUP BY exam.id, e.classroom.id ")
		public List<ExamResult> findSubjectExamInMonthOfGrade(Date firstDayOfMonth, String subject, int grade, Date lastDayOfMonth);
		
		
		public List<ExamResult> findByClassroom_SubjectAndClassroom_GradeAndExam_CreatedDateBetween(String subject, int grade, Date firstDayOfMonth, Date lastDayOfMonth);
		
		public List<ExamResult> findById_ExamId (int id);
		
		@Query(	"SELECT st.student "
				+ "FROM ExamResult e "
				+ "RIGHT JOIN ClassroomStudent st on st.student.id = e.id.studentId "
				+ "WHERE "
				+ "e.exam.createdDate >= ?1 AND e.exam.createdDate <= ?4 "
				+ "AND e.classroom.grade = ?3 AND e.classroom.subject = ?2 AND e.id.studentId IS NULL AND st.classroom.subject = ?2 "
				+ "GROUP BY st.student.id ")
		public List<Student> findStudentNotTakeSubjectExamInMonthAtGrade(Date firstDayOfMonth, String subject, int grade, Date lastDayOfMonth);
		
		
		
}

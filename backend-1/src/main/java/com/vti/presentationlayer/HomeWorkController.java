package com.vti.presentationlayer;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vti.bussinesslayer.IClassroomService;
import com.vti.bussinesslayer.IClassroomStudentService;
import com.vti.bussinesslayer.IHomeWorkService;
import com.vti.bussinesslayer.ILessonService;
import com.vti.bussinesslayer.IStudentHomeWorkService;
import com.vti.bussinesslayer.IStudentService;
import com.vti.dto.ClassroomDTO;
import com.vti.dto.StudentDTO;
import com.vti.dto.StudentDTO10;
import com.vti.dto.StudentDTO9;
import com.vti.entity.Classroom;
import com.vti.entity.ClassroomStudent;
import com.vti.entity.HomeWork;
import com.vti.entity.Lesson;
import com.vti.entity.Student;
import com.vti.entity.StudentHomeWork;


@RestController
@RequestMapping(value = "api/v1/homeworks")
@CrossOrigin("*")
public class HomeWorkController {

	@Autowired
	private ILessonService service;

	@Autowired
	private IHomeWorkService homeWorkSerivce;

	@Autowired
	private IClassroomService classService;

	@Autowired
	private IStudentHomeWorkService submitHomeWorkService;
	
	@Autowired
	private IStudentService studentService;

	@Autowired
	private IClassroomStudentService classStudentService;

	@PostMapping("/lessons/{lessonId}")
	public ResponseEntity<?> createHomeWork(@RequestBody HomeWork homeWork,
			@PathVariable(name = "lessonId") int lessonId) {

		Lesson lesson = service.getLessonById(lessonId);
		homeWork.setLesson(lesson);
		homeWorkSerivce.createHomeWork(homeWork);
		return new ResponseEntity<String>("create successful!", HttpStatus.OK);
	}

	// nop btvn tuan trc
	@PostMapping("/classes/{classId}/current-lesson")
	public ResponseEntity<?> studentSubmitCurrentHomeWork(@RequestBody List<Integer> listStudentId,
			@PathVariable(name = "classId") int classId) {

		Lesson currentLesson = service.getCurrentLessonInClass(classId);
		List<StudentHomeWork> listSubmitsion = new ArrayList<StudentHomeWork>();
		
		for (Integer studentId : listStudentId) {

			Date today = new Date();
			StudentHomeWork submit = new StudentHomeWork();
			Student student = studentService.findStudentById(studentId);
			submit.setStudent(student);
			submit.setLesson(currentLesson);
			submit.setMark(10);
			submit.setSubmitDate(today);
			listSubmitsion.add(submit);

		
		}
		
		submitHomeWorkService.createMultiStudentSubmitHomeWork(listSubmitsion, listStudentId);

		return new ResponseEntity<String>("submit successful!", HttpStatus.OK);
	}
	
	// nop btvn trong buổi học bất kì
		@PostMapping("/classes/{classId}/lessons")
		public ResponseEntity<?> studentSubmitHomeWorkInLesson(@RequestBody List<Integer> listStudentId,
				@PathVariable(name = "classId") int classId, int lessonId) {

			Lesson lesson = service.getLessonById(lessonId);
			List<StudentHomeWork> listSubmitsion = new ArrayList<StudentHomeWork>();
			
			for (Integer studentId : listStudentId) {

				Date today = new Date();
				StudentHomeWork submit = new StudentHomeWork();
				Student student = studentService.findStudentById(studentId);
				submit.setStudent(student);
				submit.setLesson(lesson);
				submit.setMark(10);
				submit.setSubmitDate(today);
				listSubmitsion.add(submit);

			
			}
			
			submitHomeWorkService.createMultiStudentSubmitHomeWork(listSubmitsion, listStudentId);

			return new ResponseEntity<String>("submit successful!", HttpStatus.OK);
		}
	
	@DeleteMapping("/classes/{classId}/lessons/{lessonId}")
	public ResponseEntity<?> deleteStudentSubmitCurrentHomeWork(@RequestBody List<Integer> listStudentId,
			@PathVariable(name = "classId") int classId, @PathVariable(name = "lessonId") int lessonId ) {

		submitHomeWorkService.deleteMultiStudentSubmitHomeWorkInLesson(lessonId, listStudentId);

		return new ResponseEntity<String>("delete successful!", HttpStatus.OK);
	}

	@GetMapping("/classes/{classId}/students/unfinished/lessons/{lessonId}")
	public ResponseEntity<?> getAllStudentNotSubmittedHomeWorkInLesson(@PathVariable(name = "classId") int classId,
			@PathVariable(name = "lessonId") int lessonId) {

		List<StudentDTO> res = new ArrayList<StudentDTO>();

		List<Student> studentInClass = classService.getListStudentInClass(classId);

		List<StudentHomeWork> finishedStudent = submitHomeWorkService.getAllSubmittedStudentInLesson(lessonId);

		if (finishedStudent.size() == 0) {
			return new ResponseEntity<String>("empty", HttpStatus.OK);
		}

		Map<Integer, Integer> mapFinishedStudent = new HashMap<Integer, Integer>();

		for (StudentHomeWork st : finishedStudent) {
			int studentId = st.getStudent().getId();
			mapFinishedStudent.put(studentId, studentId);
		}

		for (Student st : studentInClass) {
			int studentId = st.getId();
			if (!mapFinishedStudent.containsKey(studentId)) {
				res.add(st.convertToDTO2());
			}
		}

		return new ResponseEntity<List<StudentDTO>>(res, HttpStatus.OK);
	}
	
	@GetMapping("/classes/{classId}/students/finished/lessons/{lessonId}")
	public ResponseEntity<?> getAllStudentSubmittedHomeWorkInLesson(@PathVariable(name = "classId") int classId,
			@PathVariable(name = "lessonId") int lessonId) {

		List<StudentDTO> res = new ArrayList<StudentDTO>();


		List<StudentHomeWork> finishedStudent = submitHomeWorkService.getAllSubmittedStudentInLesson(lessonId);

		if (finishedStudent.size() == 0) {
			return new ResponseEntity<String>("empty", HttpStatus.OK);
		}
		
		for (StudentHomeWork st : finishedStudent) {
			res.add(st.getStudent().convertToDTO2());
		}
		

		return new ResponseEntity<List<StudentDTO>>(res, HttpStatus.OK);
	}

	@GetMapping("/students/grade/{grade}/months/{month}")
	public ResponseEntity<?> getAllStudentSubmitHomeWorkOfSubjectInGradeOfMonth(@PathVariable(name = "grade") int grade,
			@PathVariable(name = "month") int month, String subject) {

		List<ClassroomStudent> studentsInGrade = classStudentService.getListStudentInGradeAndSubject(grade, subject,
				"active");
		List<StudentHomeWork> submittedSubjectHomeWorkInGradeInMonth = submitHomeWorkService
				.getAllSubmittedHomeWorkOfStudentInMonth(subject, month, grade);
		System.out.println(submittedSubjectHomeWorkInGradeInMonth.size());
		
		// map để đỏi format json thành dạng học sinh có list submit btvn
		Map<StudentDTO9, ArrayList<StudentHomeWork>> mapStudentHomeWork = new HashMap<StudentDTO9, ArrayList<StudentHomeWork>>();
		
		// map để đỏi format json thành dạng học sinh có list classroom
		Map<StudentDTO9, ArrayList<ClassroomDTO>> mapStudentClassroom = new HashMap<StudentDTO9, ArrayList<ClassroomDTO>>();

		List<StudentDTO9> res = new ArrayList<StudentDTO9>();

		for (ClassroomStudent st : studentsInGrade) {
			Classroom clazz = st.getClassroom();
			Student student = st.getStudent();
			
			StudentDTO9 stDTO = student.convertToDTO9();
			
			if(mapStudentHomeWork.get(stDTO) == null) {
				mapStudentHomeWork.put(stDTO, new ArrayList<StudentHomeWork>());
			}
			
			if(mapStudentClassroom.get(stDTO) == null) {
				mapStudentClassroom.put(stDTO, new ArrayList<ClassroomDTO>());
			}
			
			mapStudentClassroom.get(stDTO).add(clazz.convertToDTO());
			
			
		}
		
		for (Map.Entry<StudentDTO9, ArrayList<ClassroomDTO>> entry : mapStudentClassroom.entrySet()) {

			entry.getKey().setListClass(entry.getValue());
			res.add(entry.getKey());
		}
		
		for (StudentHomeWork homework: submittedSubjectHomeWorkInGradeInMonth) {
			StudentDTO9 student = homework.getStudent().convertToDTO9();
			if (mapStudentHomeWork.get(student) == null) {
				mapStudentHomeWork.put(student, new ArrayList<StudentHomeWork>());
			}
			mapStudentHomeWork.get(student).add(homework);
			
		}
		
		System.out.println(mapStudentClassroom);
		System.out.println(mapStudentHomeWork);
		
		for (StudentDTO9 student: res) {
			
			ArrayList<StudentHomeWork> listSubmitOfStudent = mapStudentHomeWork.get(student);
			
			System.out.println(listSubmitOfStudent.size());
			for(StudentHomeWork submittsion: listSubmitOfStudent) {
				Calendar cal = Calendar.getInstance();
				Date date = submittsion.getSubmitDate();
				cal.setTime(date);
				int dayInMonth = cal.get(Calendar.DAY_OF_MONTH);
				System.out.println(dayInMonth);
				if(dayInMonth <= 7) {
					if(student.getFirst() == null) {
						student.setFirst("y");
					}else {
						student.setFirst("y" + student.getFirst());
					}
					
				}
				if(dayInMonth > 7 && dayInMonth <= 14) {
					if(student.getSecond() == null) {
						student.setSecond("y");
					}else {
						student.setSecond("y" + student.getSecond());
					}
					
				}
				if(dayInMonth > 14 && dayInMonth <= 21) {
					if(student.getThird() == null) {
						student.setThird("y");
					}
					else {
						student.setThird("y" + student.getThird());
					}
					
				}
				if(dayInMonth > 21 && dayInMonth <= 31) {
					if(student.getFourth() == null) {
						student.setFourth("y");
					}else {
						student.setFourth("y" + student.getFourth());
					}
					
				}
			}
		}
		
		
		return new ResponseEntity<List<StudentDTO9>>(res, HttpStatus.OK);
	}

	@PutMapping("/lessons/{lessonId}")
	public ResponseEntity<?> updateHomeWork(@RequestBody HomeWork homeWork,
			@PathVariable(name = "lessonId") int lessonId) {
		HomeWork homework = homeWorkSerivce.getLessonHomeWorkByLessonId(lessonId);
		if (homework == null) {
			return new ResponseEntity<String>("update successful!", HttpStatus.OK);
		}
		if (homeWork.getName() != null) {
			homework.setName(homeWork.getName());
		}
		if (homeWork.getLink() != null) {
			homework.setLink(homeWork.getLink());
		}
		if (homeWork.getMisson() != null) {
			homework.setMisson(homeWork.getMisson());
		}
		if (homeWork.getKeyLink() != null) {
			homework.setKeyLink(homeWork.getKeyLink());
		}
		homeWorkSerivce.createHomeWork(homework);
		return new ResponseEntity<String>("update successful!", HttpStatus.OK);
	}

}

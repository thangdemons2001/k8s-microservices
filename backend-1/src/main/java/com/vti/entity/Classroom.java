package com.vti.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.vti.dto.ClassroomDTO;
import com.vti.dto.ClassroomDTO2;
import com.vti.dto.ClassroomDTO3;
import com.vti.dto.ClassroomDTO4;
import com.vti.dto.ClassroomDTO5;
import com.vti.dto.ClassroomDTO6;
import com.vti.dto.ClassroomDTO7;
import com.vti.dto.LessonDTO3;
import com.vti.dto.MentorDTO;
import com.vti.dto.ScheduleDTO;
@Entity
@Table(name = "Class", catalog = "TCTmanagement")
public class Classroom implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Column(name = "class_id", nullable = false)
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "class_name",length = 50 , nullable = false)
	private String className;

	@Column(name = "grade" , nullable = false)
	private int grade;
	
	@Column(name = "created_date", updatable = false, nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdDate;
	
	@Column(name = "subject_name",length = 50, nullable = false)
	private String subject;
	
	@Column(name = "status_boolean", nullable = false)
	private String status = "active";
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "teacher_id", nullable = false)
	private Teacher teacherId;
	
	@OneToMany(mappedBy = "classroom")
	private List<Schedule> listSchedule;
	
	@OneToMany(mappedBy = "classroom")
	private List<ClassroomStudent> studentList;
	
	@OneToMany(mappedBy = "classroom")
	private List<ClassroomLesson> lessonList;
	
	@OneToMany(mappedBy = "classroom")
	private List<ClassroomMentor> mentorList;
	
	@OneToMany(mappedBy = "classroom")
	private List<ExamResult> examList;
	
	@OneToMany(mappedBy = "classroom")
	private List<Attendance> listAttendance;
	

	
	public Classroom() {
		// TODO Auto-generated constructor stub
	}

	

	public List<Schedule> getListSchedule() {
		return listSchedule;
	}



	public void setListSchedule(List<Schedule> listSchedule) {
		this.listSchedule = listSchedule;
	}



	public Classroom(int id, String className, int grade, Date createdDate, String subject, String status,
			Teacher teacherId) {
		super();
		this.id = id;
		this.className = className;
		this.grade = grade;
		this.createdDate = createdDate;
		this.subject = subject;
		this.status = status;
		this.teacherId = teacherId;
	}



	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public int getGrade() {
		return grade;
	}

	public void setGrade(int grade) {
		this.grade = grade;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Teacher getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(Teacher teacherId) {
		this.teacherId = teacherId;
	}

	
	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}
	
	public List<ClassroomMentor> getMentorList() {
		return mentorList;
	}

	public void setMentorList(List<ClassroomMentor> mentorList) {
		this.mentorList = mentorList;
	}
	
	public ClassroomDTO convertToDTO() {
		
		List<ScheduleDTO> listScheduleDto = new ArrayList<ScheduleDTO>();
		
		for(Schedule schedule: this.listSchedule) {
			
			listScheduleDto.add(schedule.convertToDTO());
			
		}
		
		return new ClassroomDTO(id, className, subject ,grade ,teacherId.convertToDTO(),
				listScheduleDto, this.studentList.size());
	}
	
	public ClassroomDTO convertToDTO1() {
		
		List<ScheduleDTO> listScheduleDto = new ArrayList<ScheduleDTO>();
		
		for(Schedule schedule: this.listSchedule) {
			
			listScheduleDto.add(schedule.convertToDTO());
			
		}
		
		return new ClassroomDTO(id, className, subject ,grade ,teacherId.convertToDTO(),
				listScheduleDto, 0);
	}
	
	public ClassroomDTO3 convertToDTO3() {
		
		List<ScheduleDTO> listScheduleDto = new ArrayList<ScheduleDTO>();
		
		for(Schedule schedule: this.listSchedule) {
			
			listScheduleDto.add(schedule.convertToDTO());
			
		}
		
		return new ClassroomDTO3(id, className, subject ,grade ,
				listScheduleDto);
	}
	
	public ClassroomDTO2 convertToDTO2() {
		
		List<MentorDTO> listMentor = new ArrayList<MentorDTO>();
		
		List<ScheduleDTO> listScheduleDto = new ArrayList<ScheduleDTO>();
		
		for(Schedule schedule: this.listSchedule) {
			
			listScheduleDto.add(schedule.convertToDTO());
			
		}
		
		for(ClassroomMentor cm: mentorList) {
			listMentor.add(cm.getMentor().convertToDTO());
		}
		
		return new ClassroomDTO2(id, className, subject ,grade ,teacherId.convertToDTO(),
				listScheduleDto,listMentor, this.studentList.size());
	}
	
	public ClassroomDTO4 convertToDTO4() {
		
		List<MentorDTO> listMentor = new ArrayList<MentorDTO>();
		
		List<ScheduleDTO> listScheduleDto = new ArrayList<ScheduleDTO>();
		
		for(Schedule schedule: this.listSchedule) {
			
			listScheduleDto.add(schedule.convertToDTO());
			
		}
		
		for(ClassroomMentor cm: mentorList) {
			listMentor.add(cm.getMentor().convertToDTO());
		}
		
		return new ClassroomDTO4(id, className, subject ,grade ,teacherId.convertToDTO(),
				listScheduleDto,listMentor);
	}
	public ClassroomDTO5 convertToDTO5() {
		
		List<ScheduleDTO> listScheduleDto = new ArrayList<ScheduleDTO>();
		
		for(Schedule schedule: this.listSchedule) {
			
			listScheduleDto.add(schedule.convertToDTO());
			
		}
		return new ClassroomDTO5(id, className, subject ,grade ,teacherId.convertToDTO(),
				listScheduleDto);
	}
	public ClassroomDTO6 convertToDTO6() {
		
		List<ScheduleDTO> listScheduleDto = new ArrayList<ScheduleDTO>();
		
		List<Lesson> listLesson = new ArrayList<Lesson>();
		
		for(ClassroomLesson lesson: this.lessonList) {
			listLesson.add(lesson.getLesson());
		}
		
		for(Schedule schedule: this.listSchedule) {
			
			listScheduleDto.add(schedule.convertToDTO());
		}
		
		return new ClassroomDTO6(id, className, subject ,grade,
				listScheduleDto,listLesson);
	}
	
	public ClassroomDTO7 convertToDTO7 (String date, Date start, Date end) {
		
		List<MentorDTO> listMentor = new ArrayList<MentorDTO>();
		
		for(ClassroomMentor cm: mentorList) {
			listMentor.add(cm.getMentor().convertToDTO());
		}
		
		return new ClassroomDTO7(id, className, subject, grade, teacherId.convertToDTO(), start, end, date, listMentor);
	}
	
	public ClassroomDTO7 convertToDTO7 (String date, Date start, Date end, String costStatus, String subCostStatus) {
		
		
		return new ClassroomDTO7(id, className, subject, grade, teacherId.convertToDTO(), start, end, date,costStatus,subCostStatus);
	}
	
}

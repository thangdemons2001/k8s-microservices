package com.vti.bussinesslayer;

import java.util.List;

import com.vti.entity.ClassroomLesson;

public interface IClassroomLessonService {
	
	public void createClassroomLesson (List<ClassroomLesson> dto);

}

package com.vti.bussinesslayer;

import java.util.List;

import com.vti.dto.ClassroomMentorDTO;

public interface IClassroomMentorService {
		
		public void createMentorClass(int classId,List<ClassroomMentorDTO> listMentor);
		
		public void updateMentorClass (int classId,List<ClassroomMentorDTO> listMentor);
}

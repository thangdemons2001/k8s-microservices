package com.vti.config.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.stereotype.Component;

import com.vti.bussinesslayer.IUserService;
@Component
@EnableWebSecurity
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {
	
	
	
	@Autowired
	private IUserService service;

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(service).passwordEncoder(new NoEncoderPassword());
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
			.cors()
			.and()
			.authorizeRequests()
//			.antMatchers("/api/v1/class/**").hasAnyAuthority("MENTOR", "MANAGER", "ADMIN", "TEACHER")
//			.antMatchers("/api/v1/attendance/**").hasAnyAuthority("MENTOR", "MANAGER")
			.antMatchers("/api/v1/students/**").hasAnyAuthority("MENTOR", "MANAGER","ADMIN", "TEACHER")
			.antMatchers("/api/v1/attendances/**").hasAnyAuthority("MENTOR", "MANAGER","ADMIN", "TEACHER")
			.antMatchers("/api/v1/chapters/**").hasAnyAuthority("MENTOR", "MANAGER","ADMIN", "TEACHER")
			.antMatchers("/api/v1/classes/**").hasAnyAuthority("MENTOR", "MANAGER","ADMIN", "TEACHER")
			.antMatchers("/api/v1/exams/**").hasAnyAuthority("MENTOR", "MANAGER","ADMIN", "TEACHER")
			.antMatchers("/api/v1/homeworks/**").hasAnyAuthority("MENTOR", "MANAGER","ADMIN", "TEACHER")
			.antMatchers("/api/v1/lessons/**").hasAnyAuthority("MENTOR", "MANAGER","ADMIN", "TEACHER")
			.antMatchers("/api/v1/comments/**").hasAnyAuthority("MENTOR", "MANAGER","ADMIN", "TEACHER")
			.antMatchers("/api/v1/videos/**").hasAnyAuthority("MENTOR", "MANAGER","ADMIN", "TEACHER")
			.antMatchers("/api/v1/teachers/**").hasAnyAuthority("MENTOR", "MANAGER","ADMIN", "TEACHER")
			.antMatchers("/api/v1/managers/**").hasAnyAuthority("MENTOR", "MANAGER","ADMIN", "TEACHER")
			.antMatchers("/api/v1/mentors/**").hasAnyAuthority("MENTOR", "MANAGER","ADMIN", "TEACHER")
			.antMatchers("/api/v1/clients/**").hasAnyAuthority("MENTOR", "MANAGER","ADMIN", "TEACHER","STUDENT")
			.antMatchers("/api/v1/users/login").permitAll()
			.antMatchers("/").permitAll()
			.antMatchers("/api/v1/files/**").permitAll()
			.anyRequest().authenticated()
			.and()
			.httpBasic()
			.and()
			.csrf().disable()
			.addFilterBefore(
					new JWTAuthenticationFilter("/api/v1/users/login", authenticationManager(),service), 
					UsernamePasswordAuthenticationFilter.class) 
			.addFilterBefore(
					new JWTAuthorizationFilter(service), 
					UsernamePasswordAuthenticationFilter.class);
	}
}

package com.vti.entity;

import java.io.Serializable;



import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


import javax.persistence.Table;


@Entity
@Table(name = "SubjectDefaultCost", catalog = "TCTmanagement")
public class SubjectDefaultCost implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Column(name = "default_id", nullable = false)
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "subject_name", nullable = false)
	private String subjectName;
	
	@Column(name = "grade", nullable = false)
	private int grade;
	
	@Column(name = "default_cost", nullable = false)
	private double cost;

	public double getCost() {
		return cost;
	}

	public void setCost(double cost) {
		this.cost = cost;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSubjectName() {
		return subjectName;
	}

	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}

	public int getGrade() {
		return grade;
	}

	public void setGrade(int grade) {
		this.grade = grade;
	}
	
	
	
	public SubjectDefaultCost() {
		// TODO Auto-generated constructor stub
	}

	public SubjectDefaultCost(int id, String subjectName, int grade) {
		super();
		this.id = id;
		this.subjectName = subjectName;
		this.grade = grade;
	}
	
	
	
}

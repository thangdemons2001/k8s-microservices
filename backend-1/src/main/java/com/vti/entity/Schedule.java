package com.vti.entity;

import java.io.Serializable;

import java.util.Date;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.vti.dto.ScheduleDTO;

@Entity
@Table(name = "Schedule", catalog = "TCTmanagement")
public class Schedule implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Column(name = "schedule_id", nullable = false)
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "start_time", nullable = false)
	@Temporal(TemporalType.TIME)
	@JsonFormat(pattern="HH:mm")
	private Date startTime;
	
	@Column(name = "end_time", nullable = false)
	@Temporal(TemporalType.TIME)
	@JsonFormat(pattern="HH:mm")
	private Date endTime;
	
	@Column(name = "schedule_tb", nullable = false)
	private String schedule;
	
	@ManyToOne
	@JoinColumn(name = "class_id")
	private Classroom classroom;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public String getSchedule() {
		return schedule;
	}

	public void setSchedule(String schedule) {
		this.schedule = schedule;
	}

	public Classroom getClassroom() {
		return classroom;
	}

	public void setClassroom(Classroom classroom) {
		this.classroom = classroom;
	}
	
	public Schedule() {
		// TODO Auto-generated constructor stub
	}

	public Schedule(int id, Date startTime, Date endTime, String schedule, Classroom classroom) {
		super();
		this.id = id;
		this.startTime = startTime;
		this.endTime = endTime;
		this.schedule = schedule;
		this.classroom = classroom;
	}
	
	public ScheduleDTO convertToDTO () {
		return new ScheduleDTO(id, startTime, endTime, schedule);
	}
	
	
	
}

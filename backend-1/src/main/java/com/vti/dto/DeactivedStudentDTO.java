package com.vti.dto;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;



public class DeactivedStudentDTO {
	
	private int id;
	
	
	private String firstName;
	
	
	private String lastName;
	
	private String fullName;
	
	private String facebookLink;
	
	private String school;


	private int grade;

	
	private String studentNumber;


	private String parentNumber;


	private String parentName;
	

	private String processStatus;
	
	@JsonFormat(pattern="dd-MM-yyyy")
	private Date leftDate;
	
	private List<ReasonLeftDTO> listReason;
	
	

	public DeactivedStudentDTO(int id, String firstName, String lastName, String fullName, String facebookLink,
			String school, int grade, String studentNumber, String parentNumber, String parentName,
			String processStatus, Date leftDate, List<ReasonLeftDTO> listReason) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.fullName = fullName;
		this.facebookLink = facebookLink;
		this.school = school;
		this.grade = grade;
		this.studentNumber = studentNumber;
		this.parentNumber = parentNumber;
		this.parentName = parentName;
		this.processStatus = processStatus;
		this.leftDate = leftDate;
		this.listReason = listReason;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getFacebookLink() {
		return facebookLink;
	}

	public void setFacebookLink(String facebookLink) {
		this.facebookLink = facebookLink;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getSchool() {
		return school;
	}

	public void setSchool(String school) {
		this.school = school;
	}

	public int getGrade() {
		return grade;
	}

	public void setGrade(int grade) {
		this.grade = grade;
	}

	public String getStudentNumber() {
		return studentNumber;
	}

	public void setStudentNumber(String studentNumber) {
		this.studentNumber = studentNumber;
	}

	public String getParentNumber() {
		return parentNumber;
	}

	public void setParentNumber(String parentNumber) {
		this.parentNumber = parentNumber;
	}

	public String getParentName() {
		return parentName;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	public String getProcessStatus() {
		return processStatus;
	}

	public void setProcessStatus(String processStatus) {
		this.processStatus = processStatus;
	}

	public Date getLeftDate() {
		return leftDate;
	}

	public void setLeftDate(Date leftDate) {
		this.leftDate = leftDate;
	}

	public List<ReasonLeftDTO> getListReason() {
		return listReason;
	}

	public void setListReason(List<ReasonLeftDTO> listReason) {
		this.listReason = listReason;
	}
	
	public DeactivedStudentDTO() {
		// TODO Auto-generated constructor stub
	}

	public DeactivedStudentDTO(int id, String firstName, String lastName, String school, int grade,
			String studentNumber, String parentNumber, String parentName, String processStatus, Date leftDate,
			List<ReasonLeftDTO> listReason) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.school = school;
		this.grade = grade;
		this.studentNumber = studentNumber;
		this.parentNumber = parentNumber;
		this.parentName = parentName;
		this.processStatus = processStatus;
		this.leftDate = leftDate;
		this.listReason = listReason;
	}
	
	
	
	
}

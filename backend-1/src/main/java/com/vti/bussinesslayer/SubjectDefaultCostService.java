package com.vti.bussinesslayer;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vti.datalayer.ISubjectDefaultCost;
import com.vti.entity.SubjectDefaultCost;
@Service
public class SubjectDefaultCostService implements ISubjectDefaultCostService {
	
	@Autowired
	private ISubjectDefaultCost repository;

	@Override
	public void createDefaultCost(SubjectDefaultCost dto) {
		// TODO Auto-generated method stub
		repository.save(dto);
	}

	@Override
	public void updateDefaultCost(int id, SubjectDefaultCost dto) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<SubjectDefaultCost> getAllDefaultCost() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SubjectDefaultCost getDefaultCostOfSubjectInGrade(String subject, int grade) {
		// TODO Auto-generated method stub
		return null;
	}
	
	

}

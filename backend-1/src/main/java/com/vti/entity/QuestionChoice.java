package com.vti.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "QuestionChoices", catalog = "TCTmanagement")
public class QuestionChoice implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Column(name = "question_choice_id", nullable = false)
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "question_id")
	private Question question;
	
	@Column(name = "is_correct", nullable = false)
	private String isCorrect;
	
	@Column(name = "content", nullable = true, length = 8000)
	private String content;
	
	@Column(name = "choice_image_link", nullable = true, length = 8000)
	private String imageLink;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Question getQuestion() {
		return question;
	}

	public void setQuestion(Question question) {
		this.question = question;
	}

	public String getIsCorrect() {
		return isCorrect;
	}

	public void setIsCorrect(String isCorrect) {
		this.isCorrect = isCorrect;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getImageLink() {
		return imageLink;
	}

	public void setImageLink(String imageLink) {
		this.imageLink = imageLink;
	}

	public QuestionChoice() {
		// TODO Auto-generated constructor stub
	}

	public QuestionChoice(long id, Question question, String isCorrect, String content, String imageLink) {
		super();
		this.id = id;
		this.question = question;
		this.isCorrect = isCorrect;
		this.content = content;
		this.imageLink = imageLink;
	}
	
	
}

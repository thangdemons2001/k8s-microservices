package com.vti.dto;

import java.util.ArrayList;


// only use for attendance 
public class StudentDTO10 {
	
	private int id;
		
	private String firstName;
	
	private String lastName;
		
	private String fullName;
	
	private String status;
	
	private String school;
	
	private int grade;
	
	private String studentNumber;
	
	private String parentNumber;
	
	private String parentName;
	
	private ArrayList<ClassroomDTO> listClass;

	private String avatarUrl;
	
	private String facebookUrl;
	
	private String subCostStauts;
	

	public String getSubCostStauts() {
		return subCostStauts;
	}


	public void setSubCostStauts(String subCostStauts) {
		this.subCostStauts = subCostStauts;
	}


	public StudentDTO10(int id, String firstName, String lastName, String fullName, String status, String school,
			int grade, String studentNumber, String parentNumber, String parentName, String avatarUrl,
			String facebookUrl, String subCostStatus) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.fullName = fullName;
		this.status = status;
		this.school = school;
		this.grade = grade;
		this.studentNumber = studentNumber;
		this.parentNumber = parentNumber;
		this.parentName = parentName;
		this.avatarUrl = avatarUrl;
		this.facebookUrl = facebookUrl;
		this.subCostStauts = subCostStatus;
	}



	public String getAvatarUrl() {
		return avatarUrl;
	}



	public void setAvatarUrl(String avatarUrl) {
		this.avatarUrl = avatarUrl;
	}



	public String getFacebookUrl() {
		return facebookUrl;
	}



	public void setFacebookUrl(String facebookUrl) {
		this.facebookUrl = facebookUrl;
	}



	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public String getFirstName() {
		return firstName;
	}



	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}



	public String getLastName() {
		return lastName;
	}



	public void setLastName(String lastName) {
		this.lastName = lastName;
	}



	public String getFullName() {
		return fullName;
	}



	public void setFullName(String fullName) {
		this.fullName = fullName;
	}



	public String getStatus() {
		return status;
	}



	public void setStatus(String status) {
		this.status = status;
	}



	public String getSchool() {
		return school;
	}



	public void setSchool(String school) {
		this.school = school;
	}



	public int getGrade() {
		return grade;
	}



	public void setGrade(int grade) {
		this.grade = grade;
	}



	public String getStudentNumber() {
		return studentNumber;
	}



	public void setStudentNumber(String studentNumber) {
		this.studentNumber = studentNumber;
	}



	public String getParentNumber() {
		return parentNumber;
	}



	public void setParentNumber(String parentNumber) {
		this.parentNumber = parentNumber;
	}



	public String getParentName() {
		return parentName;
	}



	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	

	public ArrayList<ClassroomDTO> getListClass() {
		return listClass;
	}

	

	public StudentDTO10(int id, String firstName, String lastName, String fullName, String status, String school,
			int grade, String studentNumber, String parentNumber, String parentName,
			ArrayList<ClassroomDTO> listClass) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.fullName = fullName;
		this.status = status;
		this.school = school;
		this.grade = grade;
		this.studentNumber = studentNumber;
		this.parentNumber = parentNumber;
		this.parentName = parentName;
		this.listClass = listClass;
	}

	

	public StudentDTO10(int id, String firstName, String lastName, String fullName, String status, String school,
			int grade, String studentNumber, String parentNumber, String parentName) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.fullName = fullName;
		this.status = status;
		this.school = school;
		this.grade = grade;
		this.studentNumber = studentNumber;
		this.parentNumber = parentNumber;
		this.parentName = parentName;
	}



	public void setListClass(ArrayList<ClassroomDTO> listClass) {
		this.listClass = listClass;
	}



	public StudentDTO10() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public String toString() {
		
		return this.fullName + " " + this.id;
	}
	
	@Override
	public int hashCode() {
		
		return this.id;
	}
	
	@Override
	public boolean equals(Object obj) {
		StudentDTO10 student = (StudentDTO10) obj;
		return this.id == student.id;
	}
	
}

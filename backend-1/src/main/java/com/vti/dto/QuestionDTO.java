package com.vti.dto;


public class QuestionDTO {
	
	private long id;
	
	private double score;
	
	private String type;
	
	private String content;
	
	private String imageLink;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public double getScore() {
		return score;
	}

	public void setScore(double score) {
		this.score = score;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getImageLink() {
		return imageLink;
	}

	public void setImageLink(String imageLink) {
		this.imageLink = imageLink;
	}
	
	public QuestionDTO() {
		// TODO Auto-generated constructor stub
	}

	public QuestionDTO(long id, double score, String type, String content, String imageLink) {
		super();
		this.id = id;
		this.score = score;
		this.type = type;
		this.content = content;
		this.imageLink = imageLink;
	}
	
	

}

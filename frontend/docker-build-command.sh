docker build -t thangdemons2001/iplat-frontend:latest \
 --build-arg REACT_APP_AVATAR_URL="http://spaceone-dev.link:30007/images"  \
 --build-arg REACT_APP_QUANLY_API_DOMAIN=http://java.spaceone-dev.link/api/v1  \
 --build-arg REACT_APP_LUYEN_DE_API_DOMAIN=https://luyendeapi.tamchitai.edu.vn/v1  \
 --build-arg REACT_APP_QUAN_LY_DOMAIN=https://quanly.tamchitai.edu.vn  \
 --build-arg REACT_APP_LUYEN_DE_DOMAIN=https://luyende.tamchitai.edu.vn  \
 --build-arg REACT_APP_SUBJECTS="Toán Đại,Toán Hình,Gia Sư Toán,Luyện Đề,Toán Cấp Tốc,Toán Nâng Cao,Văn,Tiếng Anh,Hóa,Sinh,Sử,Địa"  \
.
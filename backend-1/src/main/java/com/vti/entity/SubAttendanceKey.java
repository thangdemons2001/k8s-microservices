package com.vti.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;
@Embeddable
public class SubAttendanceKey implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Column(name = "student_id" , nullable = false)
	private int studentId;
	
	@Column(name = "class_id" , nullable = false)
	private int classroomId;
	
	@Column(name = "date_id", nullable = false)
	@Temporal(TemporalType.DATE)
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date dateId;
	
	public int getStudentId() {
		return studentId;
	}



	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}



	public Date getDateId() {
		return dateId;
	}



	public void setDateId(Date dateId) {
		this.dateId = dateId;
	}



	public SubAttendanceKey() {
	}

	

	public int getClassroomId() {
		return classroomId;
	}

	public void setClassroomId(int classroomId) {
		this.classroomId = classroomId;
	}



	public SubAttendanceKey(int studentId, int classroomId, Date dateId) {
		super();
		this.studentId = studentId;
		this.classroomId = classroomId;
		this.dateId = dateId;
	}

	
	
}

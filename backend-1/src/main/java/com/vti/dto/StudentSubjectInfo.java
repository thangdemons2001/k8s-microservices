package com.vti.dto;

import java.util.List;

public class StudentSubjectInfo {
	
	private String fullName;
	
	private int grade;
	
	private double score;
	
	private String school;
	
	private List<SubjectStatus> subjectStatus;
	
	private List<ClassroomDTO3> listClass;

	
	private String avatarUrl;
	
	private String facebookUrl;
	
	
	
	public String getFacebookUrl() {
		return facebookUrl;
	}

	public void setFacebookUrl(String facebookUrl) {
		this.facebookUrl = facebookUrl;
	}

	public String getAvatarUrl() {
		return avatarUrl;
	}

	public void setAvatarUrl(String avatarUrl) {
		this.avatarUrl = avatarUrl;
	}

	public double getScore() {
		return score;
	}

	public void setScore(double score) {
		this.score = score;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public int getGrade() {
		return grade;
	}

	public void setGrade(int grade) {
		this.grade = grade;
	}

	public String getSchool() {
		return school;
	}

	public void setSchool(String school) {
		this.school = school;
	}

	public List<SubjectStatus> getSubjectStatus() {
		return subjectStatus;
	}

	public void setSubjectStatus(List<SubjectStatus> subjectStatus) {
		this.subjectStatus = subjectStatus;
	}

	public List<ClassroomDTO3> getListClass() {
		return listClass;
	}

	public void setListClass(List<ClassroomDTO3> listClass) {
		this.listClass = listClass;
	}
	
	public StudentSubjectInfo() {
		// TODO Auto-generated constructor stub
	}

	public StudentSubjectInfo(String fullName, int grade, String school, List<SubjectStatus> subjectStatus,
			List<ClassroomDTO3> listClass) {
		super();
		this.fullName = fullName;
		this.grade = grade;
		this.school = school;
		this.subjectStatus = subjectStatus;
		this.listClass = listClass;
	}
	
	
	
	public StudentSubjectInfo(String fullName, int grade, double score, String school,
			List<SubjectStatus> subjectStatus, List<ClassroomDTO3> listClass, String avatarUrl) {
		super();
		this.fullName = fullName;
		this.grade = grade;
		this.score = score;
		this.school = school;
		this.subjectStatus = subjectStatus;
		this.listClass = listClass;
		this.avatarUrl = avatarUrl;
	}
	
	

}

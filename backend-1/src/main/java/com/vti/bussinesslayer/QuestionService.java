package com.vti.bussinesslayer;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vti.datalayer.IQuestionRepository;
import com.vti.entity.Question;

@Service
public class QuestionService implements IQuestionService {
	
	@Autowired
	private IQuestionRepository repository;

	@Override
	public void createQuestion(List<Question> question) {
		repository.saveAll(question);
	}

	@Override
	public void updateQuestion(Question questionDto) {
		Question question = repository.getById(questionDto.getId());
		if(questionDto.getContent() != null) {
			question.setContent(questionDto.getContent());
		}
		if(questionDto.getImageLink() != null) {
			question.setImageLink(questionDto.getImageLink());
		}
		if(questionDto.getScore() != 0) {
			question.setScore(questionDto.getScore());
		}
		if(questionDto.getType() != null) {
			question.setType(questionDto.getType());
		}
		
		
	}

	@Override
	public void deleteQuestion(long questionId) {
		Question question = repository.getById(questionId);
		repository.delete(question);
	}

	@Override
	public List<Question> getHomeWorkQuestion(int homeworkId) {
		return repository.findByHomework_Id(homeworkId);
	}

	@Override
	public Question getQuestion(long questionId) {
		return repository.getById(questionId);
	}
	
	

}

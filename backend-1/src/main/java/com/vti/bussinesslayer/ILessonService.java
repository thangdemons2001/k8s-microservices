package com.vti.bussinesslayer;

import java.util.List;

import com.vti.entity.Lesson;


public interface ILessonService {
	
	public void createLesson (Lesson lesson);
	
	public void deleteLesson (int id);
	
	public Lesson getLessonById (int id);
	
	public List<Lesson> getAllLessonByIds (List<Integer> ids);
	
	public void addExistLessonsToClass (int classId, List<Integer> lessonIds);
	
	public List<Lesson> searchLessonByIdOrByLessonName (String subject, int grade, String search, int page, int pageSize);
	
	public List<Lesson> getAllLessonInSubjectAtGrade (String subject, int grade, int page, int pageSize);
	
	public List<Lesson> getAllLessonInClass (int classId);
	
	public List<Lesson> getAllLessonOfChapterInClass(int classId, int chapterId);
	
	public Lesson getCurrentLessonInClass (int classId);
	
	public Lesson getLessonInClassToday (int classId);
	
	public List<Lesson> getAllLessonInAllClassByClassIds (List<Integer> listClassId);
}

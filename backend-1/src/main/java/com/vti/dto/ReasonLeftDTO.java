package com.vti.dto;

import javax.persistence.Column;

public class ReasonLeftDTO {
	
	
	private int id;


	private String reasonLeft;


	private String departmentName;


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getReasonLeft() {
		return reasonLeft;
	}


	public void setReasonLeft(String reasonLeft) {
		this.reasonLeft = reasonLeft;
	}


	public String getDepartmentName() {
		return departmentName;
	}


	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}
	
	public ReasonLeftDTO() {
		// TODO Auto-generated constructor stub
	}


	public ReasonLeftDTO(int id, String reasonLeft, String departmentName) {
		super();
		this.id = id;
		this.reasonLeft = reasonLeft;
		this.departmentName = departmentName;
	}
	
	
}

package com.vti.bussinesslayer;

import com.vti.entity.Video;

public interface IVideoService {
	
	public void createVideo (Video video);
	
	public Video getVideoById (int id);
	

}

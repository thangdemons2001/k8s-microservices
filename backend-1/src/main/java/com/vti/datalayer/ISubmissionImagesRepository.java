package com.vti.datalayer;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.vti.entity.SubmissionImages;

public interface ISubmissionImagesRepository
		extends JpaRepository<SubmissionImages, Long>, JpaSpecificationExecutor<SubmissionImages> {

}

package com.vti.bussinesslayer;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.vti.datalayer.IClassroomLessonRepository;
import com.vti.datalayer.IClassroomRepository;
import com.vti.datalayer.ILessonRepository;
import com.vti.entity.Classroom;
import com.vti.entity.ClassroomLesson;
import com.vti.entity.ClassroomLessonKey;
import com.vti.entity.Lesson;
@Service
public class LessonService implements ILessonService {
	
	@Autowired
	private ILessonRepository repository;
	
	@Autowired
	private IClassroomRepository classRepository;
	
	@Autowired
	private IClassroomLessonRepository classLessonRepository;
	
	@Override
	public void createLesson(Lesson lesson) {
		repository.save(lesson);
	}

	@Override
	public Lesson getLessonById(int id) {
		// TODO Auto-generated method stub
		return repository.findById(id);
	}

	@Override
	public List<Lesson> getAllLessonInClass(int classId) {
		return repository.findByClassroom_IdOrderByDateDesc(classId);
	}

	@Override
	public Lesson getCurrentLessonInClass(int classId) {
		Date now = new Date();
		Pageable pageable = PageRequest.of(0, 1);
		List<Lesson> lessons = repository.findTop1ByClassroom_IdAndDateBeforeOrderByDateDesc(classId,now,pageable);
		if(lessons.size() == 0) {
			return null;
		}
		Lesson currentLesson = lessons.get(0);
		
		return currentLesson;
	}

	@Override
	public Lesson getLessonInClassToday(int classId) {
		Date today = new Date();
		return repository.findByClassroom_IdAndDate(classId, today);
	}

	@Override
	public List<Lesson> getAllLessonOfChapterInClass(int classId, int chapterId) {
		return repository.findByClassroom_IdAndChapter_IdOrderByDateAsc(classId, chapterId);
	}

	@Override
	public void deleteLesson(int id) {
		Lesson lesson = repository.findById(id);
		repository.delete(lesson);
	}

	@Override
	public List<Lesson> getAllLessonByIds(List<Integer> ids) {
		return repository.findByIdIn(ids);
	}

	@Override
	public void addExistLessonsToClass(int classId, List<Integer> lessonIds) {
		
		Classroom clazz = classRepository.findById(classId);
		
		List<ClassroomLesson> listClassToAddLesson = new ArrayList<ClassroomLesson>();
		
		List<Lesson> getAllLessonsByIds = repository.findByIdIn(lessonIds);
		
		for(Lesson lesson: getAllLessonsByIds) {
		
			ClassroomLessonKey id = new ClassroomLessonKey(lesson.getId(), classId);
			
			ClassroomLesson addLessonToClass = new ClassroomLesson(id, lesson, clazz);
			
			listClassToAddLesson.add(addLessonToClass);
		}
		
		classLessonRepository.saveAll(listClassToAddLesson);
		
	}
	
	@Override
	public List<Lesson> getAllLessonInSubjectAtGrade(String subject, int grade, int page, int pageSize) {
		Pageable pageable = PageRequest.of(page, pageSize);
		return repository.findByClassroom_SubjectAndGradeOrderByDateDesc(subject, grade, pageable);
	}

	@Override
	public List<Lesson> getAllLessonInAllClassByClassIds(List<Integer> listClassId) {
		return repository.findByListCLassId(listClassId);
	}

	@Override
	public List<Lesson> searchLessonByIdOrByLessonName(String subject, int grade, String search, int page, int pageSize) {
		Pageable pageable = PageRequest.of(page, pageSize);
		
		try{
            int id = Integer.parseInt(search);
            return repository.findByIdOrderByDate(subject, grade, id, pageable);
        }
        catch (NumberFormatException ex){
            ex.printStackTrace();
        }
		
		return repository.findByLessonNameLikeOrderByDate(subject, grade, search, pageable);
	}

}

package com.vti.presentationlayer;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vti.bussinesslayer.IClassroomStudentService;
import com.vti.bussinesslayer.IStudentService;
import com.vti.bussinesslayer.IUserService;
import com.vti.dto.ClassroomDTO;
import com.vti.dto.ClassroomDTO7;
import com.vti.dto.ClassroomStudentDTO;
import com.vti.dto.StudentDTO;
import com.vti.dto.StudentDTO7;
import com.vti.dto.StudentDTO9;
import com.vti.dto.StudentStatisticDTO;
import com.vti.entity.Classroom;
import com.vti.entity.ClassroomStudent;
import com.vti.entity.ExamResult;
import com.vti.entity.Schedule;
import com.vti.entity.Student;
import com.vti.filter.Filter;

@RestController
@RequestMapping(value = "api/v1/students")
@CrossOrigin("*")
public class StudentController {

	
	@Autowired
	private IStudentService service;

	@Autowired
	private IClassroomStudentService classSerivce;

	@Autowired
	private IUserService userService;
	
	@Autowired
    private RedisTemplate<String, List<Student>> template;
	
//	@Autowired
//	public StudentController(RedisTemplate<String, List<Student>> redisTemplate){
//	  this.template = redisTemplate;
//	}

	@GetMapping("/")
	public List<StudentDTO> getAllStudent() {

		List<StudentDTO> res = new ArrayList<StudentDTO>();

		List<Student> query = service.getAllStudent();

		for (Student st : query) {
			res.add(st.convertToDTO());
		}

		return res;
	}

	@GetMapping("/grade/{grade}")
	public List<StudentDTO> getAllStudentInGrade(@PathVariable(name = "grade") int grade) {

		List<StudentDTO> res = new ArrayList<StudentDTO>();
		List<Student> students = new ArrayList<Student>();
		
		String cacheKey = "users:grade";
		
		List<Student> cached = template.opsForValue().get(cacheKey);
		
		if(cached != null) {
			students = cached;
		}else {
			List<Student> query = service.getListStudentInGrade(grade);
			students = query;
			template.opsForValue().set(cacheKey, query, Duration.ofMinutes(1));
		}
		
		for (Student st : students) {
			res.add(st.convertToDTO2(st.getAvatarUrl()));
		}

		return res;
	}
	
	@GetMapping("/active-status/")
	public List<StudentDTO9> getAllStudentWithActiveStatusInGrade(int grade, String status) {

		List<StudentDTO9> res = new ArrayList<StudentDTO9>();

		List<Student> query = service.getListStudentInGradeWithActiveStatus(grade,status);

		for (Student st : query) {
			res.add(st.convertToDTO9());
		}

		return res;
	}

	@PostMapping("/")
	public ResponseEntity<?> createStudent(@RequestBody Student student) {
		student.setStartDate(new Date());
		service.createStudent(student);
		return new ResponseEntity<Integer>(student.getId(), HttpStatus.OK);
	}
	
	// 1 - 6 của năm nay và 8- 12 của năm ngoái nếu thời điểm hiện tại vào các tháng 1 => 6
	// 1 - 6 của năm sau và 8 - 12 của năm nay nếu thời điểm hiện tại vào các tháng 8 => 12
	@GetMapping("/statistic")
	public ResponseEntity<?> getStasticsOfStudentAtGradeInMonth(int month, int grade) {
		
		Calendar cal = Calendar.getInstance();
		int thisYear = cal.get(Calendar.YEAR);
		int thisMonth = cal.get(Calendar.MONTH) + 1;
		int year = thisYear;
		
		if(thisMonth >= 1 && thisMonth <= 6 && month >= 8 && month <= 12 ) {
			year = thisYear - 1;
		}else if (thisMonth >= 8 && thisMonth <= 12 && month >= 1 && month <= 6) {
			year = thisYear + 1;
		}
		
		List<StudentStatisticDTO> res = new ArrayList<StudentStatisticDTO>();

		// 0 là học sinh mới, // 1 là học sinh nghỉ // 2 là chênh lệch // 3 là % so  // với học sinh mới 

		int countNewStudentInMonth = service.countNewStudentAtGradeInMonth(month, grade,year);
		int countDeactivedStudentInMonth = service.countDeactivedStudentAtGradeInMonth(month, grade,year);
		int allStudentThisMonth = service.countAllStudentAtGradeInMonth(month, grade,year);

		int countNewStudentInCurrentMonth = ((month - 1 ) > 0 ) ? service.countNewStudentAtGradeInMonth(month - 1, grade,year) :
																	service.countNewStudentAtGradeInMonth(12, grade,year - 1) 	;
		int countDeactivedStudentInCurrentMonth = ((month - 1 ) > 0 ) ? service.countDeactivedStudentAtGradeInMonth(month - 1, grade,year): 
																		service.countDeactivedStudentAtGradeInMonth(12, grade,year - 1);
		int allStudentCurrentMonth = allStudentThisMonth - countNewStudentInMonth;
		

		double precentNewStudent = (countNewStudentInCurrentMonth != 0)
				? ((double)(countNewStudentInMonth - countNewStudentInCurrentMonth) / countNewStudentInCurrentMonth) * 100
				: -96.6996; // số này sinh ra để thông báo rằng là tháng trước đéo có học sinh mới/nghỉ/chênh lệch nào

		double precentDeactivedStudent = (countDeactivedStudentInCurrentMonth != 0)
				? ((double)(countDeactivedStudentInMonth
						- countDeactivedStudentInCurrentMonth) / countDeactivedStudentInCurrentMonth) * 100
				: -96.6996;
		
		double precentDelta = (allStudentCurrentMonth != 0)
				? ((double)(allStudentThisMonth
						- allStudentCurrentMonth) / allStudentCurrentMonth) * 100
				: -96.6996;

		
		StudentStatisticDTO newStudent = new StudentStatisticDTO(countNewStudentInMonth, precentNewStudent);
		StudentStatisticDTO deactivedStudent = new StudentStatisticDTO(countDeactivedStudentInMonth, precentDeactivedStudent);
		StudentStatisticDTO deltaStudent = new StudentStatisticDTO(allStudentThisMonth, precentDelta);
		
		res.add(newStudent);
		res.add(deactivedStudent);
		res.add(deltaStudent);


		return new ResponseEntity<List<StudentStatisticDTO>>(res, HttpStatus.OK);
	}
	// 1 - 6 của năm nay và 8- 12 của năm ngoái nếu thời điểm hiện tại vào các tháng 1 => 6
		// 1 - 6 của năm sau và 8 - 12 của năm nay nếu thời điểm hiện tại vào các tháng 8 => 12
	@GetMapping("/statistic/new")
	public ResponseEntity<?> getStasticsOfNewStudentAtGradeAllYear(int grade) {
		Calendar cal = Calendar.getInstance();
		
		int thisYear = cal.get(Calendar.YEAR);
		int thisMonth = cal.get(Calendar.MONTH) + 1;
		
		System.out.println(thisMonth);
		
		// 0 la thang 8 => 10 la thang 6 khong tinh thang 7
		int[] res = new int [11];
		for (int month = 8; month <= 12 ; month ++ ) {
			if(thisMonth >= 8 && thisMonth <= 12) {
				int newStudentOfMonth = service.countNewStudentAtGradeInMonth(month, grade,thisYear);
				res[month - 8] = newStudentOfMonth;
			}else {
				int newStudentOfMonth = service.countNewStudentAtGradeInMonth(month, grade, thisYear - 1);
				res[month - 8] = newStudentOfMonth;
			}
			
		}
		//5 - 1 6 -2 7 -3 8 -4 9 - 5 10 - 6
		for (int month = 1 ; month <= 6 ; month ++) {
			if(thisMonth >= 1 && thisMonth <= 6) {
				int newStudentOfMonth = service.countNewStudentAtGradeInMonth(month, grade, thisYear);
				res[month + 4] = newStudentOfMonth;
			}else {
				int newStudentOfMonth = service.countNewStudentAtGradeInMonth(month, grade, thisYear + 1);
				res[month + 4] = newStudentOfMonth;
			}
			
		}

		return new ResponseEntity<int[]>(res, HttpStatus.OK);
	}
	
	// 1 - 6 của năm nay và 8- 12 của năm ngoái nếu thời điểm hiện tại vào các tháng 1 => 6
	// 1 - 6 của năm sau và 8 - 12 của năm nay nếu thời điểm hiện tại vào các tháng 8 => 12
	@GetMapping("/statistic/deactived")
	public ResponseEntity<?> getStasticsOfDeactivedStudentAtGradeAllYear(int grade) {
		Calendar cal = Calendar.getInstance();
		
		int thisYear = cal.get(Calendar.YEAR);
		int thisMonth = cal.get(Calendar.MONTH) + 1;
		
		System.out.println(thisMonth);
		
		int[] res = new int [11];
		for (int month = 8; month <= 12 ; month ++ ) { // từ tháng 8 - 12
			if(thisMonth >= 8 && thisMonth <= 12) {
				int deactivedStudentOfMonth = service.countDeactivedStudentAtGradeInMonth(month, grade,thisYear);
				res[month - 8] = deactivedStudentOfMonth;
			}
			else {
				int deactivedStudentOfMonth = service.countDeactivedStudentAtGradeInMonth(month, grade, thisYear - 1);
				res[month - 8] = deactivedStudentOfMonth;
			}
		}
		//5 - 1 6 -2 7 -3 8 -4 9 - 5 10 - 6
		for (int month = 1 ; month <= 6 ; month ++) { // từ tháng 1 => 6
			if(thisMonth >= 1 && thisMonth <= 6) {
				System.out.println("vào đây cơ!");
				int deactivedStudentOfMonth = service.countDeactivedStudentAtGradeInMonth(month, grade, thisYear);
				res[month + 4] = deactivedStudentOfMonth;
			}else {
				System.out.println("vào đây nè!");
				int deactivedStudentOfMonth = service.countDeactivedStudentAtGradeInMonth(month, grade, thisYear + 1);
				res[month + 4] = deactivedStudentOfMonth;
			}
			
		}


		return new ResponseEntity<int[]>(res, HttpStatus.OK);
	}

	@DeleteMapping("/{studentId}")
	public ResponseEntity<?> deleteUser(@PathVariable(name = "studentId") int studentId) {
		userService.deleteUser(studentId);
		return new ResponseEntity<String>("delete successful!", HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public StudentDTO getStudentById(@PathVariable(name = "id") int studentId) {
		return service.findStudentById(studentId).convertToDTO();
	}

	@GetMapping("/phone/{phoneNumber}")
	public StudentDTO getStudentByPhoneNumber(@PathVariable(name = "phoneNumber") String number) {
		return service.findStudentByPhoneNumber(number).convertToDTO();
	}

	@GetMapping("/{id}/classes")
	public List<ClassroomDTO> getStudentClass(@PathVariable(name = "id") int studentId) {
		List<Classroom> studentClasses = classSerivce.findStudentClass(studentId);
		List<ClassroomDTO> res = new ArrayList<ClassroomDTO>();
		for (Classroom clazz : studentClasses) {
			res.add(clazz.convertToDTO());
		}
		return res;
	}

	@GetMapping("/{id}/classes/schedule/now")
	public List<ClassroomDTO7> getStudentClassNow(@PathVariable(name = "id") int studentId, String schedule)
			throws ParseException {
		// only take time to compare
		Date date = Calendar.getInstance().getTime();
		DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
		String now = dateFormat.format(date);
		// get today without year month day
		Date today = new SimpleDateFormat("HH:mm:ss").parse(now);

		List<ClassroomStudent> studentClasses = classSerivce.findStudentClassroom(studentId);

		List<ClassroomDTO7> res = new ArrayList<ClassroomDTO7>();
		Calendar c = Calendar.getInstance();

		for (ClassroomStudent clazzSt : studentClasses) {
			Classroom clazz = clazzSt.getClassroom();
			boolean isExist = false; // flag to check lịch học của các lớp có tồn tại không? tại thời điểm hs điểm
										// danh
			for (Schedule clazzSchedule : clazz.getListSchedule()) {
				// convert date -> string, string -> date to compare time with Date now
				String startString = dateFormat.format(clazzSchedule.getStartTime());
				Date startTime = new SimpleDateFormat("HH:mm:ss").parse(startString);
				// minus startTime to 30 mins
				c.setTime(startTime);
				c.add(Calendar.YEAR, 0);
				c.add(Calendar.MONTH, 0);
				c.add(Calendar.DATE, 0);
				c.add(Calendar.HOUR, 0);
				c.add(Calendar.MINUTE, -30);
				c.add(Calendar.SECOND, 0);
				// Starting attendance time
				Date startringAttendance = c.getTime();
				// add startTime to 30 minus
				c.add(Calendar.MINUTE, 60);
				// ending attendance time
				Date endingAttendance = c.getTime();
				if (clazzSchedule.getSchedule().equals(schedule) && today.after(startringAttendance)
						&& today.before(endingAttendance)) {
					isExist = true;

					res.add(clazz.convertToDTO7(schedule, clazzSchedule.getStartTime(), clazzSchedule.getEndTime(),
							clazzSt.getStatus(),clazzSt.getSubStatus()));
					break;
				}
			}

		}
		return res;
	}

	@PutMapping("/{id}/classes")
	public ResponseEntity<?> updateStudentClass(@PathVariable(name = "id") int studentId,
			@RequestBody List<ClassroomStudentDTO> listClass) {
		classSerivce.updateStudentClass(studentId, listClass);
		return new ResponseEntity<String>("Update successful!", HttpStatus.OK);
	}

	@PostMapping("/{id}/classes")
	public ResponseEntity<?> createStudentClass(@PathVariable(name = "id") int studentId,
			@RequestBody List<ClassroomStudentDTO> listClass) {
		classSerivce.updateStudentClass(studentId, listClass);
		return new ResponseEntity<String>("create successful!", HttpStatus.OK);
	}

	@PutMapping("/{id}")
	public ResponseEntity<?> updateStudent(@PathVariable(name = "id") int studentId, @RequestBody Student dto) {
		service.updateStudent(dto);
		return new ResponseEntity<String>("Update successful!", HttpStatus.OK);
	}

	@PutMapping("/{id}/status/")
	public ResponseEntity<?> updateStudentStatus(@PathVariable(name = "id") int studentId, @RequestBody Student dto) {
		userService.updateStatusUser(studentId, dto.getStatus());
		return new ResponseEntity<String>("Update successful!", HttpStatus.OK);
	}

	@GetMapping("/status/{status}")
	public List<StudentDTO> getStudentByStatus(@PathVariable(name = "status") String status) {
		List<StudentDTO> res = new ArrayList<StudentDTO>();

		List<Student> query = service.findStudentByStatus(status);

		for (Student st : query) {
			res.add(st.convertToDTO2());
		}

		return res;
	}

	@GetMapping("/avg-mark/{grade}")
	public List<StudentDTO7> getAllStudentAvgMarkInSubjectOfGrade(@PathVariable(name = "grade") int grade,
			String subjectName) {
		List<StudentDTO7> res = service.getListStudentAvgMarkSubjectInGrade(grade, subjectName);

		return res;
	}

	@GetMapping("/avg-mark/weak/{grade}")
	public List<StudentDTO7> getAllWeekStudentAvgMarkInSubjectOfGrade(@PathVariable(name = "grade") int grade,
			String subjectName) {
		List<StudentDTO7> query = service.getListStudentAvgMarkSubjectInGrade(grade, subjectName);

		List<StudentDTO7> res = new ArrayList<StudentDTO7>();
		for (StudentDTO7 st : query) {
			if (st.getAvgMark() <= 5 && st.getAvgMark() > 0) {
				res.add(st);
			}
		}

		return res;
	}

	@GetMapping("/avg-mark/statistic/{grade}")
	public int[] getSubjectAvgMarkStatisticInGrade(@PathVariable(name = "grade") int grade, String subjectName) {
		int res[] = new int[6];

		for (int i = 0; i < 5; i++) {
			res[i] = 0;
		}
		List<StudentDTO7> results = service.getListStudentAvgMarkSubjectInGrade(grade, subjectName);

		for (StudentDTO7 result : results) {
			if (result.getAvgMark() > 0 && result.getAvgMark() < 3) {
				res[0]++;
			}
			if (result.getAvgMark() >= 3 && result.getAvgMark() < 5) {
				res[1]++;
			}
			if (result.getAvgMark() >= 5 && result.getAvgMark() < 7) {
				res[2]++;
			}
			if (result.getAvgMark() >= 7 && result.getAvgMark() < 8) {
				res[3]++;
			}
			if (result.getAvgMark() >= 8 && result.getAvgMark() < 9) {
				res[4]++;
			}
			if (result.getAvgMark() >= 9 && result.getAvgMark() <= 10) {
				res[5]++;
			}
		}

		return res;
	}
}

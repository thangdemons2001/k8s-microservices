package com.vti.dto;




// only use for compute avgMark of a student 
public class StudentDTO7 implements Comparable<StudentDTO7> {
	
	private int id;
		
	private String firstName;
	
	private String lastName;
		
	private String fullName;
	
	private String school;
	
	private int grade;
	
	private double avgMark;
	
	private String studentNumber;
	
	private String parentNumber;
	
	private String parentName;
	
	private int rank;
	
	private double score;
	
	private String avatarUrl;
	
	private String facebookUrl;
	
	

	public String getFacebookUrl() {
		return facebookUrl;
	}





	public void setFacebookUrl(String facebookUrl) {
		this.facebookUrl = facebookUrl;
	}


	
	

	public StudentDTO7(int id, String firstName, String lastName, String fullName, String school, int grade,
			double avgMark, String studentNumber, String parentNumber, String parentName, double score,
			String avatarUrl, String facebookUrl) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.fullName = fullName;
		this.school = school;
		this.grade = grade;
		this.avgMark = avgMark;
		this.studentNumber = studentNumber;
		this.parentNumber = parentNumber;
		this.parentName = parentName;
		this.score = score;
		this.avatarUrl = avatarUrl;
		this.facebookUrl = facebookUrl;
	}





	public StudentDTO7(int id, String firstName, String lastName, String fullName, String school, int grade,
			double avgMark, String studentNumber, String parentNumber, String parentName, int rank, double score,
			String avatarUrl, String facebookUrl) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.fullName = fullName;
		this.school = school;
		this.grade = grade;
		this.avgMark = avgMark;
		this.studentNumber = studentNumber;
		this.parentNumber = parentNumber;
		this.parentName = parentName;
		this.rank = rank;
		this.score = score;
		this.avatarUrl = avatarUrl;
		this.facebookUrl = facebookUrl;
	}





	public StudentDTO7(int id, String firstName, String lastName, String fullName, String school, int grade,
			double avgMark, String studentNumber, String parentNumber, String parentName, int rank) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.fullName = fullName;
		this.school = school;
		this.grade = grade;
		this.avgMark = avgMark;
		this.studentNumber = studentNumber;
		this.parentNumber = parentNumber;
		this.parentName = parentName;
		this.rank = rank;
	}





	public int getRank() {
		return rank;
	}





	public void setRank(int rank) {
		this.rank = rank;
	}

	



	public double getScore() {
		return score;
	}





	public void setScore(double score) {
		this.score = score;
	}


	


	public String getAvatarUrl() {
		return avatarUrl;
	}





	public void setAvatarUrl(String avatarUrl) {
		this.avatarUrl = avatarUrl;
	}


	
	

	public StudentDTO7(int id, String firstName, String lastName, String fullName, String school, int grade,
			double avgMark, String studentNumber, String parentNumber, String parentName, double score,
			String avatarUrl) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.fullName = fullName;
		this.school = school;
		this.grade = grade;
		this.avgMark = avgMark;
		this.studentNumber = studentNumber;
		this.parentNumber = parentNumber;
		this.parentName = parentName;
		this.score = score;
		this.avatarUrl = avatarUrl;
	}





	public StudentDTO7(int id, String firstName, String lastName, String fullName, String school, int grade,
			double avgMark, String studentNumber, String parentNumber, String parentName, int rank, double score,
			String avatarUrl) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.fullName = fullName;
		this.school = school;
		this.grade = grade;
		this.avgMark = avgMark;
		this.studentNumber = studentNumber;
		this.parentNumber = parentNumber;
		this.parentName = parentName;
		this.rank = rank;
		this.score = score;
		this.avatarUrl = avatarUrl;
	}





	public StudentDTO7(int id, String firstName, String lastName, String fullName,  String school,
			int grade, double avgMark, String studentNumber, String parentNumber, String parentName) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.fullName = fullName;
		this.school = school;
		this.grade = grade;
		this.avgMark = avgMark;
		this.studentNumber = studentNumber;
		this.parentNumber = parentNumber;
		this.parentName = parentName;
	}





	public int getId() {
		return id;
	}





	public void setId(int id) {
		this.id = id;
	}





	public String getFirstName() {
		return firstName;
	}





	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}





	public String getLastName() {
		return lastName;
	}





	public void setLastName(String lastName) {
		this.lastName = lastName;
	}





	public String getFullName() {
		return fullName;
	}





	public void setFullName(String fullName) {
		this.fullName = fullName;
	}









	public String getSchool() {
		return school;
	}





	public void setSchool(String school) {
		this.school = school;
	}





	public int getGrade() {
		return grade;
	}





	public void setGrade(int grade) {
		this.grade = grade;
	}





	public double getAvgMark() {
		return avgMark;
	}





	public void setAvgMark(double avgMark) {
		this.avgMark = avgMark;
	}





	public String getStudentNumber() {
		return studentNumber;
	}





	public void setStudentNumber(String studentNumber) {
		this.studentNumber = studentNumber;
	}





	public String getParentNumber() {
		return parentNumber;
	}





	public void setParentNumber(String parentNumber) {
		this.parentNumber = parentNumber;
	}





	public String getParentName() {
		return parentName;
	}





	public void setParentName(String parentName) {
		this.parentName = parentName;
	}





	public StudentDTO7() {
		// TODO Auto-generated constructor stub
	}


	


	public StudentDTO7(int id, String firstName, String lastName, String fullName, String school, int grade,
			double avgMark, String studentNumber, String parentNumber, String parentName, int rank, double score) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.fullName = fullName;
		this.school = school;
		this.grade = grade;
		this.avgMark = avgMark;
		this.studentNumber = studentNumber;
		this.parentNumber = parentNumber;
		this.parentName = parentName;
		this.rank = rank;
		this.score = score;
	}





	@Override
	public int compareTo(StudentDTO7 o) {
		// TODO Auto-generated method stub
		if (this.avgMark == o.getAvgMark())
	        return 0;
	    if (this.avgMark < o.getAvgMark()) return 1;
	    else if (this.avgMark == o.getAvgMark()) return 0;
	    else return -1;

	}
	
	
}

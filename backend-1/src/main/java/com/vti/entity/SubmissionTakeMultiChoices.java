package com.vti.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "SubmissionTakeMultiChoices", catalog = "TCTmanagement")
public class SubmissionTakeMultiChoices implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Column(name = "take_id", nullable = false)
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "question_id")
	private Question question;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "question_choice_id")
	private QuestionChoice choice;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "submission_id")
	private StudentHomeWork submission;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Question getQuestion() {
		return question;
	}

	public void setQuestion(Question question) {
		this.question = question;
	}

	public QuestionChoice getChoice() {
		return choice;
	}

	public void setChoice(QuestionChoice choice) {
		this.choice = choice;
	}

	public StudentHomeWork getSubmission() {
		return submission;
	}

	public void setSubmission(StudentHomeWork submission) {
		this.submission = submission;
	}
	
	public SubmissionTakeMultiChoices() {
		// TODO Auto-generated constructor stub
	}

	public SubmissionTakeMultiChoices(long id, Question question, QuestionChoice choice, StudentHomeWork submission) {
		super();
		this.id = id;
		this.question = question;
		this.choice = choice;
		this.submission = submission;
	}
	
}

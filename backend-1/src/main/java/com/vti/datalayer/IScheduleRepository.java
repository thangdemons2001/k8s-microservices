package com.vti.datalayer;










import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;


import com.vti.entity.Schedule;





public interface IScheduleRepository extends JpaRepository<Schedule, Integer>, JpaSpecificationExecutor<Schedule> {
		
		List<Schedule> findByClassroom_Id (int classId);
}

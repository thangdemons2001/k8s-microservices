package com.vti.bussinesslayer;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.vti.datalayer.ISubAttendanceRepository;
import com.vti.entity.SubAttendance;
@Service
public class SubAttendanceService implements ISubAttendanceService {
	
	@Autowired
	private ISubAttendanceRepository repository;

	@Override
	public List<SubAttendance> getListSubAttendanceInClassToday(int classId, Date dateId) {
		return repository.findById_ClassroomIdAndId_DateId(classId, dateId);
	}

	@Override
	public void studentAttenCompensate(SubAttendance subAtten) {
		repository.save(subAtten);
	}

	@Override
	public void deleteAttenCompensate(int studentId, int classId, Date dateId) {
		SubAttendance atten = repository.findById_ClassroomIdAndId_DateIdAndId_StudentId(classId, dateId, studentId);
		repository.delete(atten);
	}

}

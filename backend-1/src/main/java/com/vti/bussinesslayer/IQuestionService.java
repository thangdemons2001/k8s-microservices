package com.vti.bussinesslayer;

import java.util.List;

import com.vti.entity.Question;

public interface IQuestionService {
		public void createQuestion (List<Question> question);
		
		public void updateQuestion (Question questionId);
		
		public void deleteQuestion (long questionId);
		
		public List<Question> getHomeWorkQuestion (int homeworkId);
		
		public Question getQuestion (long questionId);
}

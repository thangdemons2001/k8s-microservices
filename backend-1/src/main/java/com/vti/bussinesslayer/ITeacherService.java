package com.vti.bussinesslayer;

import java.util.List;

import com.vti.entity.Teacher;

public interface ITeacherService {
	public List<Teacher> findTeacherBySubjectName(String subjectName);
	
	public Teacher findTeacherById(int id);
	
	public void createTeacher (Teacher teacher);
	
	public List<Teacher> findAllTeacher();
	
}

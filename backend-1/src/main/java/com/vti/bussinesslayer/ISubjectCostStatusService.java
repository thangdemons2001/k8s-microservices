package com.vti.bussinesslayer;

import java.util.List;

import com.vti.entity.SubjectCostStatus;

public interface ISubjectCostStatusService {
	
	public SubjectCostStatus getCostStatusOfSubjectInGrade (String subject, int grade);
	
	public void updateCostStatusOfSubjectInGrade (SubjectCostStatus dto);
	
	public void createSubjectCostStatus (SubjectCostStatus dto);
	
	public List<SubjectCostStatus> findAllCostStatusRequirementAtGrade (int grade);

}

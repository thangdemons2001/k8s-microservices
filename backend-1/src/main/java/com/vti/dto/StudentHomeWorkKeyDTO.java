package com.vti.dto;








public class StudentHomeWorkKeyDTO  {

	
	
	private int homeWorkId;
	

	private int studentId;
	
	
	
	public int getHomeWorkId() {
		return homeWorkId;
	}

	public void setHomeWorkId(int homeWorkId) {
		this.homeWorkId = homeWorkId;
	}

	public int getStudentId() {
		return studentId;
	}

	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}

	public StudentHomeWorkKeyDTO() {
		// TODO Auto-generated constructor stub
	}

	public StudentHomeWorkKeyDTO(int homeWorkId, int studentId) {
		super();
		this.homeWorkId = homeWorkId;
		this.studentId = studentId;
	}
	
	
	
	
}

package com.vti.datalayer;







import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.vti.entity.Video;


public interface IVideoRepository extends JpaRepository<Video, Integer>, JpaSpecificationExecutor<Video> {
		
	public Video findById (int id);
	
	
		
}

package com.vti.datalayer;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;


import com.vti.entity.SubAttendance;
import com.vti.entity.SubAttendanceKey;


public interface ISubAttendanceRepository extends JpaRepository<SubAttendance, SubAttendanceKey>, JpaSpecificationExecutor<SubAttendance>{
			
	
	public List<SubAttendance> findById_ClassroomIdAndId_DateId( int classId, Date dateId);
	
	public SubAttendance findById_ClassroomIdAndId_DateIdAndId_StudentId (int classId, Date dateId, int studentId);
}

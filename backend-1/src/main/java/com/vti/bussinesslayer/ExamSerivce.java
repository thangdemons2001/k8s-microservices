package com.vti.bussinesslayer;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vti.datalayer.IExamRepository;
import com.vti.datalayer.IExamResultRepository;
import com.vti.dto.ExamResultDTO2;
import com.vti.entity.Classroom;
import com.vti.entity.Exam;
import com.vti.entity.ExamResult;

@Service
public class ExamSerivce implements IExamService {

	@Autowired
	private IExamRepository repository;

	@Autowired
	private IExamResultRepository examResultRepository;

	@Override
	public void createExam(Exam exam) {
		repository.save(exam);
	}

	@Override
	public List<ExamResultDTO2> findAllSubjectExamInMonthAtGrade(int month, String subject, int grade) {

		List<ExamResultDTO2> result = new ArrayList<ExamResultDTO2>();

		Calendar cal = Calendar.getInstance();

		int year = cal.get(Calendar.YEAR);

		cal.set(Calendar.DATE, 25);
		cal.set(Calendar.MONTH, month - 1);
		cal.set(Calendar.YEAR, year);

		cal.set(Calendar.DAY_OF_MONTH, 1);
		Date firstDayOfMonth = cal.getTime();
		cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DATE));
		Date lastDayOfMonth = cal.getTime();

		List<ExamResult> query = examResultRepository.findSubjectExamInMonthOfGrade(firstDayOfMonth, subject, grade,
				lastDayOfMonth);

		for (ExamResult res : query) {
			Classroom tempClazz = res.getClassroom();
			String teacherName = tempClazz.getTeacherId().getFullName();
			String className = tempClazz.getClassName();
			int classId = tempClazz.getId();
			Exam exam = res.getExam();
			ExamResultDTO2 temp = new ExamResultDTO2(exam.getId(), exam.getName(), exam.getCreatedDate(),
					exam.getType().getName(), teacherName, className,classId,exam.getTestingSystemId());
			result.add(temp);
		}

		return result;
	}

	@Override
	public void deleteExam(int examId) {
			Exam exam = repository.findById(examId);
			repository.delete(exam);
	}

}

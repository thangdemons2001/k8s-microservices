package com.vti.presentationlayer;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vti.bussinesslayer.IChapterService;
import com.vti.bussinesslayer.IClassroomService;
import com.vti.bussinesslayer.IHomeWorkService;
import com.vti.bussinesslayer.ILessonService;
import com.vti.bussinesslayer.IVideoService;
import com.vti.entity.Chapter;




@RestController
@RequestMapping(value = "api/v1/chapters")
@CrossOrigin("*")
public class ChapterController {
	
	@Autowired
	private ILessonService service;
	
	@Autowired
	private IVideoService videoSerivce;
	
	@Autowired
	private IChapterService chapterService;
	
	@Autowired
	private IHomeWorkService homeWorkSerivce;
	
	@Autowired
	private IClassroomService classService;
	
	@PostMapping("/")
	public ResponseEntity<?> createChapter(@RequestBody Chapter chapterDto) {
		Chapter chapter = new Chapter();
		chapter.setChapterName(chapterDto.getChapterName());
		chapter.setGrade(chapterDto.getGrade());
		chapter.setSubject(chapterDto.getSubject());
		chapterService.createChapter(chapter);
		
		return new ResponseEntity<String>("create successful!", HttpStatus.OK);
	}
	@GetMapping("/grade/{grade}")
	public List<Chapter> getAllSubjectChapterInGrade(@PathVariable(name = "grade") int grade, String subject) {
		
		return chapterService.getAllChapterSubjectInGrade(subject, grade);
	}

}

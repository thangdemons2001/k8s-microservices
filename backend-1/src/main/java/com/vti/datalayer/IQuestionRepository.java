package com.vti.datalayer;








import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.vti.entity.Question;





public interface IQuestionRepository extends JpaRepository<Question, Long>, JpaSpecificationExecutor<Question> {
		
	public List<Question> findByHomework_Id (int homeworkID);
}

package com.vti.bussinesslayer;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.vti.datalayer.IExamResultRepository;
import com.vti.datalayer.IStudentRepository;


import com.vti.dto.ClassroomDTO3;
import com.vti.dto.ExamResultDTO3;
import com.vti.dto.SortByAvgMark;
import com.vti.dto.StudentDTO7;
import com.vti.dto.StudentSubjectInfo;
import com.vti.dto.SubjectStatus;
import com.vti.entity.Classroom;
import com.vti.entity.ClassroomStudent;
import com.vti.entity.ExamResult;
import com.vti.entity.Student;
import com.vti.filter.Filter;



@Service
public class StudentService implements IStudentService {
	
	@Autowired
	private IStudentRepository repository;
	
	@Autowired
	private IExamResultRepository examRepository;
	
	
	
	public int getRankOfStudentInSubject(int studentId, int grade, String subjectName) {
		
				// lay id cua tat ca hoc sinh
				List<Integer> studentsId = examRepository.findSubjectStudentAvgMarkOfStudentInGrade(subjectName, grade);
				System.out.println(studentsId);
				
				// lay ranking cua tat ca hoc sinh
				List<Integer> studentsRank = examRepository.findSubjectStudentRankAvgMarkOfStudentInGrade(subjectName, grade);
				System.out.println(studentsRank);
				
				// map id cua hoc sinh voi rank cua hoc sinh
				Map<Integer, Integer> studentMapRank = new HashMap<Integer, Integer>();
				
				for(int i = 0 ; i < studentsId.size() ; i ++) {
					
					studentMapRank.put(studentsId.get(i), studentsRank.get(i));
				}
				
				int res = (studentMapRank.get(studentId) != null ) ? studentMapRank.get(studentId) : 0;
		
		return res;
	}

	@Override
	public List<Student> getListStudentByClassId(int id, short page, short pageSize, Filter filter) {
		// TODO Auto-generated method stub
		Pageable pageable = PageRequest.of(page, pageSize, Sort.by("firstName").ascending());
		
		Specification<Student> where = inClass(id);
		where = where.and(studentFirstNameLike(filter.getSearch()));
		return repository.findAll(where,pageable).toList();
	}
	
	
	public Specification<Student> studentFirstNameLike(String name){
		return new Specification<Student>() {

			@Override
			public Predicate toPredicate(Root<Student> root, CriteriaQuery<?> query,
					CriteriaBuilder criteriaBuilder) {
				// TODO Auto-generated method stub
				return criteriaBuilder.like(root.get("firstName"),"%"+ name + "%");
			}
			
		};
	}
	public Specification<Student> inClass(int id){
		return new Specification<Student>() {

			@Override
			public Predicate toPredicate(Root<Student> root, CriteriaQuery<?> query,
					CriteriaBuilder criteriaBuilder) {
				// TODO Auto-generated method stub
				return criteriaBuilder.equal(root.join("listClass").get("classroom").get("id"),id);
			}
			
		};
	}
	public Specification<Student> inGrade(int grade){
		return new Specification<Student>() {

			@Override
			public Predicate toPredicate(Root<Student> root, CriteriaQuery<?> query,
					CriteriaBuilder criteriaBuilder) {
				// TODO Auto-generated method stub
				return criteriaBuilder.equal(root.get("grade"),grade);
			}
			
		};
	}
	

	@Override
	public List<Student> getAllStudent() {
		// TODO Auto-generated method stub
		return repository.findAll();
	}


	@Override
	public void updateStudent(Student studentdto) {
		Student st = repository.findById(studentdto.getId());
		if(studentdto.getFirstName() != null) {
			st.setFirstName(studentdto.getFirstName());
		}
		if(studentdto.getLastName() != null) {
			st.setLastName(studentdto.getLastName());
		}
		if(studentdto.getGrade() != 0) {
			st.setGrade(studentdto.getGrade());
		}
		if(studentdto.getParentName() != null) {
			st.setParentName(studentdto.getParentName());
		}
		if(studentdto.getParentNumber() != null) {
			st.setParentNumber(studentdto.getParentNumber());
		}
		if(studentdto.getSchool() != null) {
			st.setSchool(studentdto.getSchool());
		}
		if(studentdto.getSocial() != null) {
			st.setSocial(studentdto.getSocial());
		}
		if(studentdto.getStatus() != null) {
			st.setStatus(studentdto.getStatus());
		}
		if(studentdto.getRole() != null) {
			st.setRole(studentdto.getRole());
		}
		if(studentdto.getPassword() != null) {
			st.setPassword(studentdto.getPassword() );
		}
		// TODO Auto-generated method stub
		repository.save(st);
	}


	@Override
	public Student findStudentById(int id) {
		// TODO Auto-generated method stub
		return repository.findById(id);
	}


	@Override
	public void createStudent(Student student) {
		// TODO Auto-generated method stub
		student.setCreatedDate(new Date());
		repository.save(student);
	}


	@Override
	public Student findStudentByPhoneNumber(String number) {
		
		return repository.findByStudentNumber(number);
	}


	@Override
	public List<Student> findStudentByStatus(String status) {
		return repository.findByStatus(status);
	}


	@Override
	public List<Student> getListStudentInGrade(int grade) {
		return repository.findByGrade(grade);
	}


	@Override
	public List<StudentDTO7> getListStudentAvgMarkSubjectInGrade(int grade, String subjectName) {
		
		
		// lấy tất cả học sinh trong khối
		List<Student> students = repository.findAllByGrade(grade);
		System.out.println(students.size());
		
		// mảng trả về
		List<StudentDTO7> res = new ArrayList<StudentDTO7>();
		
		// lấy điểm trung bình tat ca hoc sinh
		List<Double> avgMark = examRepository.findSubjectAvgMarkOfStudentInGrade(subjectName, grade);
		System.out.println(avgMark);
		
		// lay id cua tat ca hoc sinh
		List<Integer> studentsId = examRepository.findSubjectStudentAvgMarkOfStudentInGrade(subjectName, grade);
		System.out.println(studentsId);
		
		// lay ranking cua tat ca hoc sinh
		List<Integer> studentsRank = examRepository.findSubjectStudentRankAvgMarkOfStudentInGrade(subjectName, grade);
		System.out.println(studentsRank);
		
		// map id cua hoc sinh voi diem trung binh
		Map<Integer, Double> studentMapAvgMark = new HashMap<Integer, Double>();
		
		// map id cua hoc sinh voi rank cua hoc sinh
		Map<Integer, Integer> studentMapRank = new HashMap<Integer, Integer>();
		
		for(int i = 0 ; i < studentsId.size() ; i ++) {
			studentMapAvgMark.put(studentsId.get(i), avgMark.get(i));
		}
		for(int i = 0 ; i < studentsId.size() ; i ++) {
			studentMapRank.put(studentsId.get(i), studentsRank.get(i));
		}
		System.out.println(studentMapAvgMark);
		System.out.println(studentMapRank);
		for(Student st: students) {
			if(studentMapAvgMark.containsKey(st.getId())) {
				res.add(st.convertToDTO8(studentMapAvgMark.get(st.getId()), st.getScore(), st.getAvatarUrl()));
			}
			else {
				res.add(st.convertToDTO8(0, st.getScore(), st.getAvatarUrl()));
			}
		}
		// sorting điểm giảm dần
		Collections.sort(res, Collections.reverseOrder(new SortByAvgMark()));
		// ranking điểm
		for (int i = 0 ; i < res.size() ; i ++) {
			if(studentMapRank.containsKey(res.get(i).getId())) {
				res.get(i).setRank(studentMapRank.get(res.get(i).getId()));
			}
			else {
				res.get(i).setRank(0);
			}
			
		}

		
		return res;
		
	}


	@Override
	public StudentSubjectInfo getSubjectStudentInfo(int studentId) {
		Student student = repository.findById(studentId);
		
//		StudentScore score = scoreRepository.findByStudent_Id(studentId);
		
		StudentSubjectInfo studentSubjetInfo = new StudentSubjectInfo();
		studentSubjetInfo.setFullName(student.getFullName());
		studentSubjetInfo.setSchool(student.getSchool());
		studentSubjetInfo.setGrade(student.getGrade());
		
		List<ClassroomStudent> listClass = student.getListClass();
		
		List<ClassroomDTO3> listClassDto = new ArrayList<ClassroomDTO3>();
		
		
		Set<SubjectStatus> uniqueSubjectStatus = new HashSet<SubjectStatus>();
		
		for(ClassroomStudent classStudent: listClass) {
			
			Classroom clazz = classStudent.getClassroom();
			
			listClassDto.add(clazz.convertToDTO3());
			
			SubjectStatus subject = new SubjectStatus();
			List<ExamResultDTO3> examResultList = new ArrayList<ExamResultDTO3>();
			subject.setSubjectName(clazz.getSubject());
			double totalMark = 0;
			List<ExamResult> examList = examRepository.findByClassroom_SubjectAndId_StudentId(subject.getSubjectName(), studentId);
			for(ExamResult exam: examList) {
				examResultList.add(exam.convertToDTO3());
				totalMark += exam.getMark();
			}
			double avgMark = (totalMark != 0) ? totalMark/examList.size() : 0;
			subject.setAvgMark(avgMark);
			int rank = getRankOfStudentInSubject(studentId, studentSubjetInfo.getGrade(), subject.getSubjectName());
			subject.setRank(rank);
			subject.setExamList(examResultList);
			uniqueSubjectStatus.add(subject);
			
		}
		
		List<SubjectStatus> subjectStatus = new ArrayList<>(uniqueSubjectStatus);
		
		studentSubjetInfo.setListClass(listClassDto);
		studentSubjetInfo.setSubjectStatus(subjectStatus);
		studentSubjetInfo.setScore(student.getScore());
		studentSubjetInfo.setAvatarUrl(student.getAvatarUrl());
		studentSubjetInfo.setFacebookUrl(student.getFacebookUrl());
		
		
		return studentSubjetInfo;
	}

	@Override
	public void deleteStudent(int studentId) {
		 Student student = repository.findById(studentId);
		 repository.delete(student);
	}

	@Override
	public Integer countNewStudentAtGradeOfSubjectInMonth(int month, int grade, String subject) {
		
		Calendar cal = Calendar.getInstance();
		
		 int year = cal.get(Calendar.YEAR);
		 
	     cal.set(Calendar.DATE, 25);
	     cal.set(Calendar.MONTH, month-1);
	     cal.set(Calendar.YEAR, year);

	     cal.set(Calendar.DAY_OF_MONTH, 1);
	     Date firstDayOfMonth = cal.getTime(); 
	     cal.set(Calendar.DAY_OF_MONTH,cal.getActualMaximum(Calendar.DATE));
	     Date lastDayOfMonth = cal.getTime();
	     
		return repository.countNewStudentAtGradeOfSubjectInMonth(firstDayOfMonth, lastDayOfMonth, grade, subject);
	}

	@Override
	public Integer countNewStudentAtGradeInMonth(int month, int grade, int year) {
		Calendar cal = Calendar.getInstance();
		 
	     cal.set(Calendar.DATE, 25);
	     cal.set(Calendar.MONTH, month-1);
	     cal.set(Calendar.YEAR, year);

	     cal.set(Calendar.DAY_OF_MONTH, 1);
	     Date firstDayOfMonth = cal.getTime(); 
	     cal.set(Calendar.DAY_OF_MONTH,cal.getActualMaximum(Calendar.DATE));
	     Date lastDayOfMonth = cal.getTime();
	     
		return repository.countNewStudentAtGradeInMonth(firstDayOfMonth, lastDayOfMonth, grade);
	}

	@Override
	public Integer countDeactivedStudentAtGradeInMonth(int month, int grade, int year) {
		Calendar cal = Calendar.getInstance();
		
		 
	     cal.set(Calendar.DATE, 25);
	     cal.set(Calendar.MONTH, month-1);
	     cal.set(Calendar.YEAR, year);

	     cal.set(Calendar.DAY_OF_MONTH, 1);
	     Date firstDayOfMonth = cal.getTime(); 
	     cal.set(Calendar.DAY_OF_MONTH,cal.getActualMaximum(Calendar.DATE));
	     Date lastDayOfMonth = cal.getTime();
	     
		return repository.countDeactivedStudentAtGradeInMonth(firstDayOfMonth, lastDayOfMonth, grade);
	}

	@Override
	public Integer countAllStudentAtGradeInMonth(int month, int grade, int year) {
		 Calendar cal = Calendar.getInstance();
		
//		 int year = cal.get(Calendar.YEAR);
		 
	     cal.set(Calendar.DATE, 25);
	     cal.set(Calendar.MONTH, month-1);
	     cal.set(Calendar.YEAR, year);

	     cal.set(Calendar.DAY_OF_MONTH, 1);
	     Date firstDayOfMonth = cal.getTime(); 
	     cal.set(Calendar.DAY_OF_MONTH,cal.getActualMaximum(Calendar.DATE));
	     Date lastDayOfMonth = cal.getTime();
	     
	     
		return repository.countAllStudentAtGradeInMonth(lastDayOfMonth, grade);
	}

	@Override
	public List<Student> getListStudentInGradeWithActiveStatus(int grade, String status) {
		// TODO Auto-generated method stub
		return repository.findByGradeAndStatus(grade, status);
	}



}

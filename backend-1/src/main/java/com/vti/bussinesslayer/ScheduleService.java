package com.vti.bussinesslayer;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vti.datalayer.IScheduleRepository;
import com.vti.entity.Schedule;

@Service
public class ScheduleService implements IScheduleService {
	
	@Autowired
	private IScheduleRepository repository;

	@Override
	public void createSchedule(List<Schedule> schedule, int classId) {
		repository.saveAll(schedule);
	}

	@Override
	public void updateSchedule(int classId, List<Schedule> dto) {
		List<Schedule> listScheduleOfClass = repository.findByClassroom_Id(classId);
		repository.deleteAll(listScheduleOfClass);
		repository.saveAll(dto);
	}

	@Override
	public void deleteSchedule(List<Integer> listId) {
		
	}

}

package com.vti.bussinesslayer;

import java.util.Date;
import java.util.List;

import com.vti.entity.SubAttendance;

public interface ISubAttendanceService {
		
		public List<SubAttendance> getListSubAttendanceInClassToday(int classId, Date dateId);
		
		public void studentAttenCompensate (SubAttendance subAtten);
		
		public void deleteAttenCompensate  (int studentId, int classId, Date dateId);
}

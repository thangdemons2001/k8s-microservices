package com.vti.config.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;



@Configuration
@ConfigurationProperties(prefix = "file")
public class FileProperties {
	
	private String avatar;

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
	
	public FileProperties() {
		// TODO Auto-generated constructor stub
	}

	public FileProperties(String avatar) {
		super();
		this.avatar = avatar;
	}
	
	
}

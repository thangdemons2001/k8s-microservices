package com.vti.datalayer;









import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.vti.entity.HomeWork;


public interface IHomeWorkRepository extends JpaRepository<HomeWork, Integer>, JpaSpecificationExecutor<HomeWork> {
		
	
		public HomeWork findByLesson_Id (int lessonId);
		
		public HomeWork findTop1ByLesson_DateBeforeOrderByLesson_DateDesc(Date date);
		
		
}

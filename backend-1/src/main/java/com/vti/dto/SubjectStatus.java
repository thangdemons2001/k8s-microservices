package com.vti.dto;

import java.util.List;

public class SubjectStatus implements Comparable<SubjectStatus>{
	
	private String subjectName;
	
	private int rank;
	
	private double avgMark;
	
	List<ExamResultDTO3> examList;

	public String getSubjectName() {
		return subjectName;
	}

	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}

	public int getRank() {
		return rank;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}

	public double getAvgMark() {
		return avgMark;
	}

	public void setAvgMark(double avgMark) {
		this.avgMark = avgMark;
	}

	public List<ExamResultDTO3> getExamList() {
		return examList;
	}

	public void setExamList(List<ExamResultDTO3> examList) {
		this.examList = examList;
	}
	
	public SubjectStatus() {
		// TODO Auto-generated constructor stub
	}

	public SubjectStatus(String subjectName, int rank, double avgMark, List<ExamResultDTO3> examList) {
		super();
		this.subjectName = subjectName;
		this.rank = rank;
		this.avgMark = avgMark;
		this.examList = examList;
	}
	
	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return this.subjectName.hashCode();
	}

	@Override
	public int compareTo(SubjectStatus o) {
		// TODO Auto-generated method stub
		return this.getSubjectName().compareTo(o.getSubjectName());
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
	        
	    if (obj == null) {
	    	return false;
	    }
	    if (getClass() != obj.getClass()) {
	    	return false;
	    }
	    
	    SubjectStatus other = (SubjectStatus) obj;
	        
		return this.getSubjectName().equals(other.getSubjectName());
	}
	
}

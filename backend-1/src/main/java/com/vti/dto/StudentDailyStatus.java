package com.vti.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class StudentDailyStatus implements Comparable<StudentDailyStatus> {
	
	
	private String lessonName;
	
	@JsonFormat(pattern = "dd-MM-yyyy")
	private Date lessonDate;
	
	private String attendanceStatus;
	
	private String homeWorkStatus;
	
	private String lessonLink;

	public String getLessonName() {
		return lessonName;
	}

	public void setLessonName(String lessonName) {
		this.lessonName = lessonName;
	}

	public Date getLessonDate() {
		return lessonDate;
	}

	public void setLessonDate(Date lessonDate) {
		this.lessonDate = lessonDate;
	}

	public String getAttendanceStatus() {
		return attendanceStatus;
	}

	public void setAttendanceStatus(String attendanceStatus) {
		this.attendanceStatus = attendanceStatus;
	}

	public String getHomeWorkStatus() {
		return homeWorkStatus;
	}

	public void setHomeWorkStatus(String homeWorkStatus) {
		this.homeWorkStatus = homeWorkStatus;
	}

	public String getLessonLink() {
		return lessonLink;
	}

	public void setLessonLink(String lessonLink) {
		this.lessonLink = lessonLink;
	}
	
	public StudentDailyStatus() {
		// TODO Auto-generated constructor stub
	}

	public StudentDailyStatus(String lessonName, Date lessonDate, String attendanceStatus, String homeWorkStatus,
			String lessonLink) {
		super();
		this.lessonName = lessonName;
		this.lessonDate = lessonDate;
		this.attendanceStatus = attendanceStatus;
		this.homeWorkStatus = homeWorkStatus;
		this.lessonLink = lessonLink;
	}

	@Override
	public int compareTo(StudentDailyStatus o) {
		// TODO Auto-generated method stub
		return getLessonDate().compareTo(o.getLessonDate());
	}
	

}

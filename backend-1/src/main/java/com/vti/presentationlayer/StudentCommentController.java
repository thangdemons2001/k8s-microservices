package com.vti.presentationlayer;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vti.bussinesslayer.IStudentCommentService;
import com.vti.bussinesslayer.IStudentService;
import com.vti.bussinesslayer.IUserService;
import com.vti.dto.UserCommentDTO;
import com.vti.entity.Student;
import com.vti.entity.StudentComment;
import com.vti.entity.User;

@RestController
@RequestMapping(value = "api/v1/comments")
@CrossOrigin("*")
public class StudentCommentController {

	@Autowired
	private IStudentCommentService service;

	@Autowired
	private IStudentService studentService;

	@Autowired
	private IUserService userService;

	@PostMapping("/students/{studentId}/users/{userId}")
	public ResponseEntity<?> commentForStudent(@PathVariable(name = "studentId") int studentId,
			@PathVariable(name = "userId") int userId,@RequestBody UserCommentDTO comment) {

		StudentComment studentNewComment = new StudentComment();
		User user = userService.getUserById(userId);
		Student student = studentService.findStudentById(studentId);
		studentNewComment.setStudent(student);
		studentNewComment.setUser(user);
		studentNewComment.setComment(comment.getComment());
		studentNewComment.setCommentDate(new Date());
		service.createStudentComment(studentNewComment);
		return new ResponseEntity<String>("create successful!", HttpStatus.OK);
	}

}

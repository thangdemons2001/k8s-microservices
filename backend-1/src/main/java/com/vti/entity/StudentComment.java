package com.vti.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.vti.dto.UserCommentDTO;

@Entity
@Table(name = "StudentComment", catalog = "TCTmanagement")
public class StudentComment implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Column(name = "comment_id", nullable = false)
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@ManyToOne
	@JoinColumn(name = "student_id")
	private Student student;
	
	@ManyToOne
	@JoinColumn(name = "user_id")
	private User user;
	
	@Column(name = "comment_text",  nullable = false, length = 8000)
	private String comment;
	
	@Column(name = "comment_date",  nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date commentDate;

	
	
	

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Date getCommentDate() {
		return commentDate;
	}

	public void setCommentDate(Date commentDate) {
		this.commentDate = commentDate;
	}
	
	public StudentComment() {
		// TODO Auto-generated constructor stub
	}
	
	
	

	

	public StudentComment(int id, Student student, User user, String comment, Date commentDate) {
		super();
		this.id = id;
		this.student = student;
		this.user = user;
		this.comment = comment;
		this.commentDate = commentDate;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	

	public UserCommentDTO convertToDTO() {
		User author = this.getUser();
		return new UserCommentDTO(id, author.getFullName(),author.getRole() ,comment, commentDate);
	}
	
	public UserCommentDTO convertToDTO2() {
		User author = this.getUser();
		return new UserCommentDTO(id, author.getFullName(),author.getRole() ,comment, commentDate, author.getAvatarUrl(), author.getFacebookUrl());
	}
	
}

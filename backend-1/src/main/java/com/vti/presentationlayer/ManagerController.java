package com.vti.presentationlayer;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vti.bussinesslayer.IManagerService;
import com.vti.bussinesslayer.ISubjectCostStatusService;
import com.vti.dto.ClassroomDTO;
import com.vti.entity.SubjectCostStatus;


@RestController
@RequestMapping(value = "api/v1/managers")
@CrossOrigin("*")
public class ManagerController {
	
	@Autowired
	private IManagerService service;
	
	@Autowired
	private ISubjectCostStatusService costStatusService;
	
	@PutMapping("/cost-info/{grade}")
	public ResponseEntity<?> resetCostInfo(@PathVariable(name = "grade") int grade, @RequestBody ClassroomDTO subject) {
		System.out.println(subject.getSubjectName());
		service.resetCostStatusOfStatusInSubjectInGrade(subject.getSubjectName(), grade);
		SubjectCostStatus costStatus = costStatusService.getCostStatusOfSubjectInGrade(subject.getSubjectName(), grade);
		costStatus.setLastModify(new Date());
		costStatus.setCostStatus("inactive");
		costStatusService.updateCostStatusOfSubjectInGrade(costStatus);
		return new ResponseEntity<String>("reset successful!", HttpStatus.OK);
	}
	
	@DeleteMapping("/attendances/homeworks")
	public ResponseEntity<?> resetMonth() {
		service.resetMonth();
		return new ResponseEntity<String>("reset successful!", HttpStatus.OK);
	}
}

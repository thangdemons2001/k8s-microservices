package com.vti.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
@Embeddable
public class ClassroomLessonKey implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Column(name = "lesson_id" , nullable = false)
	private int lessonId;
	
	@Column(name = "class_id" , nullable = false)
	private int classroomId;

	public int getLessonId() {
		return lessonId;
	}

	public void setLessonId(int lessonId) {
		this.lessonId = lessonId;
	}

	public int getClassroomId() {
		return classroomId;
	}

	public void setClassroomId(int classroomId) {
		this.classroomId = classroomId;
	}

	public ClassroomLessonKey() {
		// TODO Auto-generated constructor stub
	}

	public ClassroomLessonKey(int lessonId, int classroomId) {
		super();
		this.lessonId = lessonId;
		this.classroomId = classroomId;
	}
	
	
}

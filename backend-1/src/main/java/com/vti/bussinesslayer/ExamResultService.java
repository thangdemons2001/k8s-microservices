package com.vti.bussinesslayer;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.vti.datalayer.IClassroomStudentRepository;
import com.vti.datalayer.IExamResultRepository;
import com.vti.datalayer.IStudentRepository;
import com.vti.dto.ExamResultDTO;
import com.vti.entity.ClassroomStudent;
import com.vti.entity.ExamResult;
import com.vti.entity.ExamResultKey;
import com.vti.entity.Student;




@Service
public class ExamResultService implements IExamResultService{
		
		@Autowired
		private IExamResultRepository repository;
		
		@Autowired
		private IClassroomStudentRepository classroomStudentRepository;


		@Override
		public void createListExamResult(List<ExamResult> results) {
			repository.saveAll(results);
		}


		@Override
		public List<ExamResult> getListStudentExamMarkInClass(int classId, int examId) {
			return repository.findById_ClassroomIdAndId_ExamId(classId, examId);
		}


		@Override
		public List<ExamResult> getListExamInClass(int classId) {
			return repository.findById_ClassroomId(classId);
		}


		@Override
		public List<ExamResult> getListStudentAvgMarkSubjectInGrade(int grade, String subjectName) {
		
			return null;
		}
		
		public Specification<ExamResult> inGrade(int grade){
			return new Specification<ExamResult>() {

				@Override
				public Predicate toPredicate(Root<ExamResult> root, CriteriaQuery<?> query,
						CriteriaBuilder criteriaBuilder) {
	
					return criteriaBuilder.equal(root.join("classroom", JoinType.LEFT).get("grade"),grade);
				}
				
			};
		}
		public Specification<ExamResult> andSubjectEqual(String subjectName){
			return new Specification<ExamResult>() {

				@Override
				public Predicate toPredicate(Root<ExamResult> root, CriteriaQuery<?> query,
						CriteriaBuilder criteriaBuilder) {
		
					return criteriaBuilder.equal(root.join("classroom", JoinType.LEFT).get("subject"),subjectName);
				}
				
			};
		}


		@Override
		public List<ExamResult> getListStudentMarkInExam(int id) {
	
			return repository.findById_ExamId(id);
		}


		@Override
		public List<Student> getListStudentNotTakeSubjectExamInMonthAtGrade(int month, String subject, int grade) {
			
			Calendar cal = Calendar.getInstance();
			
			 int year = cal.get(Calendar.YEAR);
			 
		     cal.set(Calendar.DATE, 25);
		     cal.set(Calendar.MONTH, month-1);
		     cal.set(Calendar.YEAR, year);

		     cal.set(Calendar.DAY_OF_MONTH, 1);
		     Date firstDayOfMonth = cal.getTime(); 
		     cal.set(Calendar.DAY_OF_MONTH,cal.getActualMaximum(Calendar.DATE));
		     Date lastDayOfMonth = cal.getTime();
		     
		     List<ExamResult> query = repository.findByClassroom_SubjectAndClassroom_GradeAndExam_CreatedDateBetween(subject, grade,firstDayOfMonth, lastDayOfMonth);
		     
		     Set<Student> uniqueStudent = new HashSet<Student>();
		     
		     
		     List<ClassroomStudent> studentsInSubject = classroomStudentRepository.findByClassroom_GradeAndClassroom_Subject(grade, subject);
		     
		     Map<Integer, Integer> studentMapId = new HashMap<Integer, Integer>();
		    
		     for(ExamResult st: query) {
		    	int studentIdTemp = st.getStudent().getId();
		    	studentMapId.put(studentIdTemp, studentIdTemp);
		    	
		     }
		     
		     for (ClassroomStudent stInSubject: studentsInSubject) {
		    	 int stId = stInSubject.getStudent().getId();
		    	 if(studentMapId.get(stId) == null) {
		    		 uniqueStudent.add(stInSubject.getStudent());
		    		 
		    	 }
		     }
		     
		     List<Student> res = new ArrayList<Student>(uniqueStudent);
		     
			return res;
		}


		@Override
		public void updateExamResult(ExamResultKey id, double mark) {
			ExamResult result = repository.getById(id);
			result.setMark(mark);
			repository.save(result);
		}


		@Override
		public void updateManyExamResult(List<ExamResultDTO> dtos) {
				List<ExamResult> body = new ArrayList<ExamResult>();
				for(ExamResultDTO result: dtos) {
					ExamResultKey id = new ExamResultKey(result.getStudentId(), result.getClassroomId(), result.getExamId());
					ExamResult examResult = new ExamResult();
					examResult.setId(id);
					examResult.setMark(result.getMark());
					body.add(examResult);
				}
				repository.saveAll(body);
		}
		
}

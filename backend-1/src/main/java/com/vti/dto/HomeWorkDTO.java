package com.vti.dto;



public class HomeWorkDTO {
	
	private int id;
	
	private String name;
	
	private String link;
	
	private String keyLink;
	
	private String misson;
	
	private int quizzId;
	
	private String type;
	
	

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getKeyLink() {
		return keyLink;
	}

	public void setKeyLink(String keyLink) {
		this.keyLink = keyLink;
	}

	public String getMisson() {
		return misson;
	}

	public void setMisson(String misson) {
		this.misson = misson;
	}

	public int getQuizzId() {
		return quizzId;
	}

	public void setQuizzId(int quizzId) {
		this.quizzId = quizzId;
	}
	
	public HomeWorkDTO() {
		// TODO Auto-generated constructor stub
	}

	public HomeWorkDTO(int id, String name, String link, String keyLink, String misson, int quizzId) {
		super();
		this.id = id;
		this.name = name;
		this.link = link;
		this.keyLink = keyLink;
		this.misson = misson;
		this.quizzId = quizzId;
	}

	public HomeWorkDTO(int id, String name, String link, String keyLink, String misson, int quizzId, String type) {
		super();
		this.id = id;
		this.name = name;
		this.link = link;
		this.keyLink = keyLink;
		this.misson = misson;
		this.quizzId = quizzId;
		this.type = type;
	}
	
	
	
}

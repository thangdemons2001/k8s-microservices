package com.vti.presentationlayer;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vti.bussinesslayer.ITeacherService;
import com.vti.bussinesslayer.IUserService;
import com.vti.dto.TeacherDTO;
import com.vti.entity.Teacher;

@RestController
@RequestMapping(value = "api/v1/teachers")
@CrossOrigin("*")
public class TeacherController {
	
	@Autowired
	private ITeacherService service;
	
	@Autowired
	private IUserService userService;
	
	@GetMapping("/subject/")
	public List<TeacherDTO> getListTeacherInSubject(String subjectName){
		
		List<TeacherDTO> res = new ArrayList<TeacherDTO>();
		
		List<Teacher> listTeacher = service.findTeacherBySubjectName(subjectName);
		
		for(Teacher teacher: listTeacher) {
			res.add(teacher.convertToDTO());
		}
		
		return res;
	}
	
	@GetMapping("/")
	public List<TeacherDTO> getAllTeacher(){
		
		List<TeacherDTO> res = new ArrayList<TeacherDTO>();
		
		List<Teacher> listTeacher = service.findAllTeacher();
		
		for(Teacher teacher: listTeacher) {
			res.add(teacher.convertToDTO());
		}
		
		return res;
	}
	
	@DeleteMapping("/{studentId}")
	public ResponseEntity<?> deleteUser(@PathVariable(name = "studentId") int studentId) {
		userService.deleteUser(studentId);
		return new ResponseEntity<String>("delete successful!", HttpStatus.OK);
	}
	
	@PostMapping("/")
	public ResponseEntity<?>  createTeacher(@RequestBody Teacher teacher){
		teacher.setStatus("active");
		teacher.setSocial("https://www.facebook.com/thaychungtoan.edu.vn");
		service.createTeacher(teacher);
		return new ResponseEntity<String>("create successful!", HttpStatus.OK);
	}
	
	@PutMapping("/{teacherId}")
	public ResponseEntity<?>  updateTeacher(@PathVariable(name = "teacherId") int teacherId, @RequestBody Teacher teacherdto){
		
		Teacher teacher = service.findTeacherById(teacherId);
		teacher.setSubjectName(teacherdto.getSubjectName());
		teacher.setFirstName(teacherdto.getFirstName());
		teacher.setLastName(teacherdto.getLastName());
		service.createTeacher(teacher);
		return new ResponseEntity<String>("update successful!", HttpStatus.OK);
	}
	
	@GetMapping("/{teacherId}")
	public ResponseEntity<?>  getTeacherById(@PathVariable(name = "teacherId") int teacherId){
		
		Teacher teacher = service.findTeacherById(teacherId);
		TeacherDTO res = teacher.convertToDTO();
		return new ResponseEntity<TeacherDTO>(res, HttpStatus.OK);
	}
	
}

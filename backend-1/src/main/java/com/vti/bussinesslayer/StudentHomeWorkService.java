package com.vti.bussinesslayer;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vti.datalayer.IStudentHomeWorkRepository;
import com.vti.datalayer.IStudentRepository;
import com.vti.entity.StudentHomeWork;

@Service
public class StudentHomeWorkService implements IStudentHomeWorkService {

	@Autowired
	private IStudentHomeWorkRepository repository;
	
	@Autowired
	private IStudentRepository studentRepository;
	

	@Override
	public void createStudentSubmitHomeWork(StudentHomeWork submit) {
		repository.save(submit);
	}

	@Override
	public List<StudentHomeWork> getAllSubmittedStudentInLesson(int lessonId) {
		// TODO Auto-generated method stub
		return repository.findByLesson_Id(lessonId);
	}

	@Override
	public List<StudentHomeWork> getAllSubmittedHomeWorkOfStudentInMonth(String subject, int month, int grade) {
		Calendar cal = Calendar.getInstance();

		int year = cal.get(Calendar.YEAR);

		cal.set(Calendar.DATE, 25);
		cal.set(Calendar.MONTH, month - 1);
		cal.set(Calendar.YEAR, year);

		cal.set(Calendar.DAY_OF_MONTH, 1);
		Date firstDayOfMonth = cal.getTime();
		cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DATE));
		Date lastDayOfMonth = cal.getTime();

		return repository.findByLesson_Classroom_SubjectAndLesson_Classroom_GradeAndSubmitDateBetween(subject, grade,
				firstDayOfMonth, lastDayOfMonth);
	}

	@Override
	public List<StudentHomeWork> getAllSubmittedHomeWorkOfStudentInThreeMonths(int studentId) {
		Calendar cal = Calendar.getInstance();
		
		int month = cal.get(Calendar.MONTH) ;
		
		cal.set(Calendar.MONTH, month -  3);
		Date lastThreeMonths = cal.getTime();
		
		return repository.findByStudent_IdAndSubmitDateAfter(studentId, lastThreeMonths);
	}

	@Override
	public void deleteStudentSubmitHomeWorkInLesson(int studentId, int lessonId) {
		StudentHomeWork submit = repository.findByLesson_IdAndStudent_Id(lessonId, studentId);
		repository.delete(submit);
	}


	@Override
	public void deleteMultiStudentSubmitHomeWorkInLesson(int lessonId, List<Integer> studentIds) {
		// xóa đi các submit của học sinh
		repository.deleteStudentSubmitHomeWorkInListId(lessonId, studentIds);
		// giảm điểm học sinh xuống 10 điểm
		studentRepository.decreaseScoreForMultiHomeWorkSubmitDeletion(studentIds);
	}

	@Override
	public void createMultiStudentSubmitHomeWork(List<StudentHomeWork> listSubmision, List<Integer> studentIds) {
		// save các submission xuống db
		repository.saveAll(listSubmision);
		// tăng điểm lên 10 cho mỗi học sinh submit
		studentRepository.increaseScoreForMultiHomeWorkSubmissions(studentIds);
	}

}

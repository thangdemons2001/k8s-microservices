package com.vti.bussinesslayer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vti.datalayer.IHomeWorkRepository;
import com.vti.entity.HomeWork;
@Service
public class HomeWorkService implements IHomeWorkService {
	
	@Autowired
	private IHomeWorkRepository repository;

	@Override
	public void createHomeWork(HomeWork homework) {
			repository.save(homework);
	}

	@Override
	public HomeWork getLessonHomeWorkByLessonId(int id) {
		return repository.findByLesson_Id(id);
	}

	@Override
	public HomeWork getHomeWorkById(int id) {
		return repository.getById(id);
	}

}

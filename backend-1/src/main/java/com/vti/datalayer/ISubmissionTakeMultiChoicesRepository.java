package com.vti.datalayer;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.vti.entity.SubmissionTakeMultiChoices;

public interface ISubmissionTakeMultiChoicesRepository
		extends JpaRepository<SubmissionTakeMultiChoices, Long>, JpaSpecificationExecutor<SubmissionTakeMultiChoices> {

}

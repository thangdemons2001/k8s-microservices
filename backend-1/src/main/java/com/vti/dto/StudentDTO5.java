package com.vti.dto;




// only use for attendance 
public class StudentDTO5 {
	
	private int id;
		
	private String firstName;
	
	private String lastName;
		
	private String fullName;
	
	private String status;
	
	private String school;
	
	private double mark;
	
	private String studentNumber;
	
	private String parentNumber;
	
	private String parentName;
	
	private String testingSystemId;
	
	private String avatarUrl;
	
	private String facebookUrl;
	
	

	public String getAvatarUrl() {
		return avatarUrl;
	}

	public void setAvatarUrl(String avatarUrl) {
		this.avatarUrl = avatarUrl;
	}

	public String getFacebookUrl() {
		return facebookUrl;
	}

	public void setFacebookUrl(String facebookUrl) {
		this.facebookUrl = facebookUrl;
	}
	
	
	
	public StudentDTO5(int id, String firstName, String lastName, String fullName, String status, String school,
			double mark, String studentNumber, String parentNumber, String parentName, String testingSystemId,
			String avatarUrl, String facebookUrl) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.fullName = fullName;
		this.status = status;
		this.school = school;
		this.mark = mark;
		this.studentNumber = studentNumber;
		this.parentNumber = parentNumber;
		this.parentName = parentName;
		this.testingSystemId = testingSystemId;
		this.avatarUrl = avatarUrl;
		this.facebookUrl = facebookUrl;
	}

	public StudentDTO5(int id, String firstName, String lastName, String fullName, String status, String school,
			double mark, String studentNumber, String parentNumber, String parentName, String testingSystemId) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.fullName = fullName;
		this.status = status;
		this.school = school;
		this.mark = mark;
		this.studentNumber = studentNumber;
		this.parentNumber = parentNumber;
		this.parentName = parentName;
		this.testingSystemId = testingSystemId;
	}

	public String getTestingSystemId() {
		return testingSystemId;
	}

	public void setTestingSystemId(String testingSystemId) {
		this.testingSystemId = testingSystemId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSchool() {
		return school;
	}

	public void setSchool(String school) {
		this.school = school;
	}

	public double getMark() {
		return mark;
	}

	public void setMark(double mark) {
		this.mark = mark;
	}

	public String getStudentNumber() {
		return studentNumber;
	}

	public void setStudentNumber(String studentNumber) {
		this.studentNumber = studentNumber;
	}

	public String getParentNumber() {
		return parentNumber;
	}

	public void setParentNumber(String parentNumber) {
		this.parentNumber = parentNumber;
	}

	public String getParentName() {
		return parentName;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	}
	
	public StudentDTO5() {
		// TODO Auto-generated constructor stub
	}

	public StudentDTO5(int id, String firstName, String lastName, String fullName, String status, String school,
			double mark, String studentNumber, String parentNumber, String parentName) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.fullName = fullName;
		this.status = status;
		this.school = school;
		this.mark = mark;
		this.studentNumber = studentNumber;
		this.parentNumber = parentNumber;
		this.parentName = parentName;
	}
	
	
	
	
}

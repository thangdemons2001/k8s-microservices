package com.vti.bussinesslayer;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vti.datalayer.IDeactivedStudentRepository;
import com.vti.datalayer.IReasonLeftRepository;
import com.vti.entity.DeactivedStudent;
import com.vti.entity.ReasonLeft;

@Service
public class ReasonLeftService implements IReasonLeftService {
	
	@Autowired
	private IReasonLeftRepository repository;
	
	@Autowired
	private IDeactivedStudentRepository deactivedRepository;

	@Override
	public void createReasonLeft(List<ReasonLeft> dto, int deactivedStudentId) {
			DeactivedStudent student = deactivedRepository.findById(deactivedStudentId);
			for(ReasonLeft reason: dto) {
				reason.setDeactivedStudent(student);
			}
			repository.saveAll(dto);
		
	}

	@Override
	public void updateDeactivedStudentReasonLeft(int deactivedStudentId, List<ReasonLeft> dto) {
			DeactivedStudent student = deactivedRepository.findById(deactivedStudentId);
			List<ReasonLeft> reasons = repository.findByDeactivedStudent_Id(deactivedStudentId);
			repository.deleteAll(reasons);
			for(ReasonLeft reason: dto) {
				reason.setDeactivedStudent(student);
			}
			repository.saveAll(dto);
	}

}

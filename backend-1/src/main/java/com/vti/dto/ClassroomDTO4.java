package com.vti.dto;

import java.util.Date;
import java.util.List;

import com.vti.entity.Classroom;




public class ClassroomDTO4 {
	
	private int id;
	
	private String className;
	
	private String subjectName;
	
	private int grade;
	
	private TeacherDTO teacher;
	
	private List<ScheduleDTO> listSchedule;
	
	private List<MentorDTO> listMentor;
	
	public List<MentorDTO> getListMentor() {
		return listMentor;
	}

	public void setListMentor(List<MentorDTO> listMentor) {
		this.listMentor = listMentor;
	}	

	public TeacherDTO getTeacher() {
		return teacher;
	}

	public void setTeacher(TeacherDTO teacher) {
		this.teacher = teacher;
	}

	public List<ScheduleDTO> getListSchedule() {
		return listSchedule;
	}

	public void setListSchedule(List<ScheduleDTO> listSchedule) {
		this.listSchedule = listSchedule;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public int getGrade() {
		return grade;
	}

	public void setGrade(int grade) {
		this.grade = grade;
	}

	public TeacherDTO getTeacherId() {
		return teacher;
	}

	public void setTeacherId(TeacherDTO teacherId) {
		this.teacher = teacherId;
	}

	

	public String getSubjectName() {
		return subjectName;
	}

	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}

	public ClassroomDTO4(int id, String className, int grade, TeacherDTO teacherId, List<ScheduleDTO> listSchedule) {
		super();
		this.id = id;
		this.className = className;
		this.grade = grade;
		this.teacher = teacherId;
		this.listSchedule = listSchedule;
	}
	
	public ClassroomDTO4() {
		// TODO Auto-generated constructor stub
	}

	public ClassroomDTO4(int id, String className, String subjectName, int grade, TeacherDTO teacher, List<ScheduleDTO> listSchedule) {
		super();
		this.id = id;
		this.className = className;
		this.subjectName = subjectName;
		this.grade = grade;
		this.teacher = teacher;
		this.listSchedule = listSchedule;
		
	}
	
	
	public ClassroomDTO4(int id, String className, String subjectName, int grade, TeacherDTO teacher, List<ScheduleDTO> listSchedule, List<MentorDTO> listMentor) {
		super();
		this.id = id;
		this.className = className;
		this.subjectName = subjectName;
		this.grade = grade;
		this.teacher = teacher;
		this.listSchedule = listSchedule;
		this.listMentor = listMentor;
	}

	
}

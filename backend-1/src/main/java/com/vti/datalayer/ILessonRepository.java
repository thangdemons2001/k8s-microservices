package com.vti.datalayer;







import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;


import com.vti.entity.Lesson;


public interface ILessonRepository extends JpaRepository<Lesson, Integer>, JpaSpecificationExecutor<Lesson> {
		
	
	public Lesson findById (int id);
	
	public List<Lesson> findByIdIn(List<Integer> ids);
	
	@Query("FROM Lesson l LEFT JOIN l.classList c WHERE c.id.classroomId = ?1 ORDER BY l.date DESC")
	public List<Lesson> findByClassroom_IdOrderByDateDesc (int id);
	
	@Query("FROM Lesson l LEFT JOIN l.classList c WHERE c.id.classroomId = ?1 AND l.date < ?2 ORDER BY l.date DESC")
	public List<Lesson> findTop1ByClassroom_IdAndDateBeforeOrderByDateDesc (int id, Date date, Pageable pageable);
	
	@Query("FROM Lesson l LEFT JOIN l.classList c WHERE c.id.classroomId = ?1 AND l.date = ?2")
	public Lesson findByClassroom_IdAndDate (int id, Date date);
	
	@Query("FROM Lesson l LEFT JOIN l.classList c WHERE c.id.classroomId = ?1 AND l.chapter.id = ?2 ORDER BY l.date ASC")
	public List<Lesson> findByClassroom_IdAndChapter_IdOrderByDateAsc(int classId, int chapter_id);
	
	@Query("SELECT DISTINCT l FROM Lesson l JOIN l.classList c WHERE c.classroom.subject = ?1 AND c.classroom.grade = ?2 ORDER BY l.date DESC")
	public List<Lesson> findByClassroom_SubjectAndGradeOrderByDateDesc (String subject, int grade, Pageable pageable);
	
	@Query("SELECT DISTINCT l FROM Lesson l JOIN l.classList c WHERE c.id.classroomId IN ?1")
	public List<Lesson> findByListCLassId (List<Integer> listClassId);
	
	@Query("SELECT DISTINCT l FROM Lesson l JOIN l.classList c WHERE c.classroom.subject = ?1 AND c.classroom.grade = ?2 AND LOWER(l.lessonName) LIKE LOWER(CONCAT('%', ?3, '%')) ORDER BY l.date DESC")
	public List<Lesson> findByLessonNameLikeOrderByDate (String subject, int grade,String searchName, Pageable pageable);
	
	@Query("SELECT DISTINCT l FROM Lesson l JOIN l.classList c WHERE c.classroom.subject = ?1 AND c.classroom.grade = ?2 AND l.id = ?3 ORDER BY l.date DESC")
	public List<Lesson> findByIdOrderByDate (String subject, int grade, int id, Pageable pageable);
}

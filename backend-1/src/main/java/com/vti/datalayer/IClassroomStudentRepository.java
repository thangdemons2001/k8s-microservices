package com.vti.datalayer;



import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.vti.entity.ClassroomStudent;
import com.vti.entity.ClassroomStudentKey;

public interface IClassroomStudentRepository extends JpaRepository<ClassroomStudent, ClassroomStudentKey>, JpaSpecificationExecutor<ClassroomStudent> {
		
	
		public List<ClassroomStudent> findByStudent_Id(int id);
		
		public List<ClassroomStudent> findById_StudentIdAndStatus(int id, String status);
		
		public List<ClassroomStudent> findByClassroom_Id(int classId);
		
		public List<ClassroomStudent> findByClassroom_GradeAndClassroom_SubjectAndSubStatus(int grade, String subject, String status);
		
		public List<ClassroomStudent> findByClassroom_GradeAndClassroom_Subject(int grade, String subject);
		
		public int countByStatusAndId_StudentIdAndClassroom_Subject (String status, int studentId, String subject);
		
		@Transactional
		@Modifying
		@Query(value =
		 " UPDATE ClassroomStudent s "
		+ " LEFT JOIN Class c on s.class_id = c.class_id "
		+ " SET s.status_boolean_sub = ?3 , s.status_boolean = ?4 "
		+ " WHERE c.subject_name = ?1 AND s.student_id = ?2 "
		, nativeQuery = true)
		public void updateStudentSubjectClassStatus (String subject, int studentId, String status, String mainStatus);
		
		@Transactional
		@Modifying
		@Query(value =
					 " UPDATE ClassroomStudent s "
					+ " LEFT JOIN Class c on s.class_id = c.class_id "
					+ " SET s.status_boolean_sub = ?3 "
					+ " WHERE c.subject_name = ?1 AND c.grade = ?2 "
					, nativeQuery = true)
		public void resetAllCostInfoOfSubjectInGrade (String subject, int grade, String status);
		
		@Transactional
		@Modifying
		@Query(value =
					 " UPDATE ClassroomStudent s "
					+ " LEFT JOIN Class c on s.class_id = c.class_id "
					+ " SET s.status_boolean = s.status_boolean_sub "
					+ " WHERE c.subject_name = ?1 AND c.grade = ?2 "
					, nativeQuery = true)
		public void enableSubCostStatusForAllStudentInGradeOfSubject (String subject, int grade);
		
		
		
}

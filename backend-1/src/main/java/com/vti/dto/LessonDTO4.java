package com.vti.dto;

import java.util.Date;



import com.fasterxml.jackson.annotation.JsonFormat;
import com.vti.entity.Chapter;
import com.vti.entity.Video;

public class LessonDTO4 {
	
	private int id;
	
	
	private String lessonName;
	
	@JsonFormat(pattern = "dd-MM-yyyy")
	private Date date;

	private TeacherDTO teacher;

	private Video video;
	
	private Chapter chapter;
	

	public TeacherDTO getTeacher() {
		return teacher;
	}

	public void setTeacher(TeacherDTO teacher) {
		this.teacher = teacher;
	}

	public LessonDTO4(int id, String lessonName, Date date, Video video, Chapter chapter) {
		super();
		this.id = id;
		this.lessonName = lessonName;
		this.date = date;
		this.video = video;
		this.chapter = chapter;
	
	}

	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLessonName() {
		return lessonName;
	}

	public void setLessonName(String lessonName) {
		this.lessonName = lessonName;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Video getVideo() {
		return video;
	}

	public void setVideo(Video video) {
		this.video = video;
	}

	public Chapter getChapter() {
		return chapter;
	}

	public void setChapter(Chapter chapter) {
		this.chapter = chapter;
	}
	
	public LessonDTO4() {
		// TODO Auto-generated constructor stub
	}

	public LessonDTO4(int id, String lessonName, Date date, TeacherDTO teacher, Video video, Chapter chapter) {
		super();
		this.id = id;
		this.lessonName = lessonName;
		this.date = date;
		this.teacher = teacher;
		this.video = video;
		this.chapter = chapter;
	}


	
	
	
}

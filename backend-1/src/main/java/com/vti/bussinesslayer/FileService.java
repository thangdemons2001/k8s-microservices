package com.vti.bussinesslayer;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.vti.config.properties.FileProperties;
import com.vti.datalayer.IUserRepository;
import com.vti.utils.FileManager;

@Service
public class FileService implements IFileService{

	private FileManager fileManager = new FileManager();
	
//	@Value("${file.avatar}")
//	private String linkFolder = "C:\\Users\\ducta\\OneDrive\\Máy tính\\Avatar";
	
	@Autowired
	private FileProperties fileProperties;
	
	@Autowired
	private IUserRepository userRepository;

	@Override
	public String uploadImage(MultipartFile image, int userId) throws IOException {

		String nameImage = new Date().getTime() + "." + fileManager.getFormatFile(image.getOriginalFilename());

		String path = fileProperties.getAvatar() + "//" + nameImage;

		fileManager.createNewMultiPartFile(path, image);

		// TODO save link file to database
		userRepository.uploadUserAvatar(nameImage, userId);
		

		// return link uploaded file
		return nameImage;
	}

	@Override
	public File dowwnloadImage(String nameImage) throws IOException {

		String path = fileProperties.getAvatar() + "//" + nameImage;

		return new File(path);
	}

}

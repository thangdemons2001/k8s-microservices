package com.vti.presentationlayer;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import com.vti.bussinesslayer.IReasonLeftService;

import com.vti.entity.ReasonLeft;



@RestController
@RequestMapping(value = "api/v1/reasonlefts")
@CrossOrigin("*")
public class ReasonLeftController {
	
	
	@Autowired
	private IReasonLeftService service;
	
	
	@PostMapping(value = "/deactived/students/{studentId}")
	public ResponseEntity<?> createReasonLeft(@RequestBody List<ReasonLeft> dto, @PathVariable(name = "studentId") int deactivedStudentId) {
		// get entity
		service.createReasonLeft(dto, deactivedStudentId);
		// return result
		return new ResponseEntity<String>("create successful!", HttpStatus.OK);
	}


		

}

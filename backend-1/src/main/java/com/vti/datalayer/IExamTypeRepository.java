package com.vti.datalayer;





import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;


import com.vti.entity.ExamType;


public interface IExamTypeRepository extends JpaRepository<ExamType, Integer>, JpaSpecificationExecutor<ExamType> {
		
	
		
}

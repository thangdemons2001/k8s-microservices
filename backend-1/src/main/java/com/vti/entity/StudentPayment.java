package com.vti.entity;

import java.io.Serializable;
import java.util.Date;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;



import com.fasterxml.jackson.annotation.JsonFormat;
@Entity
@Table(name = "StudentPayment", catalog = "TCTmanagement")
public class StudentPayment implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Column(name = "payment_id", nullable = false)
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@ManyToOne
	@JoinColumn(name = "student_id")
	private Student student;
	
	@ManyToOne
	@JoinColumn(name = "creator_id")
	private User creator;
	
	@ManyToOne
	@JoinColumn(name = "last_modifer")
	private User modifer;
	
	@Column(name = "modify_date",  nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date modifyDate;
	
	@Column(name = "subject_name", length = 50, nullable = false )
	private String subjectName;
	
	@Column(name = "datetime_payment",  nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date timePayment;
	
	@Column(name = "date_payment", nullable = false)
	@Temporal(TemporalType.DATE)
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date datePayment;
	
	@Column(name = "total_paid", nullable = false)
	private double totalPaid;
	
	@Column(name = "money_type", nullable = false)
	private String status;
	
	@Column(name = "note",  length = 8000, nullable = false)
	private String role;
	
	

	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public Student getStudent() {
		return student;
	}


	public void setStudent(Student student) {
		this.student = student;
	}


	public User getCreator() {
		return creator;
	}


	public void setCreator(User creator) {
		this.creator = creator;
	}


	public User getModifer() {
		return modifer;
	}


	public void setModifer(User modifer) {
		this.modifer = modifer;
	}


	public Date getModifyDate() {
		return modifyDate;
	}


	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}


	public String getSubjectName() {
		return subjectName;
	}


	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}


	public Date getTimePayment() {
		return timePayment;
	}


	public void setTimePayment(Date timePayment) {
		this.timePayment = timePayment;
	}


	public Date getDatePayment() {
		return datePayment;
	}


	public void setDatePayment(Date datePayment) {
		this.datePayment = datePayment;
	}


	public double getTotalPaid() {
		return totalPaid;
	}


	public void setTotalPaid(double totalPaid) {
		this.totalPaid = totalPaid;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public String getRole() {
		return role;
	}


	public void setRole(String role) {
		this.role = role;
	}


	public StudentPayment(long id, Student student, User creator, User modifer, Date modifyDate, String subjectName,
			Date timePayment, Date datePayment, double totalPaid, String status, String role) {
		super();
		this.id = id;
		this.student = student;
		this.creator = creator;
		this.modifer = modifer;
		this.modifyDate = modifyDate;
		this.subjectName = subjectName;
		this.timePayment = timePayment;
		this.datePayment = datePayment;
		this.totalPaid = totalPaid;
		this.status = status;
		this.role = role;
	}
	

	public StudentPayment() {
		// TODO Auto-generated constructor stub
	}
	
	
	
	
}

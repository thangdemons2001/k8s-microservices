package com.vti.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.vti.dto.ClassroomDTO;
import com.vti.dto.ReasonLeftDTO;
import com.vti.dto.StudentDTO;
import com.vti.dto.StudentDTO10;
import com.vti.dto.StudentDTO2;
import com.vti.dto.StudentDTO3;
import com.vti.dto.StudentDTO4;
import com.vti.dto.StudentDTO5;
import com.vti.dto.StudentDTO6;
import com.vti.dto.StudentDTO7;
import com.vti.dto.StudentDTO9;
import com.vti.dto.StudentSubjectInfo;

@Entity
@Table(name = "ReasonLeft", catalog = "TCTmanagement")
public class ReasonLeft implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Column(name = "reason_id", nullable = false)
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name = "reason_left", length = 8000, nullable = false)
	private String reasonLeft;

	@Column(name = "department_name", length = 800, nullable = false)
	private String departmentName;

	@ManyToOne
	@JoinColumn(name = "student_id")
	private DeactivedStudent deactivedStudent;
	
	public ReasonLeft() {
		// TODO Auto-generated constructor stub
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getReasonLeft() {
		return reasonLeft;
	}

	public void setReasonLeft(String reasonLeft) {
		this.reasonLeft = reasonLeft;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	public DeactivedStudent getDeactivedStudent() {
		return deactivedStudent;
	}

	public void setDeactivedStudent(DeactivedStudent deactivedStudent) {
		this.deactivedStudent = deactivedStudent;
	}

	public ReasonLeft(int id, String userName, String password, String firstName, String lastName, Date createdDate,
			String social, String fullName, String status, String role, int id2, String reasonLeft,
			String departmentName, DeactivedStudent deactivedStudent) {
		
		id = id2;
		this.reasonLeft = reasonLeft;
		this.departmentName = departmentName;
		this.deactivedStudent = deactivedStudent;
	}
	
	
	public ReasonLeftDTO convertToDTO() {
		return new ReasonLeftDTO(id, reasonLeft, departmentName);
	}
	
	
	
}

package com.vti.presentationlayer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vti.bussinesslayer.IChapterService;
import com.vti.bussinesslayer.IClassroomStudentService;
import com.vti.bussinesslayer.ILessonService;
import com.vti.bussinesslayer.IStudentCommentService;
import com.vti.bussinesslayer.IStudentHomeWorkService;
import com.vti.bussinesslayer.IStudentService;
import com.vti.dto.ClassroomDTO5;
import com.vti.dto.ClassroomDTO6;
import com.vti.dto.LessonDTO3;
import com.vti.dto.StudentDailyStatus;
import com.vti.dto.StudentHomeWorkDTO;
import com.vti.dto.StudentSubjectInfo;
import com.vti.dto.UserCommentDTO;
import com.vti.entity.Attendance;
import com.vti.entity.Chapter;
import com.vti.entity.Classroom;
import com.vti.entity.ClassroomStudent;
import com.vti.entity.Lesson;
import com.vti.entity.Student;
import com.vti.entity.StudentComment;
import com.vti.entity.StudentHomeWork;


@RestController
@RequestMapping(value = "api/v1/clients")
@CrossOrigin("*")
public class ClientController {

	@Autowired
	private IStudentService service;

	@Autowired
	private IClassroomStudentService classService;
	
	@Autowired
	private IStudentCommentService studentCommentService;

	@Autowired
	private ILessonService lessonService;

	@Autowired
	private IChapterService chapterService;

	@Autowired
	private IStudentHomeWorkService submitHomeWorkSerivce;
	
	@GetMapping("/{studentId}")
	public StudentSubjectInfo getStudentSubjectInfo(@PathVariable(name = "studentId") int studentId) {

		return service.getSubjectStudentInfo(studentId);
	}

	@GetMapping("/{studentId}/classes")
	public List<ClassroomDTO5> getAllClassOfStudent(@PathVariable(name = "studentId") int studentId) {
		
		List<Classroom> listClass = classService.findStudentClass(studentId);
		List<ClassroomDTO5> res = new ArrayList<ClassroomDTO5>();

		for (Classroom clazz : listClass) {
			res.add(clazz.convertToDTO5());
		}

		return res;
	}
	
	
	@GetMapping("/comments/students/{studentId}")
	public ResponseEntity<?> getTopTenOfStudentComment(@PathVariable(name = "studentId") int studentId) {
		
		List<UserCommentDTO> res = new ArrayList<UserCommentDTO>();
		
		List<StudentComment> topTenComment = studentCommentService.getLastTenStudentCommentOfStudent(studentId);
		
		for(StudentComment comment: topTenComment) {
			
			res.add(comment.convertToDTO2());
		}
		
		
		return new ResponseEntity<List<UserCommentDTO>>(res, HttpStatus.OK);
	}
	
	@GetMapping("/grade/{grade}/chapters")
	public List<Chapter> getAllSubjectChapterInGrade(@PathVariable(name = "grade") int grade, String subject) {

		return chapterService.getAllChapterSubjectInGrade(subject, grade);
	}

	@GetMapping("/{studentId}/homeworks/")
	public List<StudentHomeWorkDTO> getAllHomeWorkOfStudent(@PathVariable(name = "studentId") int studentId) {

		List<Classroom> listClass = classService.findStudentClass(studentId);
		List<ClassroomDTO6> classes = new ArrayList<ClassroomDTO6>();

		for (Classroom clazz : listClass) {
			classes.add(clazz.convertToDTO6());
		}

		List<StudentHomeWork> submitInLastThreeMonths = submitHomeWorkSerivce
				.getAllSubmittedHomeWorkOfStudentInThreeMonths(studentId);

		Map<Integer, Integer> mapSubmit = new HashMap<Integer, Integer>();

		for (StudentHomeWork submit : submitInLastThreeMonths) {
			mapSubmit.put(submit.getLesson().getId(), submit.getStudent().getId());
		}

		List<StudentHomeWorkDTO> res = new ArrayList<StudentHomeWorkDTO>();
		for (ClassroomDTO6 clazz : classes) {
			for (Lesson lesson : clazz.getListLesson()) {
				if (lesson.getHomework() == null) {
					continue;
				}

				if (mapSubmit.containsKey(lesson.getId())) {
					res.add(lesson.getHomework().convertToDTO2("P", lesson.getDate()));
				} else {
					res.add(lesson.getHomework().convertToDTO2("A", lesson.getDate()));
				}
			}
		}

		return res;
	}

	@GetMapping("/classes/{classId}/lessons/chapters/{chapterId}")
	public List<LessonDTO3> getAllLessonOfClassInChapter(@PathVariable(name = "classId") int classId,
			@PathVariable(name = "chapterId") int chapterId) {
		List<Lesson> listLesson = lessonService.getAllLessonOfChapterInClass(classId, chapterId);
		List<LessonDTO3> res = new ArrayList<LessonDTO3>();
		for (Lesson lesson : listLesson) {
			res.add(lesson.convertToDTO());
		}

		return res;
	}

	@GetMapping("/{studentId}/daily/status")
	public List<StudentDailyStatus> getAllStudentDailyStatus(@PathVariable(name = "studentId") int studentId) {

		List<ClassroomStudent> listClassOfStudent = classService.findStudentClassroom(studentId);
		
		List<Integer> listClassId = new ArrayList<Integer>();
		for (ClassroomStudent clazz : listClassOfStudent) {
			listClassId.add(clazz.getId().getClassroomId());
		}

		List<StudentHomeWork> submitInLastThreeMonths = submitHomeWorkSerivce
				.getAllSubmittedHomeWorkOfStudentInThreeMonths(studentId);

		Student student = service.findStudentById(studentId);

		List<Attendance> studentAttendance = student.getListAttendance();

		Map<Date, String> mapStudentAttendanceStatusDate = new HashMap<Date, String>();

		Map<Integer, Integer> mapSubmit = new HashMap<Integer, Integer>();

		for (StudentHomeWork submit : submitInLastThreeMonths) {
			mapSubmit.put(submit.getLesson().getId(), submit.getStudent().getId());
		}

		for (Attendance atten : studentAttendance) {
			mapStudentAttendanceStatusDate.put(atten.getId().getDateId(), atten.getStatus());
		}

		List<StudentDailyStatus> res = new ArrayList<StudentDailyStatus>();
		
		List<Lesson> listLessonInAllStudentClasses = lessonService.getAllLessonInAllClassByClassIds(listClassId);
		
			for (Lesson lesson : listLessonInAllStudentClasses) {
				String homeworkStauts = " ";
				String attendanceStatus = "";
				if (lesson.getHomework() == null) {
					homeworkStauts = "none";
				} else {
					if (mapSubmit.containsKey(lesson.getId())) {
						homeworkStauts = "P";
					} else {
						homeworkStauts = "A";
					}
				}
				String videoLink = "none";
				if (lesson.getVideo() != null) {
					videoLink = lesson.getVideo().getLink();
				}
				if (mapStudentAttendanceStatusDate.containsKey(lesson.getDate())) {
					attendanceStatus = mapStudentAttendanceStatusDate.get(lesson.getDate());
				} else {
					attendanceStatus = "A";
				}
				res.add(new StudentDailyStatus(lesson.getLessonName(), lesson.getDate(), attendanceStatus,
						homeworkStauts, videoLink));
			}
		
		
		Collections.sort(res);
		

		return res;
	}

}

package com.vti.presentationlayer;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vti.bussinesslayer.IMentorService;
import com.vti.bussinesslayer.IUserService;
import com.vti.dto.MentorDTO;
import com.vti.entity.Mentor;

@RestController
@RequestMapping(value = "api/v1/mentors")
@CrossOrigin("*")
public class MentorController {
	
	@Autowired
	private IMentorService service;
	
	@Autowired
	private IUserService userService;
	
	@GetMapping("/")
	public List<MentorDTO> getAllMentor(){
		List<Mentor> list = service.getListMentor();
		
		List<MentorDTO> res = new ArrayList<MentorDTO>();
		
		for(Mentor mentor: list) {
			res.add(mentor.convertToDTO());
		}
		return res;
	}
	
	@PostMapping("/")
	public ResponseEntity<?> createMentor(@RequestBody Mentor mentor){
		service.createMentor(mentor);
		return new ResponseEntity<String>("create successful!", HttpStatus.OK);
	}
	
	
	@DeleteMapping("/{studentId}")
	public ResponseEntity<?> deleteUser(@PathVariable(name = "studentId") int studentId) {
		userService.deleteUser(studentId);
		return new ResponseEntity<String>("delete successful!", HttpStatus.OK);
	}
	
	@PutMapping("/{mentorId}")
	public ResponseEntity<?> updateMentor(@RequestBody Mentor mentordto, @PathVariable(name = "mentorId") int mentorId){
		Mentor mentor = service.getMentorById(mentorId);
		mentor.setFirstName(mentordto.getFirstName());
		mentor.setLastName(mentordto.getLastName());
		mentor.setSchool(mentordto.getSchool());
		if(mentordto.getUserName() != null) {
			mentor.setUserName(mentordto.getUserName());
		}
		if(mentordto.getPassword() != null) {
			mentor.setPassword(mentordto.getPassword());
		}
		
		service.createMentor(mentor);
		return new ResponseEntity<String>("update successful!", HttpStatus.OK);
	}
	
	@GetMapping("/{mentorId}")
	public ResponseEntity<?> getMentorById(@PathVariable(name = "mentorId") int mentorId){
		Mentor mentor = service.getMentorById(mentorId);
		MentorDTO res = mentor.convertToDTO();
		return new ResponseEntity<MentorDTO>(res, HttpStatus.OK);
	}
}

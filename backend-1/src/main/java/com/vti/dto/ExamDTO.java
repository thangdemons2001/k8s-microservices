package com.vti.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class ExamDTO  {
	
	private int examId;
	
	private String examName;
	
	@JsonFormat(pattern = "dd-MM-yyyy")
	private Date createdDate;

	private String type;
	
	private int typeId;
	
	private String testingSystemId;
	
	

	public ExamDTO(int examId, String examName, Date createdDate, String type, int typeId, String testingSystemId) {
		super();
		this.examId = examId;
		this.examName = examName;
		this.createdDate = createdDate;
		this.type = type;
		this.typeId = typeId;
		this.testingSystemId = testingSystemId;
	}

	public String getTestingSystemId() {
		return testingSystemId;
	}

	public void setTestingSystemId(String testingSystemId) {
		this.testingSystemId = testingSystemId;
	}

	public int getTypeId() {
		return typeId;
	}

	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}

	public int getExamId() {
		return examId;
	}

	public void setExamId(int examId) {
		this.examId = examId;
	}

	public String getExamName() {
		return examName;
	}

	public void setExamName(String examName) {
		this.examName = examName;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	public ExamDTO() {
		// TODO Auto-generated constructor stub
	}

	public ExamDTO(int examId, String examName, Date createdDate, String type) {
		super();
		this.examId = examId;
		this.examName = examName;
		this.createdDate = createdDate;
		this.type = type;
	}

	public ExamDTO(int examId, String examName, Date createdDate, String type, int typeId) {
		super();
		this.examId = examId;
		this.examName = examName;
		this.createdDate = createdDate;
		this.type = type;
		this.typeId = typeId;
	}
	
	
	
}

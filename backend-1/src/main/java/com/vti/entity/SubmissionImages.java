package com.vti.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "SubmissionImages", catalog = "TCTmanagement")
public class SubmissionImages implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Column(name = "take_id", nullable = false)
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "submission_id")
	private StudentHomeWork submissionId;
	
	@Column(name = "image_link",  nullable = false, length = 8000)
	private String imageLink;

	public StudentHomeWork getSubmissionId() {
		return submissionId;
	}

	public void setSubmissionId(StudentHomeWork submissionId) {
		this.submissionId = submissionId;
	}

	public String getImageLink() {
		return imageLink;
	}

	public void setImageLink(String imageLink) {
		this.imageLink = imageLink;
	}
	
	
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public SubmissionImages() {
		// TODO Auto-generated constructor stub
	}

	public SubmissionImages(StudentHomeWork submissionId, String imageLink) {
		super();
		this.submissionId = submissionId;
		this.imageLink = imageLink;
	}
	
	
	
}

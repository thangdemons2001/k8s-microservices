package com.vti.dto;

import java.util.Date;

public class ScheduleDTO {
	
	private int id;
	
	private Date startTime;
	
	private Date endTime;
	
	private String schedule;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public String getSchedule() {
		return schedule;
	}

	public void setSchedule(String schedule) {
		this.schedule = schedule;
	}
	
	
	public ScheduleDTO() {
		// TODO Auto-generated constructor stub
	}

	public ScheduleDTO(int id, Date startTime, Date endTime, String schedule) {
		super();
		this.id = id;
		this.startTime = startTime;
		this.endTime = endTime;
		this.schedule = schedule;
	}
	
	
}

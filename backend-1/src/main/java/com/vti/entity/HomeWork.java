package com.vti.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.vti.dto.HomeWorkDTO;
import com.vti.dto.StudentHomeWorkDTO;




@Entity
@Table(name = "HomeWork", catalog = "TCTmanagement")
public class HomeWork implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Column(name = "home_work_id" , nullable = false)
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "home_work_name" , nullable = false, length = 800)
	private String name;
	
	@Column(name = "home_work_link" , nullable = false, length = 8000)
	private String link;
	
	@Column(name = "home_work_key" , length = 8000)
	private String keyLink;
	
	@Column(name = "misson_detail" , nullable = false, length = 800)
	private String misson;
	
	@Column(name = "quizz_id")
	private int quizzId;
	
	@OneToOne
	@JoinColumn(name = "lesson_id")
	private Lesson lesson;
	
	@Column(name = "home_work_type")
	private String type;
	
	@Column(name = "deadline")
	@Temporal(TemporalType.TIMESTAMP)
	private Date deadline;
	
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getKeyLink() {
		return keyLink;
	}

	public void setKeyLink(String keyLink) {
		this.keyLink = keyLink;
	}

	public String getMisson() {
		return misson;
	}

	public void setMisson(String misson) {
		this.misson = misson;
	}

	public int getQuizzId() {
		return quizzId;
	}

	public void setQuizzId(int quizzId) {
		this.quizzId = quizzId;
	}

	public Lesson getLesson() {
		return lesson;
	}

	public void setLesson(Lesson lesson) {
		this.lesson = lesson;
	}

	public HomeWork() {
		// TODO Auto-generated constructor stub
	}
	
	
	
	public HomeWork(int id, String name, String link, String keyLink, String misson, int quizzId, Lesson lesson,
			String type) {
		super();
		this.id = id;
		this.name = name;
		this.link = link;
		this.keyLink = keyLink;
		this.misson = misson;
		this.quizzId = quizzId;
		this.lesson = lesson;
		this.type = type;
	}
	
	public HomeWork(int id, String name, String link, String keyLink, String misson, int quizzId) {
		super();
		this.id = id;
		this.name = name;
		this.link = link;
		this.keyLink = keyLink;
		this.misson = misson;
		this.quizzId = quizzId;
	}
	
	public HomeWorkDTO convertToDTO() {
		return new HomeWorkDTO(id, name, link, keyLink, misson, quizzId,type);
	}
	
	public StudentHomeWorkDTO convertToDTO2 (String status, Date date) {
		return new StudentHomeWorkDTO(status, id, name, link, keyLink, misson, quizzId,date);
	}
	
}

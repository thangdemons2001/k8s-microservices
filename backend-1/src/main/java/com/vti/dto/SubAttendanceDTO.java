package com.vti.dto;

import java.util.Date;

public class SubAttendanceDTO  {
	
	private StudentDTO student;
	
	private String attendanceStatus;
	
	private Date dateId;
	
	
	
	public Date getDateId() {
		return dateId;
	}

	public void setDateId(Date dateId) {
		this.dateId = dateId;
	}

	public StudentDTO getStudent() {
		return student;
	}

	public void setStudent(StudentDTO student) {
		this.student = student;
	}

	public String getAttendanceStatus() {
		return attendanceStatus;
	}

	public void setAttendanceStatus(String attendanceStatus) {
		this.attendanceStatus = attendanceStatus;
	}
	
	public SubAttendanceDTO() {
		// TODO Auto-generated constructor stub
	}

	public SubAttendanceDTO(StudentDTO student, String attendanceStatus, Date dateId) {
		super();
		this.student = student;
		this.attendanceStatus = attendanceStatus;
		this.dateId = dateId;
	}

	
	
}

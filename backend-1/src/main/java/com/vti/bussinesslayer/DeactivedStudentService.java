package com.vti.bussinesslayer;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vti.datalayer.IDeactivedStudentRepository;
import com.vti.datalayer.IStudentRepository;
import com.vti.entity.DeactivedStudent;


@Service
public class DeactivedStudentService implements IDeactivedStudentService {

	@Autowired
	private IDeactivedStudentRepository repository;


	@Override
	public List<DeactivedStudent> getAllDeactivedStudentInGrade(int grade) {
		return repository.findByGrade(grade);
	}

	@Override
	public void createDeactiveStudent(DeactivedStudent dto) {
		repository.save(dto);
	}

	@Override
	public void updateDeactiveStudent(int studentId, DeactivedStudent dto) {
		DeactivedStudent student = repository.findById(studentId);
		if (dto.getProcessStatus() != null) {
			student.setProcessStatus(dto.getProcessStatus());
		}
		repository.save(student);
	}

	@Override
	public void studentComeBack(int studentId) {
		DeactivedStudent student = repository.findById(studentId);
		repository.delete(student);
		
	}

}

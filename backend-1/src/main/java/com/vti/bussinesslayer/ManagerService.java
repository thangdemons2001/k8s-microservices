package com.vti.bussinesslayer;

import java.util.Calendar;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vti.datalayer.IAttenanceRepository;
import com.vti.datalayer.IClassroomStudentRepository;
import com.vti.datalayer.IStudentHomeWorkRepository;

@Service
public class ManagerService implements IManagerService {
	
	@Autowired
	private IAttenanceRepository attendanceRepository;
	
	@Autowired
	private IClassroomStudentRepository classroomStudentRepository;
	
	@Autowired
	private IStudentHomeWorkRepository studentHomeWorkRepository;

	@Override
	public void resetMonth() {
		Calendar cal = Calendar.getInstance();
		
		int month = cal.get(Calendar.MONTH) ;
		
		cal.set(Calendar.MONTH, month -  3);
		Date lastThreeMonths = cal.getTime();
		
		attendanceRepository.deleteAllAttendanceBeforeDate(lastThreeMonths);
		studentHomeWorkRepository.deleteAllSubmitBeforeDate(lastThreeMonths);
		
		
	}

	@Override
	public void resetCostStatusOfStatusInSubjectInGrade(String subject, int grade) {
		classroomStudentRepository.resetAllCostInfoOfSubjectInGrade(subject, grade,"inactive");
		
	}

}

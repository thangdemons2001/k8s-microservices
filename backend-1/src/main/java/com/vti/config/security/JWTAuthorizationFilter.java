package com.vti.config.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;

import com.vti.bussinesslayer.IUserService;
import com.vti.bussinesslayer.JWTTokenService;

import io.jsonwebtoken.ExpiredJwtException;

public class JWTAuthorizationFilter extends GenericFilterBean {

	private IUserService service;

	public JWTAuthorizationFilter(IUserService service) {
		super();
		this.service = service;
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain)
			throws IOException, ServletException {
		// TODO Auto-generated method stub
		 try {
			 	Authentication authentication = JWTTokenService.parseTokenToUserInformation((HttpServletRequest) request,
						service);

				SecurityContextHolder.getContext().setAuthentication(authentication);

				filterChain.doFilter(request, response);
	     } catch (ExpiredJwtException e) {

	    	 HttpServletResponse error = (HttpServletResponse) response;
	    	 error.setStatus(HttpStatus.UNAUTHORIZED.value());
	    	 error.getWriter().write("expried token");
	    }

		

	}

}

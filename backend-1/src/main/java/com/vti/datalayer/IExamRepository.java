package com.vti.datalayer;







import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.vti.entity.Exam;


public interface IExamRepository extends JpaRepository<Exam, Integer>, JpaSpecificationExecutor<Exam> {
		
	
	public Exam findById (int examId);
	
		
}

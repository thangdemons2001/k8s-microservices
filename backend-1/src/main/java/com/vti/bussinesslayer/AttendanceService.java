package com.vti.bussinesslayer;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vti.datalayer.IAttenanceRepository;
import com.vti.entity.Attendance;
import com.vti.entity.AttendanceKey;
@Service
public class AttendanceService implements IAttendanceService{

	@Autowired
	private IAttenanceRepository repository;

	@Override
	public void studentAtten(Attendance atten) {
		repository.save(atten);
	}

	@Override
	public Attendance findAttendanceById(AttendanceKey id) {
		return repository.getById(id);
	}

	@Override
	public List<Attendance> getAllStudentAttendanceInClassToday(int classId, Date dateId) {
		List<Attendance> presentStudent = repository.findById_ClassroomIdAndId_DateIdAndStatus(classId, dateId, "P");
		List<Attendance> lateStudent = repository.findById_ClassroomIdAndId_DateIdAndStatus(classId, dateId, "L");
		
		List<Attendance> res = new ArrayList<Attendance>();
		
		for(Attendance preAttendance: presentStudent) {
			res.add(preAttendance);
		}
		for(Attendance laAttendance: lateStudent) {
			res.add(laAttendance);
		}
		
		return res;
	}

	@Override
	public List<Attendance> getAllAttendance() {

		return repository.findAll();
	}

	@Override
	public Attendance getLastAttendance(int studentId, int classId) {
		return repository.findFirstById_StudentIdAndId_ClassroomIdOrderById_DateIdDesc(studentId, classId);
	}

	@Override
	public List<Attendance> getListAbsentStudentInWeeklyDay(Date dateId, int grade, String subject, String status) {
	
		return repository.findById_DateIdAndClassroom_GradeAndClassroom_SubjectAndStatus(dateId, grade, subject, status);
	}

	@Override
	public List<Attendance> getListStudentAttendanceInClass(int classId) {
		
		
		List<Attendance> res = repository.findById_ClassroomIdOrderById_DateIdDesc(classId);
		
		return res;
	}

	@Override
	public void deleteAttendance(int classId, Date dateId, int studentId) {
		Attendance atten = repository.findById_ClassroomIdAndId_DateIdAndId_StudentId(classId, dateId, studentId);
		repository.delete(atten);
	}

	@Override
	public int getTotalStudentInClassAtDate(int classId, Date dateId) {
		return repository.countById_ClassroomIdAndId_DateId(classId, dateId);
	}

	@Override
	public List<Attendance> getListAbsentStudentInLesson(int classId, Date dateId, String status) {
		return repository.findById_ClassroomIdAndId_DateIdAndStatus(classId, dateId, status);
	}

	@Override
	public Attendance findAttendanceByStudentIdAndDateIdAndClassId(int studentId, int classId, Date dateId) {
		return repository.findById_ClassroomIdAndId_DateIdAndId_StudentId(classId, dateId, studentId);
	}

	
	
	
		
	
}

package com.vti.datalayer;







import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.vti.entity.SubjectCostStatus;



public interface ISubjectCostStatusRepository extends JpaRepository<SubjectCostStatus, Integer>, JpaSpecificationExecutor<SubjectCostStatus> {
		
	
	public SubjectCostStatus findBySubjectAndGrade (String subject, int grade);
	
	public List<SubjectCostStatus> findByGrade (int grade);
	
		
}

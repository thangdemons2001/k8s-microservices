package com.vti.dto;

public class StudentStatisticDTO {
	
	private int numberOfStatisticStudentThisMonth;
	
	private double percentDeltaCurrentMonth;

	public StudentStatisticDTO(int numberOfStatisticStudentThisMonth, double percentDeltaCurrentMonth) {
		super();
		this.numberOfStatisticStudentThisMonth = numberOfStatisticStudentThisMonth;
		this.percentDeltaCurrentMonth = percentDeltaCurrentMonth;
	}
	
	public StudentStatisticDTO() {
		// TODO Auto-generated constructor stub
	}

	public int getNumberOfStatisticStudentThisMonth() {
		return numberOfStatisticStudentThisMonth;
	}

	public void setNumberOfStatisticStudentThisMonth(int numberOfStatisticStudentThisMonth) {
		this.numberOfStatisticStudentThisMonth = numberOfStatisticStudentThisMonth;
	}

	public double getPercentDeltaCurrentMonth() {
		return percentDeltaCurrentMonth;
	}

	public void setPercentDeltaCurrentMonth(double percentDeltaCurrentMonth) {
		this.percentDeltaCurrentMonth = percentDeltaCurrentMonth;
	}
	
	

}

package com.vti.dto;




// only use for attendance
public class StudentDTO2 {
	
	private int id;
		
	private String firstName;
	
	private String lastName;
		
	private String fullName;
	
	private String status;
	
	private String school;
	
	private int grade;
	
	private String studentNumber;
	
	private String parentNumber;
	
	private String parentName;
	
	private String attendanceStatus;
	
	private String testingSystemId;
	
	

	public String getTestingSystemId() {
		return testingSystemId;
	}

	public void setTestingSystemId(String testingSystemId) {
		this.testingSystemId = testingSystemId;
	}
	
	

	public StudentDTO2(int id, String firstName, String lastName, String fullName, String status, String school,
			int grade, String studentNumber, String parentNumber, String parentName, String attendanceStatus,
			String testingSystemId) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.fullName = fullName;
		this.status = status;
		this.school = school;
		this.grade = grade;
		this.studentNumber = studentNumber;
		this.parentNumber = parentNumber;
		this.parentName = parentName;
		this.attendanceStatus = attendanceStatus;
		this.testingSystemId = testingSystemId;
	}

	public StudentDTO2(int id, String firstName, String lastName, String fullName, String status, String school,
			int grade, String studentNumber, String parentNumber, String parentName, String attendanceStatus) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.fullName = fullName;
		this.status = status;
		this.school = school;
		this.grade = grade;
		this.studentNumber = studentNumber;
		this.parentNumber = parentNumber;
		this.parentName = parentName;
		this.attendanceStatus = attendanceStatus;
	}

	public String getAttendanceStatus() {
		return attendanceStatus;
	}

	public void setAttendanceStatus(String attendanceStatus) {
		this.attendanceStatus = attendanceStatus;
	}

	public String getSchool() {
		return school;
	}

	public void setSchool(String school) {
		this.school = school;
	}

	public int getGrade() {
		return grade;
	}

	public void setGrade(int grade) {
		this.grade = grade;
	}

	public String getStudentNumber() {
		return studentNumber;
	}

	public void setStudentNumber(String studentNumber) {
		this.studentNumber = studentNumber;
	}

	public String getParentNumber() {
		return parentNumber;
	}

	public void setParentNumber(String parentNumber) {
		this.parentNumber = parentNumber;
	}

	public String getParentName() {
		return parentName;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}



	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}


	public StudentDTO2() {
		// TODO Auto-generated constructor stub
	}
	
	
}

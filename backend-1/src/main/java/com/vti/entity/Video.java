package com.vti.entity;

import java.io.Serializable;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.Table;




@Entity
@Table(name = "Video", catalog = "TCTmanagement")
public class Video implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Column(name = "video_id" , nullable = false)
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "video_name" , nullable = false, length = 800)
	private String videoName;
	
	@Column(name = "grade" , nullable = false)
	private int grade;
	
	@Column(name = "subject" , nullable = false, length = 800)
	private String subject;
	
	@Column(name = "video_link" , nullable = false, length = 8000)
	private String link;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getVideoName() {
		return videoName;
	}

	public void setVideoName(String videoName) {
		this.videoName = videoName;
	}

	public int getGrade() {
		return grade;
	}

	public void setGrade(int grade) {
		this.grade = grade;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}
	
	public Video() {
		// TODO Auto-generated constructor stub
	}

	public Video(int id, String videoName, int grade, String subject, String link) {
		super();
		this.id = id;
		this.videoName = videoName;
		this.grade = grade;
		this.subject = subject;
		this.link = link;
	}
	
}

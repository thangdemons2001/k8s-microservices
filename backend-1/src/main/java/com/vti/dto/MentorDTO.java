package com.vti.dto;

public class MentorDTO {
		
	private int mentorId;
	
	private String fullName;
	
	private String firstName;
	
	private String lastName;
	
	private String school;
	
	
	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getMentorId() {
		return mentorId;
	}

	public void setMentorId(int mentorId) {
		this.mentorId = mentorId;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getSchool() {
		return school;
	}

	public void setSchool(String school) {
		this.school = school;
	}

	public MentorDTO(int mentorId, String fullName, String school) {
		super();
		this.mentorId = mentorId;
		this.fullName = fullName;
		this.school = school;
	}
	
	public MentorDTO(int mentorId, String fullName, String firstName, String lastName, String school) {
		super();
		this.mentorId = mentorId;
		this.fullName = fullName;
		this.firstName = firstName;
		this.lastName = lastName;
		this.school = school;
	}

	public MentorDTO() {
		// TODO Auto-generated constructor stub
	}
}

package com.vti.bussinesslayer;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vti.datalayer.IClassroomMentorRepository;
import com.vti.dto.ClassroomMentorDTO;
import com.vti.entity.ClassroomMentor;
import com.vti.entity.ClassroomMentorKey;


@Service
public class ClassroomMentorService implements IClassroomMentorService{

	@Autowired
	private IClassroomMentorRepository repository;
	
	
	
	@Override
	public void createMentorClass(int classId, List<ClassroomMentorDTO> listMentor) {
		
		List<ClassroomMentor> listClazzMentor = new ArrayList<ClassroomMentor>();
		
		for(ClassroomMentorDTO cs: listMentor) {
			ClassroomMentorKey id = new ClassroomMentorKey(cs.getMentorId(),cs.getClassId());
			ClassroomMentor clazzSt = new ClassroomMentor();
			clazzSt.setId(id);
			listClazzMentor.add(clazzSt);
		}
	
		repository.saveAll(listClazzMentor);
		
		
	}



	@Override
	public void updateMentorClass(int classId, List<ClassroomMentorDTO> listMentor) {
		List<ClassroomMentor> copy = repository.findByClassroom_Id(classId);
		repository.deleteAll(copy);
		List<ClassroomMentor> listClazzMentor = new ArrayList<ClassroomMentor>();
		for(ClassroomMentorDTO cs: listMentor) {
			ClassroomMentorKey id = new ClassroomMentorKey(cs.getMentorId(),cs.getClassId());
			ClassroomMentor clazzSt = new ClassroomMentor();
			clazzSt.setId(id);
			listClazzMentor.add(clazzSt);
		}
	
		repository.saveAll(listClazzMentor);
	}

}

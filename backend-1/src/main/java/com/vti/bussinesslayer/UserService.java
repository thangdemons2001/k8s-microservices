package com.vti.bussinesslayer;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;


import com.vti.datalayer.IUserRepository;
import com.vti.dto.ConnectTestingSystemDTO;
import com.vti.entity.User;


@Component
@Transactional
public class UserService implements IUserService{

		
	@Autowired
	private IUserRepository userRepository;
	

	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userRepository.findByUserName(username);

		if (user == null) {
			throw new UsernameNotFoundException(username);
		}

		return new org.springframework.security.core.userdetails.User(
				user.getUserName(), 
				user.getPassword(),
				AuthorityUtils.createAuthorityList(user.getRole()));
	}

	@Override
	public User findByUserName(String name) {
		// TODO Auto-generated method stub
		return userRepository.findByUserName(name);
	}

	@Override
	public boolean existsUserByUserName(String userName) {
		return userRepository.existsByUserName(userName);
	}

	@Override
	public User getUserById(int userId) {
		return userRepository.findById(userId);
	}

	@Override
	public void deleteUser(int userId) {
		User user = userRepository.findById(userId);
		userRepository.delete(user);
	}

	@Override
	public void updateStatusUser(int userId, String status) {
		User user = userRepository.findById(userId);
		user.setStatus(status);
		userRepository.save(user);
	}

	@Override
	public void connectToTestingSystem(ConnectTestingSystemDTO dto) {
		User user = userRepository.findById(dto.getUserId());
		user.setFacebookUrl(dto.getFacebookUrl());
		user.setTestingSystemId(dto.getTestingSystemId());
		userRepository.save(user);
	}
	
	
}

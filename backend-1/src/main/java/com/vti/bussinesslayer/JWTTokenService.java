package com.vti.bussinesslayer;


import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.vti.dto.LoginInfoUser;
import com.vti.entity.User;


import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class JWTTokenService {
	private static final long EXPIRATION_TIME = 86400000; // 1 days
    private static final String SECRET = "6r4F7y69j&T66%-a6#dL";
    private static final String PREFIX_TOKEN = "Bearer";
    private static final String AUTHORIZATION = "Authorization";
    
    public static void addJWTTokenAndUserInfoToBody(HttpServletResponse response, IUserService userService , String username) throws IOException {
        String JWT = Jwts.builder()
                .setSubject(username)
                .setExpiration(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
                .signWith(SignatureAlgorithm.HS512, SECRET)
                .compact();
        response.addHeader(AUTHORIZATION, PREFIX_TOKEN + " " + JWT);
        
        User user = userService.findByUserName(username); 	
     // convert user entity to user dto
        LoginInfoUser userDto = new LoginInfoUser(
        		JWT,
        		user.getId(),
        		user.getUserName(), 
        		user.getFirstName(), 
        		user.getLastName(),
        		user.getFullName(),
        		user.getRole(),     
        		user.getStatus(),
        		user.getAvatarUrl(),
        		user.getFacebookUrl());
        
        // convert object to json
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String json = ow.writeValueAsString(userDto);
        
        // return json
        response.setContentType("application/json;charset=UTF-8");
        response.getWriter().write(json);
    }
    
    public static Authentication parseTokenToUserInformation(HttpServletRequest request, IUserService service) {
        String token = request.getHeader(AUTHORIZATION);
        
        if (token == null) {
        	return null;
        }
        List<GrantedAuthority> list = new ArrayList<GrantedAuthority>();
    
        // parse the token
        String username = Jwts.parser()
                .setSigningKey(SECRET)
                .parseClaimsJws(token.replace(PREFIX_TOKEN, ""))
                .getBody()
                .getSubject();
        String role = service.findByUserName(username).getRole();
    	list.add(new SimpleGrantedAuthority(role));
        
        return username != null ?
                new UsernamePasswordAuthenticationToken(username, null,list) :
                null;
    }

}

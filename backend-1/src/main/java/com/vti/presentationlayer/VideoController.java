package com.vti.presentationlayer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vti.bussinesslayer.IChapterService;
import com.vti.bussinesslayer.IClassroomService;
import com.vti.bussinesslayer.IHomeWorkService;
import com.vti.bussinesslayer.ILessonService;
import com.vti.bussinesslayer.IVideoService;
import com.vti.entity.Video;



@RestController
@RequestMapping(value = "api/v1/videos")
@CrossOrigin("*")
public class VideoController {
	
	@Autowired
	private ILessonService service;
	
	@Autowired
	private IVideoService videoSerivce;
	
	@Autowired
	private IChapterService chapterService;
	
	@Autowired
	private IHomeWorkService homeWorkSerivce;
	
	@Autowired
	private IClassroomService classService;
	
	@PostMapping("/")
	public ResponseEntity<?> createVideo(@RequestBody Video videoDto) {
		
		Video video = new Video();
		
		video.setGrade(videoDto.getGrade());
		video.setVideoName(videoDto.getVideoName());
		video.setSubject(videoDto.getSubject());
		video.setLink(videoDto.getLink());
		videoSerivce.createVideo(video);
		
		return new ResponseEntity<Integer>(video.getId(), HttpStatus.OK);
	}
	@PutMapping("/{videoId}")
	public ResponseEntity<?> updateVideo(@RequestBody Video videoDto,@PathVariable(name = "videoId") int videoId) {
		
		Video video = videoSerivce.getVideoById(videoId);
		
		video.setGrade(videoDto.getGrade());
		video.setVideoName(videoDto.getVideoName());
		video.setSubject(videoDto.getSubject());
		video.setLink(videoDto.getLink());
		videoSerivce.createVideo(video);
		
		return new ResponseEntity<String>("update successful!", HttpStatus.OK);
	}

}

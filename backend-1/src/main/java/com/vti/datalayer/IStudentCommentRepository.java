package com.vti.datalayer;









import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;


import com.vti.entity.StudentComment;



public interface IStudentCommentRepository extends JpaRepository<StudentComment, Integer>, JpaSpecificationExecutor<StudentComment> {
	
		
	public List<StudentComment> findTop10ByStudent_IdOrderByCommentDateDesc(int studentId);
		
}

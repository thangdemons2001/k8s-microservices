package com.vti.datalayer;







import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;


import com.vti.entity.SubjectDefaultCost;



public interface ISubjectDefaultCost extends JpaRepository<SubjectDefaultCost, Integer>, JpaSpecificationExecutor<SubjectDefaultCost> {
		

	
	
		
}

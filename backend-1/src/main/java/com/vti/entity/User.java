package com.vti.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Formula;
@Entity
@Table(name = "User", catalog = "TCTmanagement")
@Inheritance(strategy = InheritanceType.JOINED)
public class User implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Column(name = "user_id", nullable = false)
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "user_name", length = 50, nullable = false, unique = true )
	private String userName;
	
	
	@Column(name = "password", length = 800, nullable = false )
	private String password;
	
	@Column(name = "first_name", length = 50, nullable = false )
	private String firstName;
	
	@Column(name = "last_name", length = 50, nullable = false )
	private String lastName;
	
	
	@Column(name = "created_date", updatable = false, nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdDate;
	
	@Column(name = "link_social", length = 800, nullable = true )
	private String social;
	
	@Formula(value = "concat(last_name, ' ', first_name)")
	private String fullName;
	
	@Column(name = "status_boolean", nullable = false)
	private String status;
	
	@Column(name = "role", nullable = false)
	private String role;
	
	@Column(name = "avatar_url", length = 8000 )
	private String avatarUrl;
	
	@Column(name = "facebook_url", length = 800 )
	private String facebookUrl;
	
	@Column(name = "testing_system_id", length = 800 )
	private String testingSystemId;
	
	
	
	public String getFacebookUrl() {
		return facebookUrl;
	}
	public void setFacebookUrl(String facebookUrl) {
		this.facebookUrl = facebookUrl;
	}
	public String getTestingSystemId() {
		return testingSystemId;
	}
	public void setTestingSystemId(String testingSystemId) {
		this.testingSystemId = testingSystemId;
	}
	public User(int id, String userName, String password, String firstName, String lastName, Date createdDate,
			String social, String fullName, String status, String role) {
		super();
		this.id = id;
		this.userName = userName;
		this.password = password;
		this.firstName = firstName;
		this.lastName = lastName;
		this.createdDate = createdDate;
		this.social = social;
		this.fullName = fullName;
		this.status = status;
		this.role = role;
	}
	public User() {
		// TODO Auto-generated constructor stub
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getSocial() {
		return social;
	}
	public void setSocial(String social) {
		this.social = social;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getAvatarUrl() {
		return avatarUrl;
	}
	public void setAvatarUrl(String avatarUrl) {
		this.avatarUrl = avatarUrl;
	}
	public User(int id, String userName, String password, String firstName, String lastName, Date createdDate,
			String social, String fullName, String status, String role, String avatarUrl, String facebookUrl,
			String testingSystemId) {
		super();
		this.id = id;
		this.userName = userName;
		this.password = password;
		this.firstName = firstName;
		this.lastName = lastName;
		this.createdDate = createdDate;
		this.social = social;
		this.fullName = fullName;
		this.status = status;
		this.role = role;
		this.avatarUrl = avatarUrl;
		this.facebookUrl = facebookUrl;
		this.testingSystemId = testingSystemId;
	}
	
	
	
}

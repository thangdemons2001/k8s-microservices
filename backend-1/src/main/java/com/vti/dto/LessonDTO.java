package com.vti.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class LessonDTO {
	
	private String lessonName;
	
	@JsonFormat(pattern = "dd-MM-yyyy")
	private Date date;
	
	private int chapterId;
	
	private int videoId;
	
	

	public LessonDTO(String lessonName, Date date, int chapterId, int videoId) {
		super();
		this.lessonName = lessonName;
		this.date = date;
		this.chapterId = chapterId;
		this.videoId = videoId;
	}

	public int getVideoId() {
		return videoId;
	}

	public void setVideoId(int videoId) {
		this.videoId = videoId;
	}

	public String getLessonName() {
		return lessonName;
	}

	public void setLessonName(String lessonName) {
		this.lessonName = lessonName;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getChapterId() {
		return chapterId;
	}

	public void setChapterId(int chapterId) {
		this.chapterId = chapterId;
	}
	
	public LessonDTO() {
		// TODO Auto-generated constructor stub
	}

	public LessonDTO(String lessonName, Date date, int chapterId) {
		super();
		this.lessonName = lessonName;
		this.date = date;
		this.chapterId = chapterId;
	}
	

}

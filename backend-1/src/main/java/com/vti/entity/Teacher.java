package com.vti.entity;

import java.io.Serializable;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;


import com.vti.dto.TeacherDTO;
@Entity
@Table(name = "Teacher", catalog = "TCTmanagement")
@PrimaryKeyJoinColumn(name = "teacher_id")
public class Teacher extends User implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Column(name = "subject_name", length = 50, nullable = false)
	private String subjectName;
	
	@OneToMany(mappedBy = "teacherId")
	private List<Classroom> listClass;

	public Teacher(String subjectName) {
		super();
		this.subjectName = subjectName;
	}
	public Teacher() {
		// TODO Auto-generated constructor stub
	}
	public String getSubjectName() {
		return subjectName;
	}
	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}
	public List<Classroom> getListClass() {
		return listClass;
	}
	public void setListClass(List<Classroom> listClass) {
		this.listClass = listClass;
	}
	public TeacherDTO convertToDTO() {
		
		
		return new TeacherDTO(this.getId(),this.getFirstName(), this.getLastName(), this.getFullName(), subjectName);
	}
}

package com.vti.dto;

public class ClassroomStudentDTO {
	
	private int classId;
	
	private int studentId;
	
	private String status;
	
	private String mainStatus;
	
	


	public String getMainStatus() {
		return mainStatus;
	}

	public void setMainStatus(String mainStatus) {
		this.mainStatus = mainStatus;
	}

	public ClassroomStudentDTO(int classId, int studentId, String status) {
		super();
		this.classId = classId;
		this.studentId = studentId;
		this.status = status;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getClassId() {
		return classId;
	}

	public void setClassId(int classId) {
		this.classId = classId;
	}

	public int getStudentId() {
		return studentId;
	}

	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}
	public ClassroomStudentDTO() {
		// TODO Auto-generated constructor stub
	}

	public ClassroomStudentDTO(int classId, int studentId) {
		super();
		this.classId = classId;
		this.studentId = studentId;
	}

	public ClassroomStudentDTO(int classId, int studentId, String status, String mainStatus) {
		super();
		this.classId = classId;
		this.studentId = studentId;
		this.status = status;
		this.mainStatus = mainStatus;
	}

	
	
	
	
}

package com.vti.dto;


import java.util.List;

import com.vti.entity.Lesson;


public class ClassroomDTO6 {
	
	private int id;
	
	private String className;
	
	private String subjectName;
	
	private int grade;
	
	private List<ScheduleDTO> listSchedule;
	
	private List<Lesson> listLesson; 


	public List<Lesson> getListLesson() {
		return listLesson;
	}

	public void setListLesson(List<Lesson> listLesson) {
		this.listLesson = listLesson;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public int getGrade() {
		return grade;
	}

	public void setGrade(int grade) {
		this.grade = grade;
	}

	

	public List<ScheduleDTO> getListSchedule() {
		return listSchedule;
	}

	public void setListSchedule(List<ScheduleDTO> listSchedule) {
		this.listSchedule = listSchedule;
	}

	public String getSubjectName() {
		return subjectName;
	}

	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}

	
	
	public ClassroomDTO6() {
		// TODO Auto-generated constructor stub
	}

	public ClassroomDTO6(int id, String className, String subjectName, int grade, List<ScheduleDTO> listSchedule) {
		super();
		this.id = id;
		this.className = className;
		this.subjectName = subjectName;
		this.grade = grade;
		this.listSchedule = listSchedule;
	}

	public ClassroomDTO6(int id, String className, String subjectName, int grade, List<ScheduleDTO> listSchedule, List<Lesson> listLesson) {
		super();
		this.id = id;
		this.className = className;
		this.subjectName = subjectName;
		this.grade = grade;
		this.listSchedule = listSchedule;
		this.listLesson = listLesson;
	}

	

	
	
	
	
}

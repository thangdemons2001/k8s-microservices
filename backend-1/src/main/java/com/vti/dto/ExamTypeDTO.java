package com.vti.dto;



public class ExamTypeDTO  {
	
	private int typeId;
	
	private String name;

	public int getTypeId() {
		return typeId;
	}

	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ExamTypeDTO() {
		// TODO Auto-generated constructor stub
	}

	public ExamTypeDTO(int typeId, String name) {
		super();
		this.typeId = typeId;
		this.name = name;
	}
	
	
}

package com.vti.datalayer;



import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.vti.entity.ClassroomMentor;
import com.vti.entity.ClassroomMentorKey;



public interface IClassroomMentorRepository extends JpaRepository<ClassroomMentor, ClassroomMentorKey>, JpaSpecificationExecutor<ClassroomMentor> {
		
		public List<ClassroomMentor> findByClassroom_Id(int id);
		
		
}

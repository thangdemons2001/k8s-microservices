package com.vti.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "SubjectCostStatus", catalog = "TCTmanagement")
public class SubjectCostStatus implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(name = "status_id", nullable = false)
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "subject_name", nullable = false)
	private String subject;
	
	@Column(name = "grade", nullable = false)
	private int grade;
	
	@Column(name = "cost_status", nullable = false)
	private String costStatus;
	
	@Column(name = "last_modify")
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastModify;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public int getGrade() {
		return grade;
	}

	public void setGrade(int grade) {
		this.grade = grade;
	}

	public String getCostStatus() {
		return costStatus;
	}

	public void setCostStatus(String costStatus) {
		this.costStatus = costStatus;
	}

	public Date getLastModify() {
		return lastModify;
	}

	public void setLastModify(Date lastModify) {
		this.lastModify = lastModify;
	}
	
	public SubjectCostStatus() {
		// TODO Auto-generated constructor stub
	}

	public SubjectCostStatus(int id, String subject, int grade, String costStatus, Date lastModify) {
		super();
		this.id = id;
		this.subject = subject;
		this.grade = grade;
		this.costStatus = costStatus;
		this.lastModify = lastModify;
	}

	
	
	

}

package com.vti.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
@Embeddable
public class ClassroomMentorKey implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Column(name = "mentor_id" , nullable = false)
	private int mentorId;
	
	@Column(name = "class_id" , nullable = false)
	private int classroomId;

	public ClassroomMentorKey() {
		// TODO Auto-generated constructor stub
	}

	public int getMentorId() {
		return mentorId;
	}

	public void setMentorId(int mentorId) {
		this.mentorId = mentorId;
	}

	public int getClassroomId() {
		return classroomId;
	}

	public void setClassroomId(int classroomId) {
		this.classroomId = classroomId;
	}

	public ClassroomMentorKey(int mentorId, int classroomId) {
		super();
		this.mentorId = mentorId;
		this.classroomId = classroomId;
	}
	
	
}

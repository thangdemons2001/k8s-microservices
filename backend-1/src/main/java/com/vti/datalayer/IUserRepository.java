package com.vti.datalayer;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.vti.entity.User;

public interface IUserRepository extends JpaRepository<User, Integer>, JpaSpecificationExecutor<User> {
	
	public User findByUserName(String userName);
	
	public boolean existsByUserName(String userName);
	
	public User findById (int userId);
	
	@Transactional
	@Modifying
	@Query("UPDATE User u SET u.avatarUrl = ?1 WHERE u.id = ?2 ")
	public void uploadUserAvatar (String nameImage, int userId);
}

package com.vti.presentationlayer;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import com.vti.bussinesslayer.IDeactivedStudentService;
import com.vti.bussinesslayer.IReasonLeftService;
import com.vti.dto.DeactivedStudentDTO;
import com.vti.entity.DeactivedStudent;


@RestController
@RequestMapping(value = "api/v1/deactived/students")
@CrossOrigin("*")
public class DeactivedStudentController {
	

	@Autowired
	private IDeactivedStudentService service;
	
	@Autowired
	private IReasonLeftService reasonService;
	
	
	@GetMapping(value = "/")
	public ResponseEntity<?> getAllDeactivedStudentInGrade(int grade) {

		List<DeactivedStudentDTO> res = new ArrayList<DeactivedStudentDTO>();
		List<DeactivedStudent> students = service.getAllDeactivedStudentInGrade(grade);
		for(DeactivedStudent student: students) {
			res.add(student.convertToDTO());
		}
		
		return new ResponseEntity<List<DeactivedStudentDTO>>(res, HttpStatus.OK);
	}

	@PostMapping(value = "/")
	public ResponseEntity<?> createDeactivedStudent(@RequestBody DeactivedStudent dto) {
		service.createDeactiveStudent(dto);
		return new ResponseEntity<Integer>(dto.getId(), HttpStatus.OK);
	}
		
	@DeleteMapping(value = "/{studentId}")
	public ResponseEntity<?> deleteDeactivedStudent(@PathVariable(name = "studentId") int deactivedStudentId) {
		service.studentComeBack(deactivedStudentId);
		return new ResponseEntity<String>("delete successful!", HttpStatus.OK);
	}
	
	@PutMapping(value = "/{studentId}")
	public ResponseEntity<?> updateDeactivedStudent(@PathVariable(name = "studentId") int deactivedStudentId, @RequestBody DeactivedStudent dto) {
		service.updateDeactiveStudent(deactivedStudentId, dto);
		return new ResponseEntity<String>("update successful!", HttpStatus.OK);
	}
	
}

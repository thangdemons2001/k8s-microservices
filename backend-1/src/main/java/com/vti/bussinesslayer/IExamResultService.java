package com.vti.bussinesslayer;

import java.util.List;

import com.vti.dto.ExamResultDTO;
import com.vti.entity.ExamResult;
import com.vti.entity.ExamResultKey;
import com.vti.entity.Student;

public interface IExamResultService {
		
		
		public void createListExamResult(List<ExamResult> results);
		
		public void updateExamResult (ExamResultKey id, double mark);
		
		public void updateManyExamResult (List<ExamResultDTO> dtos);
		
		public List<ExamResult> getListStudentExamMarkInClass (int classId, int examId);
		
		public List<ExamResult> getListExamInClass (int classId);
		
		public List<ExamResult> getListStudentAvgMarkSubjectInGrade(int grade, String subjectName);
		
		public List<ExamResult> getListStudentMarkInExam(int id);
		
		public List<Student> getListStudentNotTakeSubjectExamInMonthAtGrade(int month, String subject, int grade);
}

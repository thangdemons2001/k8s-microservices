package com.vti.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class ExamResultDTO2  {
	
	private int examId;
	
	private String examName;
	
	@JsonFormat(pattern = "dd-MM-yyyy")
	private Date createdDate;

	private String type;
	
	
	private String teacherName;
	
	private String className;
	
	private int classId;
	
	private String testingSystemId;
	
	

	public ExamResultDTO2(int examId, String examName, Date createdDate, String type, String teacherName,
			String className, int classId, String testingSystemId) {
		super();
		this.examId = examId;
		this.examName = examName;
		this.createdDate = createdDate;
		this.type = type;
		this.teacherName = teacherName;
		this.className = className;
		this.classId = classId;
		this.testingSystemId = testingSystemId;
	}

	public String getTestingSystemId() {
		return testingSystemId;
	}

	public void setTestingSystemId(String testingSystemId) {
		this.testingSystemId = testingSystemId;
	}

	public ExamResultDTO2(int examId, String examName, Date createdDate, String type, String teacherName,
			String className, int classId) {
		super();
		this.examId = examId;
		this.examName = examName;
		this.createdDate = createdDate;
		this.type = type;
		this.teacherName = teacherName;
		this.className = className;
		this.classId = classId;
	}

	public int getClassId() {
		return classId;
	}

	public void setClassId(int classId) {
		this.classId = classId;
	}

	public ExamResultDTO2(int examId, String examName, Date createdDate, String type, String teacherName,
			String className) {
		super();
		this.examId = examId;
		this.examName = examName;
		this.createdDate = createdDate;
		this.type = type;
		this.teacherName = teacherName;
		this.className = className;
	}

	public String getTeacherName() {
		return teacherName;
	}

	public void setTeacherName(String teacherName) {
		this.teacherName = teacherName;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	

	public int getExamId() {
		return examId;
	}

	public void setExamId(int examId) {
		this.examId = examId;
	}

	public String getExamName() {
		return examName;
	}

	public void setExamName(String examName) {
		this.examName = examName;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	public ExamResultDTO2() {
		// TODO Auto-generated constructor stub
	}

	public ExamResultDTO2(int examId, String examName, Date createdDate, String type) {
		super();
		this.examId = examId;
		this.examName = examName;
		this.createdDate = createdDate;
		this.type = type;
	}

	
	
	
	
}

package com.vti.dto;

import java.util.Date;
import java.util.List;

public class ClassroomDTO7 {
	
	private int id;
	
	private String className;
	
	private String subjectName;
	
	private int grade;
	
	private TeacherDTO teacher;
	
	private Date startTime;
	
	private Date endTime;
	
	private String schedule;
	
	private int totalStudent;
	
	private List<MentorDTO> listMentor;
	
	private String costStatus;
	
	private String subCostStatus;
	

	public String getSubCostStatus() {
		return subCostStatus;
	}



	public void setSubCostStatus(String subCostStatus) {
		this.subCostStatus = subCostStatus;
	}



	public String getCostStatus() {
		return costStatus;
	}



	public void setCostStatus(String costStatus) {
		this.costStatus = costStatus;
	}



	public List<MentorDTO> getListMentor() {
		return listMentor;
	}



	public void setListMentor(List<MentorDTO> listMentor) {
		this.listMentor = listMentor;
	}



	public int getTotalStudent() {
		return totalStudent;
	}
	
	
	
	public Date getStartTime() {
		return startTime;
	}



	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}



	public Date getEndTime() {
		return endTime;
	}



	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}



	public String getSchedule() {
		return schedule;
	}



	public void setSchedule(String schedule) {
		this.schedule = schedule;
	}



	public void setTotalStudent(int totalStudent) {
		this.totalStudent = totalStudent;
	}
	
	public TeacherDTO getTeacher() {
		return teacher;
	}

	public void setTeacher(TeacherDTO teacher) {
		this.teacher = teacher;
	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public int getGrade() {
		return grade;
	}

	public void setGrade(int grade) {
		this.grade = grade;
	}

	public TeacherDTO getTeacherId() {
		return teacher;
	}

	public void setTeacherId(TeacherDTO teacherId) {
		this.teacher = teacherId;
	}


	public String getSubjectName() {
		return subjectName;
	}

	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}

	
	public ClassroomDTO7() {
		// TODO Auto-generated constructor stub
	}



	public ClassroomDTO7(int id, String className, String subjectName, int grade, TeacherDTO teacher, Date startTime,
			Date endTime, String schedule) {
		super();
		this.id = id;
		this.className = className;
		this.subjectName = subjectName;
		this.grade = grade;
		this.teacher = teacher;
		this.startTime = startTime;
		this.endTime = endTime;
		this.schedule = schedule;
	}



	public ClassroomDTO7(int id, String className, String subjectName, int grade, TeacherDTO teacher, Date startTime,
			Date endTime, String schedule, List<MentorDTO> listMentor) {
		super();
		this.id = id;
		this.className = className;
		this.subjectName = subjectName;
		this.grade = grade;
		this.teacher = teacher;
		this.startTime = startTime;
		this.endTime = endTime;
		this.schedule = schedule;
		this.listMentor = listMentor;
	}



	public ClassroomDTO7(int id, String className, String subjectName, int grade, TeacherDTO teacher, Date startTime,
			Date endTime, String schedule, String costStatus, String subCostStatus) {
		super();
		this.id = id;
		this.className = className;
		this.subjectName = subjectName;
		this.grade = grade;
		this.teacher = teacher;
		this.startTime = startTime;
		this.endTime = endTime;
		this.schedule = schedule;
		this.costStatus = costStatus;
		this.subCostStatus = subCostStatus;
	}

	
	

	
}

package com.vti;

import java.util.TimeZone;

import javax.annotation.PostConstruct;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;


@SpringBootApplication
@ComponentScan(basePackages = "com.vti.*")
public class TctSystemApplication {

	public static void main(String[] args) {
		SpringApplication.run(TctSystemApplication.class, args);
	}
	
	@PostConstruct
    void setUTCTimeZone(){
        TimeZone.setDefault(TimeZone.getTimeZone("Asia/Bangkok"));
    }
}
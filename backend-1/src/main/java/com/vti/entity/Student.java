package com.vti.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.vti.dto.ClassroomDTO;
import com.vti.dto.StudentDTO;
import com.vti.dto.StudentDTO10;
import com.vti.dto.StudentDTO2;
import com.vti.dto.StudentDTO3;
import com.vti.dto.StudentDTO4;
import com.vti.dto.StudentDTO5;
import com.vti.dto.StudentDTO6;
import com.vti.dto.StudentDTO7;
import com.vti.dto.StudentDTO9;
import com.vti.dto.StudentSubjectInfo;

@Entity
@Table(name = "Student", catalog = "TCTmanagement")
@PrimaryKeyJoinColumn(name = "student_id")
public class Student extends User implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(name = "school_name", length = 800)
	private String school;

	@Column(name = "grade", nullable = false)
	private int grade;

	@Column(name = "student_phone_number", length = 50, nullable = false)
	private String studentNumber;

	@Column(name = "parent_phone_number", length = 50, nullable = false)
	private String parentNumber;

	@Column(name = "parent_name", length = 50, nullable = true)
	private String parentName;

	@Column(name = "student_score")
	private double score;
	
	@Column(name = "note", length = 100, nullable = true)
	private String note;

	@OneToMany(mappedBy = "student")
	private List<ClassroomStudent> listClass;

	@OneToMany(mappedBy = "student")
	private List<ExamResult> listExam;

	@OneToMany(mappedBy = "student")
	private List<Attendance> listAttendance;

	@Column(name = "start_date", nullable = false)
	@Temporal(TemporalType.DATE)
	private Date startDate;
	
	

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public double getScore() {
		return score;
	}

	public void setScore(double score) {
		this.score = score;
	}

	public List<Attendance> getListAttendance() {
		return listAttendance;
	}

	public void setListAttendance(List<Attendance> listAttendance) {
		this.listAttendance = listAttendance;
	}

	public List<ExamResult> getListExam() {
		return listExam;
	}

	public void setListExam(List<ExamResult> listExam) {
		this.listExam = listExam;
	}

	public Student(int id, String userName, String password, String firstName, String lastName, Date createdDate,
			String social, String fullName, String status, String role, String school, int grade, String studentNumber,
			String parentNumber, String parentName, double score, Date startDate) {
		super(id, userName, password, firstName, lastName, createdDate, social, fullName, status, role);
		this.school = school;
		this.grade = grade;
		this.studentNumber = studentNumber;
		this.parentNumber = parentNumber;
		this.parentName = parentName;
		this.score = score;
		this.startDate = startDate;
	}

	public Student(String school, int grade, String studentNumber, String parentNumber, String parentName) {
		super();
		this.school = school;
		this.grade = grade;
		this.studentNumber = studentNumber;
		this.parentNumber = parentNumber;
		this.parentName = parentName;
	}

	public Student() {
		// TODO Auto-generated constructor stub
	}

	public String getSchool() {
		return school;
	}

	public void setSchool(String school) {
		this.school = school;
	}

	public int getGrade() {
		return grade;
	}

	public void setGrade(int grade) {
		this.grade = grade;
	}

	public String getStudentNumber() {
		return studentNumber;
	}

	public void setStudentNumber(String studentNumber) {
		this.studentNumber = studentNumber;
	}

	public String getParentNumber() {
		return parentNumber;
	}

	public void setParentNumber(String parentNumber) {
		this.parentNumber = parentNumber;
	}

	public String getParentName() {
		return parentName;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	public List<ClassroomStudent> getListClass() {
		return listClass;
	}

	public void setListClass(List<ClassroomStudent> listClass) {
		this.listClass = listClass;
	}

	// DTO get student info and student's classes
	public StudentDTO convertToDTO() {

		List<ClassroomDTO> listClazz = new ArrayList<ClassroomDTO>();

		for (ClassroomStudent clazz : listClass) {
			listClazz.add(clazz.getClassroom().convertToDTO());
		}

		return new StudentDTO(this.getId(), this.getFirstName(), this.getLastName(), this.getFullName(),
				this.getStatus(), school, grade, studentNumber, parentNumber, parentName, this.getSocial(), listClazz);
	}

	// DTO2 to take student info only
	public StudentDTO convertToDTO2() {

		return new StudentDTO(this.getId(), this.getFirstName(), this.getLastName(), this.getFullName(),
				this.getStatus(), school, grade, studentNumber, parentNumber, parentName, this.getSocial(),
				this.getAvatarUrl(), this.getFacebookUrl(), this.getTestingSystemId(), this.getStartDate());
	}
	
	// thêm lí do nghỉ học khi lấy ra học sinh
	public StudentDTO convertToDTO10(String reasonAbsent) {

		return new StudentDTO(this.getId(), this.getFirstName(), this.getLastName(), this.getFullName(),
				this.getStatus(), school, grade, studentNumber, parentNumber, parentName, this.getSocial(),
				this.getAvatarUrl(), this.getFacebookUrl(), this.getTestingSystemId(), this.getStartDate(),"note",reasonAbsent);
	}

	public StudentDTO convertToDTO2(String avatarUrl) {

		return new StudentDTO(this.getId(), this.getFirstName(), this.getLastName(), this.getFullName(),
				this.getStatus(), school, grade, studentNumber, parentNumber, parentName, this.getSocial(),
				this.getAvatarUrl(), this.getFacebookUrl(), this.getTestingSystemId(), this.getStartDate(), this.getNote());
	}

	// DTO3 to get student attendance result today
	public StudentDTO2 convertToDTO3(String todayAttendannceStatus) {
		// this function take value from Attendance.java.status as param
		// "todayAttendannceStatus"

		return new StudentDTO2(this.getId(), this.getFirstName(), this.getLastName(), this.getFullName(),
				this.getStatus(), school, grade, studentNumber, parentNumber, parentName, todayAttendannceStatus,
				this.getTestingSystemId());
	}

	// DTO4 để lấy ra lớp học học sinh nghỉ ngày hôm đấy sử dụng tại
	// AttendanceController.getAllAbsentStudentInWeeklyDay
	public StudentDTO3 convertToDTO4(String className) {

		return new StudentDTO3(this.getId(), this.getFirstName(), this.getLastName(), this.getFullName(),
				this.getStatus(), school, grade, className, studentNumber, parentNumber, parentName);
	}

	// DTO5 để lấy ra học sinh nghỉ học trong tuần và lớp học sinh nghỉ
	public StudentDTO4 convertToDTO5(String className, int classId) {

		return new StudentDTO4(this.getId(), this.getFirstName(), this.getLastName(), this.getFullName(),
				this.getStatus(), school, grade, className, classId, studentNumber, parentNumber, parentName);
	}

	// DTO6 để lấy điểm {mark} bài kiểm tra {examId} của học sinh trong lớp
	// {classId} sử dụng tại ExamController.
	public StudentDTO5 convertToDTO6(double mark) {

		return new StudentDTO5(this.getId(), this.getFirstName(), this.getLastName(), this.getFullName(),
				this.getStatus(), school, mark, studentNumber, parentNumber, parentName, this.getTestingSystemId(),
				this.getAvatarUrl(), this.getFacebookUrl());
	}

	public StudentDTO6 convertToDTO7(String className, int classId, String costStatus) {

		return new StudentDTO6(this.getId(), this.getFirstName(), this.getLastName(), this.getFullName(), costStatus,
				school, grade, className, classId, studentNumber, parentNumber, parentName);
	}

	// Lay thong tin hoc sinh va diem trung binh trong mon hoc
	public StudentDTO7 convertToDTO8(double avgMark) {

		return new StudentDTO7(this.getId(), this.getFirstName(), this.getLastName(), this.getFullName(), school, grade,
				avgMark, studentNumber, parentNumber, parentName);
	}

	public StudentDTO7 convertToDTO8(double avgMark, double score, String avatarUrl) {

		return new StudentDTO7(this.getId(), this.getFirstName(), this.getLastName(), this.getFullName(), school, grade,
				avgMark, studentNumber, parentNumber, parentName, score, avatarUrl, this.getFacebookUrl());
	}

	public StudentSubjectInfo convertToStudentSubjectInFo() {

		return null;
	}

	public StudentDTO10 convertToDTO10(ArrayList<ClassroomDTO> listCLass, String status) {
		return new StudentDTO10(this.getId(), this.getFirstName(), this.getLastName(), this.getFullName(), status,
				school, grade, studentNumber, parentNumber, parentName, listCLass);
	}

	public StudentDTO9 convertToDTO9(ArrayList<ClassroomDTO> className) {

		return new StudentDTO9(this.getId(), this.getFirstName(), this.getLastName(), this.getFullName(),
				this.getStatus(), school, grade, className, studentNumber, parentNumber, parentName);
	}

	public StudentDTO9 convertToDTO9() {

		return new StudentDTO9(this.getId(), this.getFirstName(), this.getLastName(), this.getFullName(),
				this.getStatus(), school, grade, studentNumber, parentNumber, parentName, this.getAvatarUrl(), this.getFacebookUrl());
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return this.getId();
	}

	@Override
	public boolean equals(Object obj) {
		Student student = (Student) obj;
		return this.getId() == student.getId();
	}
}

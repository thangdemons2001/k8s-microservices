package com.vti.dto;


import java.util.List;






public class ClassroomDTO3 {
	
	private int id;
	
	private String className;
	
	private String subjectName;
	
	private int grade;
	
	private List<ScheduleDTO> listSchedule;
	
	

	public List<ScheduleDTO> getListSchedule() {
		return listSchedule;
	}

	public void setListSchedule(List<ScheduleDTO> listSchedule) {
		this.listSchedule = listSchedule;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public int getGrade() {
		return grade;
	}

	public void setGrade(int grade) {
		this.grade = grade;
	}


	

	public String getSubjectName() {
		return subjectName;
	}

	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}

	
	
	public ClassroomDTO3() {
		// TODO Auto-generated constructor stub
	}

	public ClassroomDTO3(int id, String className, String subjectName, int grade, List<ScheduleDTO> listSchedule) {
		super();
		this.id = id;
		this.className = className;
		this.subjectName = subjectName;
		this.grade = grade;
		this.listSchedule = listSchedule;
	}

	
	

}

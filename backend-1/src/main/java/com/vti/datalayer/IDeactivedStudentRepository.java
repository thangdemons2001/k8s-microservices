package com.vti.datalayer;



import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.vti.entity.DeactivedStudent;


public interface IDeactivedStudentRepository extends JpaRepository<DeactivedStudent, Integer>, JpaSpecificationExecutor<DeactivedStudent> {
	
	public List<DeactivedStudent> findByGrade (int grade);
	
	public DeactivedStudent findById (int id);
	
}

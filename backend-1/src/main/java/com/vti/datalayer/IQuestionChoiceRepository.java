package com.vti.datalayer;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.vti.entity.QuestionChoice;

public interface IQuestionChoiceRepository
		extends JpaRepository<QuestionChoice, Long>, JpaSpecificationExecutor<QuestionChoice> {
	
	public List<QuestionChoice> findByQuestion_Id (long questionId);

}

package com.vti.dto;





public class LoginInfoUser {
	
	private String token;
	
	private int id;
	
	private String userName;
	
	private String firstName;
	
	private String lastName;
	
	private String fullName;
	

	
	private String role;

	private String status;
	
	private String avatarUrl;
	
	private String facebookUrl;
	
	

	public String getFacebookUrl() {
		return facebookUrl;
	}

	public void setFacebookUrl(String facebookUrl) {
		this.facebookUrl = facebookUrl;
	}

	public String getAvatarUrl() {
		return avatarUrl;
	}

	public void setAvatarUrl(String avatarUrl) {
		this.avatarUrl = avatarUrl;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}



	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}


	
	public LoginInfoUser() {
		// TODO Auto-generated constructor stub
	}

	public LoginInfoUser(String token, int id, String userName, String firstName, String lastName, String fullName,
			 String role, String status) {
		super();
		this.token = token;
		this.id = id;
		this.userName = userName;
		this.firstName = firstName;
		this.lastName = lastName;
		this.fullName = fullName;
		this.role = role;
		this.status = status;

	}

	public LoginInfoUser(String token, int id, String userName, String firstName, String lastName, String fullName,
			String role, String status, String avatarUrl) {
		super();
		this.token = token;
		this.id = id;
		this.userName = userName;
		this.firstName = firstName;
		this.lastName = lastName;
		this.fullName = fullName;
		this.role = role;
		this.status = status;
		this.avatarUrl = avatarUrl;
	}

	public LoginInfoUser(String token, int id, String userName, String firstName, String lastName, String fullName,
			String role, String status, String avatarUrl, String facebookUrl) {
		super();
		this.token = token;
		this.id = id;
		this.userName = userName;
		this.firstName = firstName;
		this.lastName = lastName;
		this.fullName = fullName;
		this.role = role;
		this.status = status;
		this.avatarUrl = avatarUrl;
		this.facebookUrl = facebookUrl;
	}
	
	
}

package com.vti.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.vti.dto.LessonDTO3;
import com.vti.dto.LessonDTO4;
import com.vti.dto.TeacherDTO;




@Entity
@Table(name = "Lesson", catalog = "TCTmanagement")
public class Lesson implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Column(name = "lesson_id" , nullable = false)
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "lesson_name" , nullable = false, length = 800)
	private String lessonName;
	
	@Column(name = "date", nullable = false)
	@Temporal(TemporalType.DATE)
	@JsonFormat(pattern = "dd-MM-yyyy")
	private Date date;
	
	@ManyToOne
	@JoinColumn(name = "video_id")
	private Video video;
	
	@OneToMany(mappedBy = "lesson")
	private List<ClassroomLesson> classList;
	
	@ManyToOne
	@JoinColumn(name = "chapter_id")
	private Chapter chapter;
	
	@OneToOne(mappedBy = "lesson")
	private HomeWork homework;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLessonName() {
		return lessonName;
	}

	public void setLessonName(String lessonName) {
		this.lessonName = lessonName;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Video getVideo() {
		return video;
	}

	public void setVideo(Video video) {
		this.video = video;
	}

	public Chapter getChapter() {
		return chapter;
	}

	public void setChapter(Chapter chapter) {
		this.chapter = chapter;
	}
	
	
	public HomeWork getHomework() {
		return homework;
	}

	public void setHomework(HomeWork homework) {
		this.homework = homework;
	}

	public Lesson() {
		// TODO Auto-generated constructor stub
	}

	public Lesson(int id, String lessonName, Date date, Video video, Chapter chapter) {
		super();
		this.id = id;
		this.lessonName = lessonName;
		this.date = date;
		this.video = video;
		this.chapter = chapter;
	}

	public Lesson(int id, String lessonName, Date date) {
		super();
		this.id = id;
		this.lessonName = lessonName;
		this.date = date;
		
	}

	public Lesson(int id, String lessonName, Date date, Chapter chapter) {
		super();
		this.id = id;
		this.lessonName = lessonName;
		this.date = date;
		this.chapter = chapter;
	}
	
	public LessonDTO3 convertToDTO() {
		if(this.homework == null) {
			return new LessonDTO3(id, lessonName, date, video, chapter);
		}
		
		return new LessonDTO3(id, lessonName, date, video, chapter, this.homework.convertToDTO());
	}
	
	public LessonDTO4 convertToDTO1(TeacherDTO teacher) {
		return new LessonDTO4(id,lessonName,date,teacher,video,chapter);
	}
	
}
